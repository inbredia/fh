<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

 /**
     * Add the config to the registry
     */
    protected function _initConfig()
    {
	
        Zend_Registry::set('config', $this->getOptions());
    }

	public static function setRoutes(){

		$frontController = Zend_Controller_Front::getInstance(); 
		$router = $frontController->getRouter();

		$routes = array();

		// now include the routes file.  NOTE: PLEASE ENSURE routes.php is formatted correctly
		if(file_exists('../application/configs/routes.php')) // getcwd() shows we are in /public/
		{

			require_once "configs/routes.php";

			foreach($routes as $routeName => $routeValue){
				$router->addRoute($routeName, $routeValue);
			}

			if($useDefaultRoutes == false)
			{
				// remove Zend default routes -- model/controller/action
				$router->removeDefaultRoutes();
			}

		}

	}
	
	
	/**
     * Setup the logging
     */
    protected function _initLogging()
    {
        $this->bootstrap('frontController');
        $logger = new Zend_Log();
        $config = Zend_Registry::get('config');
        $fileWriter = new Zend_Log_Writer_Stream($config['appdata']['dirpath'] . '/logs/app.log');
        $logger->addWriter($fileWriter);

        //enable log writer for firebug if environment is not production
        if ('production' != $this->getEnvironment())
        {
            $firebugWriter = new Zend_Log_Writer_Firebug();
            $logger->addWriter($firebugWriter);
        }

        $filter = new Zend_Log_Filter_Priority(Zend_Log::NOTICE);

        if ('production' == $this->getEnvironment())
        {
            $filter = new Zend_Log_Filter_Priority(Zend_Log::CRIT);
        }
        else if ('development' == $this->getEnvironment())
        {
            $filter = new Zend_Log_Filter_Priority(Zend_Log::DEBUG);
        }

        $logger->addFilter($filter);
        $this->_logger = $logger;
        Zend_Registry::set('log', $logger);
    }

	
	
	
	protected function _initViewHelpers()
{
    $view = new Zend_View();
	$view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");
	$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
	$viewRenderer->setView($view);
	Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
}

	protected function _initBanners() {
		mysql_connect('localhost','fastest','Priyanka@1');
		mysql_select_db('fastest_hiring');
		$query = "select * from tbl_banner where status=1";
		$res= mysql_query($query);
		$arr= array();
		$i=0;
		while($data=mysql_fetch_array($res)){
			$arr[$i]['position'] = $data['position'];
			$arr[$i]['link'] = $data['link'];
			$arr[$i++]['image'] = $data['image'];
			
		}
		
		$_SESSION['banners'] = $arr;
	}

	
	
	protected function _initFrontModules() {
		$this->bootstrap('frontController');
		$front = $this->getResource('frontController');		
		$this->setRoutes();
	}

}