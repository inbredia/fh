<?php
	// ROUTES FILE
	// PLEASE USE THE '$routes' variable as an array.
	// key shoulds be the name we want to know this by
	// value should be a call to Zend_Controller_Router_Route - correctly formatted with a route.

	// more generic rules should go above specific ones.

	// set whether or not to use default routes.
	// if you do not specify, default routes WILL be available
	$useDefaultRoutes = false;


	$routes['index'] = new Zend_Controller_Router_Route('', array('controller' => 'index','action' => 'index', 'module' => 'default'));
	$routes['signup'] = new Zend_Controller_Router_Route('signup/', array('controller' => 'index','action' => 'signup', 'module' => 'user'));
	$routes['signupsuccess'] = new Zend_Controller_Router_Route('signupsuccess/', array('controller' => 'index','action' => 'signupsuccess', 'module' => 'user'));
	$routes['login'] = new Zend_Controller_Router_Route('login/', array('controller' => 'index','action' => 'login', 'module' => 'user'));
	$routes['candidateregistration'] = new Zend_Controller_Router_Route('candidateregistration/', array('controller' => 'index','action' => 'candidateregistration', 'module' => 'user'));
	$routes['getsubcats'] = new Zend_Controller_Router_Route('getsubcats/', array('controller' => 'index','action' => 'getsubcats', 'module' => 'user'));
	$routes['agencyregistration'] = new Zend_Controller_Router_Route('agencyregistration/', array('controller' => 'agency','action' => 'agencyregistration', 'module' => 'user'));
	$routes['getcity'] = new Zend_Controller_Router_Route('getcity/', array('controller' => 'index','action' => 'getcity', 'module' => 'user'));
	$routes['getskills'] = new Zend_Controller_Router_Route('getskills/', array('controller' => 'index','action' => 'getskills', 'module' => 'user'));
	$routes['login'] = new Zend_Controller_Router_Route('login/', array('controller' => 'login','action' => 'index', 'module' => 'user'));
	$routes['myprofile'] = new Zend_Controller_Router_Route('myprofile/', array('controller' => 'index','action' => 'myprofile', 'module' => 'user'));
	$routes['viewprofile'] = new Zend_Controller_Router_Route('viewprofile/', array('controller' => 'index','action' => 'viewprofile', 'module' => 'user'));
	$routes['profile'] = new Zend_Controller_Router_Route('profile/', array('controller' => 'index','action' => 'profile', 'module' => 'user'));
	$routes['editcandidate'] = new Zend_Controller_Router_Route('editcandidate/', array('controller' => 'index','action' => 'editcandidate', 'module' => 'user'));
	$routes['uploadphoto'] = new Zend_Controller_Router_Route('uploadphoto/', array('controller' => 'index','action' => 'uploadphoto', 'module' => 'user'));
	$routes['deletephoto'] = new Zend_Controller_Router_Route('deletephoto/', array('controller' => 'index','action' => 'deletephoto', 'module' => 'user'));
	$routes['getmyimages'] = new Zend_Controller_Router_Route('getmyimages/', array('controller' => 'index','action' => 'getmyimages', 'module' => 'user'));
	$routes['verifyemail'] = new Zend_Controller_Router_Route('verifyemail/', array('controller' => 'index','action' => 'verifyemail', 'module' => 'user'));
	$routes['verifymobile'] = new Zend_Controller_Router_Route('verifymobile/', array('controller' => 'index','action' => 'verifymobile', 'module' => 'user'));
	$routes['addjob'] = new Zend_Controller_Router_Route('addjob/', array('controller' => 'index','action' => 'addjob', 'module' => 'user'));
	$routes['getsubcatsselect'] = new Zend_Controller_Router_Route('getsubcatsselect/', array('controller' => 'index','action' => 'getsubcatsselect', 'module' => 'user'));
	$routes['getcityselect'] = new Zend_Controller_Router_Route('getcityselect/', array('controller' => 'index','action' => 'getcityselect', 'module' => 'user'));
	$routes['logout'] = new Zend_Controller_Router_Route('logout/', array('controller' => 'login','action' => 'logout', 'module' => 'user'));
	$routes['employerregistration'] = new Zend_Controller_Router_Route('employerregistration/', array('controller' => 'Employer','action' => 'employerregistration', 'module' => 'user'));
	$routes['search'] = new Zend_Controller_Router_Route('search/', array('controller' => 'index','action' => 'index', 'module' => 'search'));
    $routes['candidatesearch'] = new Zend_Controller_Router_Route('candidatesearch/', array('controller' => 'index','action' => 'searchcandidate', 'module' => 'search'));
    $routes['fetchbasicsearch'] = new Zend_Controller_Router_Route('fetchbasicsearch/', array('controller' => 'index','action' => 'fetchbasicsearch', 'module' => 'search'));
    $routes['searchcandidatebycategory'] = new Zend_Controller_Router_Route('searchcandidatebycategory/', array('controller' => 'index','action' => 'searchcandidatebycategory', 'module' => 'search'));
    $routes['searchcandidatebyoveallsearch'] = new Zend_Controller_Router_Route('searchcandidatebyoveallsearch/', array('controller' => 'index','action' => 'searchcandidatebyoveallsearch', 'module' => 'search'));
	$routes['jobsuccess'] = new Zend_Controller_Router_Route('jobsuccess/', array('controller' => 'index','action' => 'jobsuccess', 'module' => 'user'));
	$routes['jobdetail'] = new Zend_Controller_Router_Route('jobdetail/', array('controller' => 'index','action' => 'jobdetail', 'module' => 'default'));
	$routes['approvejob'] = new Zend_Controller_Router_Route('approvejob/', array('controller' => 'index','action' => 'approvejob', 'module' => 'user'));
	$routes['jobsearch'] = new Zend_Controller_Router_Route('jobsearch/', array('controller' => 'index','action' => 'searchcandidatebyoveallsearch', 'module' => 'search'));
	$routes['fetchbasicsearchforcheckbox'] = new Zend_Controller_Router_Route('fetchbasicsearchforcheckbox/', array('controller' => 'index','action' => 'fetchbasicsearchforcheckbox', 'module' => 'search'));
	$routes['editagency'] = new Zend_Controller_Router_Route('editagency/', array('controller' => 'agency','action' => 'editagency', 'module' => 'user'));
    $routes['addcandidatebyagency'] = new Zend_Controller_Router_Route('addcandidatebyagency/', array('controller' => 'agency','action' => 'addcandidatebyagency', 'module' => 'user'));
	$routes['jobshortlist'] = new Zend_Controller_Router_Route('jobshortlist/', array('controller' => 'index','action' => 'jobshortlist', 'module' => 'user'));
	$routes['jobapply'] = new Zend_Controller_Router_Route('jobapply/', array('controller' => 'index','action' => 'jobapply', 'module' => 'user'));
	$routes['membership'] = new Zend_Controller_Router_Route('membership/', array('controller' => 'index','action' => 'membership', 'module' => 'user'));
	$routes['employerprofile'] = new Zend_Controller_Router_Route('employerprofile/', array('controller' => 'Employer','action' => 'profile', 'module' => 'user'));
	$routes['candidateinterest'] = new Zend_Controller_Router_Route('candidateinterest/', array('controller' => 'Employer','action' => 'candidateinterest', 'module' => 'user'));
	$routes['candidateshortlist'] = new Zend_Controller_Router_Route('candidateshortlist/', array('controller' => 'Employer','action' => 'candidateshortlist', 'module' => 'user'));
	$routes['myjobs'] = new Zend_Controller_Router_Route('myjobs/', array('controller' => 'Employer','action' => 'myjobs', 'module' => 'user'));
	$routes['interestedcandidates'] = new Zend_Controller_Router_Route('interestedcandidates/', array('controller' => 'Employer','action' => 'interestedcandidates', 'module' => 'user'));
	$routes['viewcandidatesaddedbyagency'] = new Zend_Controller_Router_Route('viewcandidatesaddedbyagency/', array('controller' => 'agency', 'action' => 'viewcandidatesaddedbyagency', 'module' => 'user'));
	$routes['editcandiateaddedbyagency'] = new Zend_Controller_Router_Route('editcandiateaddedbyagency/', array('controller' => 'agency', 'action' => 'editcandiateaddedbyagency', 'module' => 'user'));

	?>
