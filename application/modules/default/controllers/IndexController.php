<?php

class IndexController extends Zend_Controller_Action
{

	public function preDispatch()
	{

		$this->view->pageIdentity = $this->_request->getActionName();

	}

	public function postDispatch(){
	$request = $this->getRequest();
		$return = $request->getParam('return');
		if($return == 1){
			$url = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
			$session = new Zend_Session_Namespace('url');
			$session->redirect = $url;
		}
	}
	
	
	
	
    public function init()
    {
        /* Initialize action controller here */
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{
			$this->view->loggedIn = 1;
			$this->view->usertype = $auth->getIdentity()->usertype;
			}			
			else
			$this->view->loggedIn = 0;
		$this->_searchModel = new Search_Model_Search();	
		$defaultModel = new Default_Model_Datatable();
		$this->view->jobs = $defaultModel->getJobs();
		
		
		$this->view->categories = $defaultModel->getmainCategories();
		$userModel = new User_Model_Users();
		
		
		//$this->view->locations  = $userModel->getLocation();
		$this->view->locations  = $userModel->getCity("'All'");
		$this->view->nationality  = $userModel->getCountries();
		
		$this->view->category  = $userModel->getCategories();
		

		$this->view->experience = array(
		                '' => 'Select Total Experience',
						'Fresher' => 'Fresher',
						'o-1' => '0-1 year',
						'1-2' => '1-2 year',
						'2-3' => '2-3 year',
						'3-5' => '3-5 year',
						'5-10' => '5-10 year',
						'10+' => '10+ years'
		            );


    }

    public function indexAction()
    {
		//$this->_auth = Zend_Auth::getInstance();
		$defaultModel = new Default_Model_Datatable();
		$this->view->candidates = $defaultModel->getCandidates();
		
		$userModel = new User_Model_Users();
		$this->view->skills  = $userModel->getSkills('');
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{	
			$id = $auth->getIdentity()->id;
			$this->view->arrApplied = $this->_searchModel->getAppliedJobsByCandidate($id);				
			
			$this->view->arrShortlisted = $this->_searchModel->getShortlistedJobsByCandidate($id);
		}		
		else{
		$this->view->arrApplied = array();
			$this->view->arrShortlisted = array();
		}
		$this->_helper->layout->setLayout('home-layout');
		
	}

   public function forgotAction()
    {
		if($_POST)
		{
			$userModel = new User_Model_Users();
			$userArr = $userModel->getUserByUsername($_POST['username']);
			
			$number = trim($userArr[0]['mobile']);
			$number = str_replace("-","",$number);
			$number = str_replace(" ","",$number);

			$message = "Dear Member Your Fastest Hiring password is : ".$userArr[0]['password'];
	
			$userModel->sendsms($number,$message);
			$this->view->st = 1;
		}
		
		$this->_helper->layout->setLayout('layout');		
	}

	public function jobdetailAction()
    {
		//$this->_auth = Zend_Auth::getInstance();
		$defaultModel = new Default_Model_Datatable();
		$param = $this->getRequest();
        $jobId = $param->getParam('id');
		
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{	
			$id = $auth->getIdentity()->id;
			$this->view->arrApplied = $this->_searchModel->getAppliedJobsByCandidate($id);				
			$this->view->arrShortlisted = $this->_searchModel->getShortlistedJobsByCandidate($id);
		}		
		$this->view->verify = $param->getParam('verify');
       
		$arr = $defaultModel->getJobDetail($jobId);
		$this->view->jobs = $arr[0];
		
		if($arr[0]['job_status']!=1 && $_REQUEST['admin']!="adminpage" && $arr[0]['employer_id']!=$auth->getIdentity()->id)
			header('Location: index.php');
		
		$this->_helper->layout->setLayout('noleft');
		
	}
	
	
	public function contactAction()
    {
		$defaultModel = new Default_Model_Datatable();
		$param = $this->getRequest();
        
		
		if($_POST['submit3'] && (trim(strtolower($_POST['captcha'])) == $_SESSION['captcha']))
		{
			$name= $_POST['name'];
			$email = $_POST['emailid'];
			$phone = $_POST['phone'];
			$category = $_POST['category'];
			$priority = $_POST['priority'];
			$suggestions = $_POST['suggestions'];
			
			$message = "Hi Admin, <br><br> A new user wants to give you a feedback. Please find teh detaile below - <br><br>";
			$message .= "<table><tr><td>name</td><td>".$name."</td></tr>";
			$message .= "<tr><td>Email</td><td>".$email."</td></tr>";
			$message .= "<tr><td>Phone</td><td>".$phone."</td></tr>";
			$message .= "<tr><td>Category</td><td>".$category."</td></tr>";
			$message .= "<tr><td>Priority</td><td>".$priority."</td></tr>";
			$message .= "<tr><td>Suggestions</td><td>".$suggestios."</td></tr></table>";
			$message .= "<br><br>Thanks,<br>Support Team<br>Fastest Hiring";
			
			$userModel = new User_Model_Users();
			
			$userModel->sendEmail('support@fastesthiring.com','Feedback Received - Fastest Hiring',$message);
			$this->_redirect(WEBSITE."contact?st=sent#tab3");
			
		}
		if($_POST['submit2'] && (trim(strtolower($_POST['captcha1'])) == $_SESSION['captcha']))
		{
			$category = $_POST['category'];
			$subject= $_POST['subject'];
			$details = $_POST['details'];
			$evidence = $_POST['evidence'];
			$email = $_POST['email'];
			
			$message = "Hi Admin, <br><br> A new user wants to give you a abuse report. Please find the details below - <br><br>";
			$message .= "<table><tr><td>Subject</td><td>".$subject."</td></tr>";
			$message .= "<tr><td>Email</td><td>".$email."</td></tr>";
			$message .= "<tr><td>Details</td><td>".$details."</td></tr>";
			$message .= "<tr><td>Category</td><td>".$category."</td></tr>";
			$message .= "<tr><td>Evidence</td><td>".$evidence."</td></tr>";
			$message .= "</table><br><br>Thanks,<br>Support Team<br>Fastest Hiring";
		
			$userModel = new User_Model_Users();
			
			$userModel->sendEmail('support@fastesthiring.com','Report Abuse - Fastest Hiring',$message);
			$this->_redirect(WEBSITE."contact?st=sent#tab2");
			
		}
		$this->_helper->layout->setLayout('home-layout');	
	}
	
	public function pagedetailAction()
	{
				
					$defaultModel = new Default_Model_Datatable();
					$this->view->content = $defaultModel->getPageDetailById($_REQUEST['id']);
		$this->_helper->layout->setLayout('home-layout');			
	}
}