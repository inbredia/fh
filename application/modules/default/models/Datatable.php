<?php

class Default_Model_Datatable extends Zend_Db_Table_Abstract
{


	public function getJobs()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('j'=>'jobs'),array('*'))
				->where('j.status =1')
				->order('id desc')
				->limit('6');
		$select = $db->fetchAll($query);
		return $select;

	}
	public function getCandidates()
	{
		$db = $this->getAdapter();
		$query = $db->select()
					->from(array('u'=>'users'),array('*'))
					->joinLeft(array('c'=>'candidate'),'u.id = c.user_id')
					->where('u.usertype = 0')
					->where('u.status = 1')
					->order('u.id desc')
					->limit('8');

		$select = $db->fetchAll($query);
		return $select;
	}

	public function getmainCategories()
	{
		$db = $this->getAdapter();
		$query = $db->select()
					->from(array('c'=>'category'),array('*'))
					->where('c.parent_category_id = 0');

		$select = $db->fetchAll($query);
		return $select;
	}

	public function getJobsCount()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('j'=>'jobs'),array('j.title'))
				->where('j.status = 1');
		$select = $db->fetchAll($query);
		return count($select);

	}

	public function getJobDetail($id)
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('j'=>'jobs'),array('j.*','j.id as job_id','j.status as job_status'))
				->joinLeft(array('u'=>'users'),'u.id = j.employer_id')
				->joinLeft(array('e'=>'employer'),'e.user_id = j.employer_id')
				->joinLeft(array('a'=>'agency'),'u.id = a.user_id')
				->where('j.id='.$id);

		$select = $db->fetchAll($query);
		return $select;


	}

	//function to return the pagination string
function getPaginationString($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "?page=")
{
	//defaults
	if(!$adjacents) $adjacents = 1;
	if(!$limit) $limit = 15;
	if(!$page) $page = 1;
	if(!$targetpage) $targetpage = "/";

	//other vars
	$prev = $page - 1;									//previous page is page - 1
	$next = $page + 1;									//next page is page + 1
	$lastpage = ceil($totalitems / $limit);				//lastpage is = total items / items per page, rounded up.
	$lpm1 = $lastpage - 1;								//last page minus 1

	/*
		Now we apply our rules and draw the pagination object.
		We're actually saving the code to a variable in case we want to draw it more than once.
	*/
	$pagination = "";
	if($lastpage > 1)
	{
		$pagination .= "<div class=\"pagination\"";
		if($margin || $padding)
		{
			$pagination .= " style=\"";
			if($margin)
				$pagination .= "margin: $margin;";
			if($padding)
				$pagination .= "padding: $padding;";
			$pagination .= "\"";
		}
		$pagination .= ">";

		//previous button
		if ($page > 1)
			$pagination .= "<a href=\"$targetpage$pagestring$prev\">« prev</a>";
		else
			$pagination .= "<span class=\"disabled\">« prev</span>";

		//pages
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination .= "<span class=\"current\">$counter</span>";
				else
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";
			}
		}
		elseif($lastpage >= 7 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 3))
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination .= "<span class=\"current\">$counter</span>";
					else
						$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";
				}
				$pagination .= "<span class=\"elipses\">...</span>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>";
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>";
				$pagination .= "<span class=\"elipses\">...</span>";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination .= "<span class=\"current\">$counter</span>";
					else
						$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";
				}
				$pagination .= "...";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>";
			}
			//close to end; only hide early pages
			else
			{
				$pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>";
				$pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>";
				$pagination .= "<span class=\"elipses\">...</span>";
				for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination .= "<span class=\"current\">$counter</span>";
					else
						$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";
				}
			}
		}

		//next button
		if ($page < $counter - 1)
			$pagination .= "<a href=\"" . $targetpage . $pagestring . $next . "\">next »</a>";
		else
			$pagination .= "<span class=\"disabled\">next »</span>";
		$pagination .= "</div>\n";
	}

	return $pagination;

	}


	    public function getPageDetailById($id) {
	        $db = $this->getAdapter();
	        $query = $db->select()
	                ->from(array('c' => 'tbl_content'), array('*'))
	                ->where('id = ?', $id);

	        $select = $db->fetchRow($query);
	        return $select;
    }

}