<?php

class Search_IndexController extends Zend_Controller_Action {

    public function preDispatch() {

        $this->view->pageIdentity = $this->_request->getActionName();
    }

   public function postDispatch(){
	$request = $this->getRequest();
		$return = $request->getParam('return');
		if (!$this->getRequest()->isXmlHttpRequest()) {
		
			$url = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
			$session = new Zend_Session_Namespace('url');
			$session->redirect = $url;
		}
	}

    /**
     *
     */
    public function init() {
        $this->view->pageName = "search";
$this->view->pageurl = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();	
        $this->_searchModel = new Search_Model_Search();
        $userModel = new User_Model_Users();
        $this->view->locations = $userModel->getLocation();
        $this->view->nationality = $userModel->getCountries();
        $this->view->category = $userModel->getCategories();
        $this->view->jobroles = $userModel->getRoles(0);
        $this->view->jobLocations = $userModel->getJobLocationCity();
		 $this->view->jobLocationCountry = $userModel->getJobLocation();
		//$this->view->jobLocations = $userModel->getcity('All');
		$this->view->languages = $userModel->getlanguages();
//        echo "<pre>";
//        print_r($this->view->locations); exit;

        $this->view->experience = array(
            'Fresher' => 'Fresher',
            '0-1' => '0-1 year',
            '1-2' => '1-2 year',
            '2-3' => '2-3 year',
            '3-5' => '3-5 year',
            '5-10' => '5-10 year',
            '10+' => '10+ years'
        );

		
         $this->view->language = array(
            'Spanish' => 'Spanish',
            'Mandarin' => 'Mandarin',
             'English' => 'English',
             'Hindi' => 'Hindi',
             'Portugues' => 'Portugues',
             'Arabic' => 'Arabic',
             'Bengali' => 'Bengali',
             'Japanese' => 'Japanese',
             'Turkish' => 'Turkish',
             'Italian' => 'Italian',
             'Greek' => 'Greek'

        );
		$this->view->language = $userModel->getlanguages();
		//echo"<pre>";print_r($this->view->language);exit;
           $this->view->religion = array(
            'Hindu' => 'Hindu',
            'Muslim' => 'Muslim',
             'Christian' => 'Christian',
             'Sikh' => 'Sikh',
             'Parsi' => 'Parsi',
             'Jain' => 'Jain',
             'Buddhist' => 'Buddhist',
             'Jewish' => 'Jewish',

        );


        $this->_helper->layout->setLayout('search');

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->view->loggedIn = 1;
            $uid = $auth->getIdentity()->id;
            $this->view->usertype = $auth->getIdentity()->usertype;
            if ($auth->getIdentity()->usertype == 0) {
                $this->view->arrApplied = $this->_searchModel->getAppliedJobsByCandidate($uid);
                $this->view->arrShortlisted = $this->_searchModel->getShortlistedJobsByCandidate($uid);
            }
            if ($auth->getIdentity()->usertype == 3) {

                $this->view->arrApplied = $this->_searchModel->getAppliedJobsByAgent($uid);
                $this->view->arrShortlisted = $this->_searchModel->getShortlistedJobsByAgent($uid);
            }
        }
        else
            $this->view->loggedIn = 0;
    }

    /**
     *
     */
    public function searchcandidateAction() {

        $request = $this->getRequest();
        $post = $request->getQuery();

        $this->view->postData = $post;

//        echo "<pre>";
//        print_r($post); exit;

        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $eid = $auth->getIdentity()->id;
            if ($auth->getIdentity()->usertype == 2 || $auth->getIdentity()->usertype == 3)
                $this->view->arrInterested = $this->_searchModel->getInterestedCandidatesByEmployer($eid);
            $this->view->arrShortlisted = $this->_searchModel->getShortlistedCandidatesByEmployer($eid);
        }

        // Paging Logic

        $page = 1; //Default page
        $limit = 10; //Records per page
        $start = 0; //starts displaying records from 0
        if (isset($_GET['page']) && $_GET['page'] != '') {
            $page = $_GET['page'];
        }


        $start = ($page - 1) * $limit;

        $this->view->limit = $limit;
        $this->view->start = $start;
        $this->view->page = $page;
        $textString = explode("&page=", $_SERVER['QUERY_STRING']);
        $this->view->searchDataString = $textString[0];

        $resultArr = $this->_searchModel->searchcandidateDataIndex($post, $this->view->start, $this->view->limit);
        $resultcount = $this->_searchModel->searchcandidateDataIndexCount($post);

	
        $this->view->searchData = $resultArr;
        $this->view->searchDataCount = $resultcount;

        $this->render('index');
    }

    public function advancesearchcandidatesAction() {
        $this->_helper->layout->setLayout('home-layout');
        $userModel = new User_Model_Users();
        $this->view->agents = $userModel->getAgents();

        $this->render('advancesearchcandidates');
    }

    public function advancesearchjobsAction() {
        $this->_helper->layout->setLayout('home-layout');


        $userModel = new User_Model_Users();
        $this->view->company = $userModel->getEmployers();

        $this->render('advancesearchjobs');
    }

    /**
     * Search the data by post string
     */
    public function jobsearchbypostdataAction() {


        $request = $this->getRequest();
        $post = $request->getQuery();
		
        $this->view->postData = $post;

        // Paging Logic
        $page = 1; //Default page
        $limit = 10; //Records per page
        $start = 0; //starts displaying records from 0
        if (isset($_GET['page']) && $_GET['page'] != '') {
            $page = $_GET['page'];
        }

        $start = ($page - 1) * $limit;

        $this->view->limit = $limit;
        $this->view->start = $start;
        $this->view->page = $page;

        $textString = explode("&page=", $_SERVER['QUERY_STRING']);
        $this->view->searchDataString = $textString[0];


        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $eid = $auth->getIdentity()->id;
            if ($auth->getIdentity()->usertype == 2) {
                $this->view->arrApplied = $this->_searchModel->getInterestedCandidatesByEmployer($eid);
                $this->view->arrShortlisted = $this->_searchModel->getShortlistedCandidatesByEmployer($eid);
            }
        } else {
            $this->view->loggedIn = 0;
        }

        $resultArr = $this->_searchModel->searchjobDataIndex($post, $this->view->start, $this->view->limit);
        $resultcount = $this->_searchModel->searchjobDataIndexCount($post);

        $this->view->searchData = $resultArr;
        $this->view->searchDataCount = $resultcount;
        $this->render('searchjobs');
    }

    /**
     *
     */
    public function searchcandidatebyoveallsearchAction() {

        $request = $this->getRequest();
        //$post = $request->getPost();
        $post = $request->getQuery();
        $this->view->postData = $post;

//echo "<pre>";
//print_r($post); exit;
        // Paging Logic

        $page = 1; //Default page
        $limit = 10; //Records per page
        $start = 0; //starts displaying records from 0
        if (isset($_GET['page']) && $_GET['page'] != '') {
            $page = $_GET['page'];
        }

        $start = ($page - 1) * $limit;

        $this->view->limit = $limit;
        $this->view->start = $start;
        $this->view->page = $page;

        $textString = explode("&page=", $_SERVER['QUERY_STRING']);
        $this->view->searchDataString = $textString[0];


        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $eid = $auth->getIdentity()->id;
            if ($auth->getIdentity()->usertype == 2 || $auth->getIdentity()->usertype == 3) {
                $this->view->arrInterested = $this->_searchModel->getInterestedCandidatesByEmployer($eid);
                $this->view->arrShortlisted = $this->_searchModel->getShortlistedCandidatesByEmployer($eid);
            } else
            if ($auth->getIdentity()->usertype == 0) {
                $this->view->arrApplied = $this->_searchModel->getAppliedJobsByCandidate($eid);

                $this->view->arrShortlisted = $this->_searchModel->getShortlistedJobsByCandidate($eid);
            }
        } else {
            $this->view->loggedIn = 0;
        }

        if (isset($post['search']) && $post['search'] == 'candidate') {
            $resultArr = $this->_searchModel->searchcandidateDataByOverallSearch($post, $this->view->start, $this->view->limit);
            $resultcount = $this->_searchModel->searchcandidateDataByOverallSearchCount($post);
            $this->view->searchData = $resultArr;
            $this->view->searchDataCount = $resultcount;
            $this->render('index');
        } else {

            if (isset($post['searchdata'])) {
                $resultArr = $this->_searchModel->searchjobDataByOverallSearch($post, $this->view->start, $this->view->limit);
                $resultcount = $this->_searchModel->searchjobDataByOverallSearchCount($post);
            }

            else {
                $resultArr = $this->_searchModel->searchjobDataIndex($post, $this->view->start, $this->view->limit);
                $resultcount = $this->_searchModel->searchjobDataIndexCount($post);
            }

            $this->view->searchData = $resultArr;
            $this->view->searchDataCount = $resultcount;
            $this->render('searchjobs');
        }
    }

    /**
     *
     */
    public function fetchbasicsearchforjobbycheckboxAction() {

        $this->_helper->layout->disableLayout();

        $request = $this->getRequest();
        $post = $request->getQuery();


        // Paging Logic

        $page = 1; //Default page
        $limit = 10; //Records per page
        $start = 0; //starts displaying records from 0
        if (isset($_GET['page']) && $_GET['page'] != '') {
            $page = $_GET['page'];
        }

        $start = ($page - 1) * $limit;

        $this->view->limit = $limit;
        $this->view->start = $start;
        $this->view->page = $page;

        $textString = explode("&page=", $_SERVER['QUERY_STRING']);

        $this->view->searchDataString = $textString[0];


        $resultArr = $this->_searchModel->searchjobDataIndex($post, $this->view->start, $this->view->limit);
        $resultcount = $this->_searchModel->searchjobDataIndexCount($post);

        $this->view->searchData = $resultArr;
        $this->view->searchDataCount = $resultcount;
		
		$auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $eid = $auth->getIdentity()->id;
            if ($auth->getIdentity()->usertype == 2) {
                $this->view->arrApplied = $this->_searchModel->getInterestedCandidatesByEmployer($eid);
                $this->view->arrShortlisted = $this->_searchModel->getShortlistedCandidatesByEmployer($eid);
            }
        } else {
            $this->view->loggedIn = 0;
        }

    }

    /**
     *
     */
    public function fetchbasicsearchforcheckboxAction() {

        $this->_helper->layout->disableLayout();

        $request = $this->getRequest();
        $post = $request->getQuery();


        // Paging Logic

        $page = 1; //Default page
        $limit = 10; //Records per page
        $start = 0; //starts displaying records from 0
        if (isset($_GET['page']) && $_GET['page'] != '') {
            $page = $_GET['page'];
        }

		
		
        $start = ($page - 1) * $limit;

        $this->view->limit = $limit;
        $this->view->start = $start;
        $this->view->page = $page;

        $textString = explode("&page=", $_SERVER['QUERY_STRING']);

        $this->view->searchDataString = $textString[0];

        $resultArr = $this->_searchModel->searchcandidateDataIndex($post, $this->view->start, $this->view->limit);
        $resultcount = $this->_searchModel->searchcandidateDataIndexCount($post);

        $this->view->searchData = $resultArr;
        $this->view->searchDataCount = $resultcount;
		
		    $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $eid = $auth->getIdentity()->id;
            if ($auth->getIdentity()->usertype == 2 || $auth->getIdentity()->usertype == 3)
                $this->view->arrInterested = $this->_searchModel->getInterestedCandidatesByEmployer($eid);
            $this->view->arrShortlisted = $this->_searchModel->getShortlistedCandidatesByEmployer($eid);
        }else {
            $this->view->loggedIn = 0;
        }

    }

    /**
     * to be remove by serach candidate function
     */
    public function searchcandidatebycategoryAction() {

        $request = $this->getRequest();

        $get = $request->getParam('category');


        $this->view->postData = $get;

        // Paging Logic

        $page = 1; //Default page
        $limit = 10; //Records per page
        $start = 0; //starts displaying records from 0
        if (isset($_GET['page']) && $_GET['page'] != '') {
            $page = $_GET['page'];
        }


        $start = ($page - 1) * $limit;

        $this->view->limit = $limit;
        $this->view->start = $start;
        $this->view->page = $page;
        $textString = explode("&page=", $_SERVER['QUERY_STRING']);
        $this->view->searchDataString = $textString[0];



        $resultArr = $this->_searchModel->searchcandidateDataByCategory($get, $this->view->start, $this->view->limit);
        $resultcount = $this->_searchModel->searchcandidateDataByCategoryCount($get);

        $this->view->searchData = $resultArr;
        $this->view->searchDataCount = $resultcount;

        $this->render('index');
    }

}
