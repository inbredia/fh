<?php

class Search_Form_Basicsearch extends Zend_Form {

    public function init() {
        // Set the method for the display form to POST
        $this->setMethod('post');

        // This is for Agency Name
        $this->addElement('text', 'agency_name', array(
            'label' => '*Agency Name:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

        // This is for Contact Person Name
        $this->addElement('text', 'contact_person_name', array(
            'label' => '*Contact Person Name:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

        // This is for desired username Field
        $this->addElement('text', 'username', array(
            'label' => '*Username:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20)),
                array('Db_NoRecordExists', true, array(
                        'table' => 'users',
                        'field' => 'username',
                        'messages' => array(
                            'recordFound' => 'Username already taken'
                        )
                    )
                )
            )
        ));

        // This is for Password 
        $this->addElement('password', 'password', array(
            'label' => '*Password:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20))
            )
        ));

        // This is for Confirm Password
        $this->addElement('password', 'repassword', array(
            'label' => '*Confirm Password:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20))
            )
        ));

        // This is for Email Address
        $this->addElement('text', 'email', array(
            'label' => '*Email address:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                'EmailAddress',
                array('Db_NoRecordExists', true, array(
                        'table' => 'users',
                        'field' => 'email',
                        'messages' => array(
                            'recordFound' => 'Email already taken'
                        )
                    )
                )
            )
        ));

        // This is for country field
        $this->addElement('select', 'country', array(
            'multiOptions' =>
            array('0' => 'Select','UAE' => 'United Arab Emirates', 'Oman' => 'Oman', 'Qatar' => 'Qatar', 'Bahrain' => 'Bahrain', 'Kuwait' => 'Kuwait', 'Saudi Arabia' => 'Saudi Arabia'
            ),
            'setValue' => '0',
            'required' => true,
            'label' => '*Country:',
        ));

        // This is for Mobile
        $this->addElement('text', 'mobile', array(
            'label' => '*Mobile:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));


        // This is for Fax
        $this->addElement('text', 'fax', array(
            'label' => 'Fax:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

        //This is for phone 
        $this->addElement('text', 'phone', array(
            'label' => 'Phone:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

        // This is for Agency Website
         $this->addElement('text', 'agency_website', array(
            'label' => 'Agency Website:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

        // This is for About Agency
        $this->addElement('text', 'about_agency', array(
            'label' => 'About Agency:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
    }

}