<?php
class User_Model_Form_Element_Categoryselect extends Zend_Form_Element_MultiSelect
{
    public function init()
    {
        $oCountryTb = new User_Model_Users();
       // $this->addMultiOption(0, 'Please select...');
		$this->setRequired(true);
		
		
		$this->class="select margin-topZ";
        foreach ($oCountryTb->getCategories() as $oCountry) {
            $this->addMultiOption($oCountry['category']."-".$oCountry['id'], $oCountry['category']);
        }
    
	}
}