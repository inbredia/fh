<?php
class User_Model_Form_Element_Cityselect extends Zend_Form_Element_MultiSelect
{
    public function init()
    {
        $oCountryTb = new User_Model_Users();
        $this->addMultiOption(0, 'Please select a city...');
		$this->setRegisterInArrayValidator(false);
		
		$this->class="select margin-topZ";
        foreach ($oCountryTb->getSubCategories() as $oCountry) {
            $this->addMultiOption($oCountry['category'], $oCountry['category']);
        }
    
	}
}