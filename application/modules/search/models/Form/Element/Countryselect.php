<?php
class User_Model_Form_Element_Countryselect extends Zend_Form_Element_Select
{
    public function init()
    {
        $oCountryTb = new User_Model_Users();
        $this->addMultiOption(0, 'Please select...');
		$this->setRequired(true);
		
		$this->class="select margin-topZ";
        foreach ($oCountryTb->getCountries() as $oCountry) {
            $this->addMultiOption($oCountry['nicename'], $oCountry['nicename']);
        }
    
	}
}