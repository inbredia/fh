<?php
class User_Model_Form_Element_Languageselect extends Zend_Form_Element_MultiSelect
{
    public function init()
    {
        $oLocationTb = new User_Model_Users();
        
		$this->setRequired(true);
		
		$this->class="select margin-topZ";
        foreach ($oLocationTb->getLanguages() as $oCountry) {
            $this->addMultiOption($oCountry['language'], $oCountry['language']);
        }
    
	}
}