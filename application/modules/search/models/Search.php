<?php

class Search_Model_Search extends Default_Model_Datatable {

    protected $_name = 'users';

    function getInterestedCandidatesByEmployer($eid) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('a' => 'apply_candidate'), array('*'))
                ->where('a.employer_id =' . $eid);

        $select = $db->fetchAll($query);
        $arr = array();
        foreach ($select as $s) {
            $arr[] = $s['candidate_id'];
        }
        return $arr;
    }

    function getShortlistedCandidatesByEmployer($eid) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('a' => 'shortlist_candidate'), array('*'))
                ->where('a.employer_id =' . $eid);

        $select = $db->fetchAll($query);
        $arr = array();
        foreach ($select as $s) {
            $arr[] = $s['candidate_id'];
        }return $arr;
    }

    function getShortlistedCandidates($eid) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('a' => 'shortlist_candidate'), array('*'))
                ->joinLeft(array('u' => 'users'), 'u.id = a.candidate_id', array('*'))
                ->joinLeft(array('c' => 'candidate'), 'u.id = c.user_id', array('*'))
                ->where('a.employer_id =' . $eid)
                ->order('a.posted_date desc');

        $select = $db->fetchAll($query);
        return $select;
    }

    function getAppliedCandidates($eid) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('a' => 'apply_candidate'), array('*'))
                ->joinLeft(array('u' => 'users'), 'u.id = a.candidate_id', array('*'))
                ->joinLeft(array('c' => 'candidate'), 'u.id = c.user_id', array('*'))
				->joinLeft(array('uc' => 'users'), 'uc.id = c.addedby', array('uc.email as agencyemail'))
				->joinLeft(array('ag' => 'agency'), 'ag.user_id = c.addedby', array('ag.mobile as agencymobile'))
                ->where('a.employer_id =' . $eid)
                ->order('a.posted_date desc');

        $select = $db->fetchAll($query);
        return $select;
    }

    function getShortlistedJobsByCandidate($uid) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('a' => 'shortlist_jobs'), array('*'))
                ->where('a.candidate_id =' . $uid);

        $select = $db->fetchAll($query);
        $arr = array();
        foreach ($select as $s) {
            $arr[] = $s['job_id'];
        }return $arr;
    }

    function getAppliedJobsByCandidate($uid) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('a' => 'apply_jobs'), array('*'))
                ->where('a.candidate_id =' . $uid);

        $select = $db->fetchAll($query);
        $arr = array();
        foreach ($select as $s) {
            $arr[] = $s['job_id'];
        }return $arr;
    }

    function getShortlistedJobsByAgent($uid) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('a' => 'shortlist_jobs'), array('*'))
                ->where("a.candidate_id IN (Select user_id from candidate where addedby=$uid)");

        $select = $db->fetchAll($query);
        $arr = array();
        foreach ($select as $s) {
            $arr[] = $s['job_id'];
        }return $arr;
    }

    function getAppliedJobsByAgent($uid) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('a' => 'apply_jobs'), array('*'))
                ->where("a.candidate_id IN (Select user_id from candidate where addedby=$uid)");

        $select = $db->fetchAll($query);
        $arr = array();
        foreach ($select as $s) {
            $arr[] = $s['job_id'];
        }return $arr;
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchcandidateDataIndex($post, $start, $limit) {

 foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}

      $where = ' ';
           $db = $this->getAdapter();
           $query = $db->select()
                   ->from(array('c' => 'candidate'), array('*'))
                   ->join(array('u' => 'users'), 'u.id = c.user_id', array('*'))
				  ;


           if (!empty($post['agentname'])) {
               $query->join(array('a' => 'agency'), 'a.user_id = c.addedby', array('a.agency_name','a.mobile'));
               $where .= " and a.agency_name LIKE '%{$post['agentname']}%'";
           }
           if (!empty($post['posted_by']))
               $where .= " and key_skills LIKE '%{$post['posted_by']}%'";

           if (!empty($post['skills']) && $post['skills'] != 'Type Job Position, Designation')
               {
				$skillsArr = explode(',',$post['skills']);
				$where .= " and ("; 
				$i=0;
				foreach($skillsArr as $v)
					{
					$v=trim($v);
					if($i++ >0)$where.=" or ";
					$where .= " ((key_skills LIKE '%{$v}%') or (role_id LIKE '%{$v}%') ) ";	
					}
				$where .=")";
				}
				
				//$where .= " and ((key_skills LIKE '%{$post['skills']}%') or (role_id LIKE '%{$post['skills']}%') )";
				
				


          // if (!empty($post['gender']))
          //     $where .= " and gender LIKE '%{$post['gender']}%'";


           if (isset($post['gender']) && !empty($post['gender'])) {

               $genderArr = explode(",", $post['gender']);
   			if($genderArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($genderArr as $gender) {
                   $gender = trim($gender);
				   if ($i == 0)
                       $where .= " gender = '{$gender}'";
                   else
                       $where .= " or gender = '{$gender}'";
                   $i++;
               }

               $where .= " ) ";
           }
           }



           if (isset($post['nationality']) && !empty($post['nationality'])) {

               $nationalityArr = explode(",", $post['nationality']);
			   
			 if(trim($nationalityArr[0]) != "Doesn`t Matter")  {
   
   if($nationalityArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($nationalityArr as $nationality) {
                   
				   $nationality=trim($nationality);
				   if ($i == 0)
                       $where .= " nationality LIKE '%{$nationality}%'";
                   else
                       $where .= " or nationality LIKE '%{$nationality}%'";
                   $i++;
               }

               $where .= " ) ";
           }
           }}


           if (isset($post['location']) && !empty($post['location'])) {

               $locationArr = explode(",", $post['location']);
   if($locationArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($locationArr as $location) {
                   $location = trim($location);
				   if ($i == 0)
                       $where .= " current_location LIKE '%{$location}%'";
                   else
                       $where .= " or current_location LIKE '%{$location}%'";
                   $i++;
               }

               $where .= " ) ";
           }
           }

           if (isset($post['job_roles']) && !empty($post['job_roles'])) {

               $rolesArr = explode(",", $post['job_roles']);
			if($rolesArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($rolesArr as $roles) {
			   $roles = trim($roles);
                   if ($i == 0)
                       $where .= " role_id LIKE '%{$roles}%'";
                   else
                       $where .= " or role_id LIKE '%{$roles}%'";
                   $i++;
               }
               $where .= " ) ";
           }
           }

           if (isset($post['salary']) && !empty($post['salary'])) {

               $salaryArr = explode(",", $post['salary']);
   if($salaryArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($salaryArr as $sal) {
                   $sal=trim($sal);
				   if ($i == 0)
                       $where .= " expected_salary LIKE '%{$sal}%'";
                   else
                       $where .= " or expected_salary LIKE '%{$sal}%'";
                   $i++;
               }
               $where .= " ) ";
           }
           }

           if (isset($post['experience']) && !empty($post['experience'])) {

               $experienceArr = explode(",", $post['experience']);
   if($experienceArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($experienceArr as $experience) {
                   $exp = explode(' ',$experience);
				   $experience = $exp[0];
				   if ($i == 0)
                       $where .= " experience LIKE '%{$experience}%'";
                   else
                       $where .= " or experience LIKE '%{$experience}%'";
                   $i++;
               }
               $where .= " ) ";
           }
           }

           if (isset($post['religion']) && !empty($post['religion'])) {

               $religionArr = explode(",", $post['religion']);
   if($religionArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($religionArr as $religion) {
                   $religion=trim($religion);
				   if ($i == 0)
                       $where .= " religion LIKE '%{$religion}%'";
                   else
                       $where .= " or religion LIKE '%{$religion}%'";
                   $i++;
               }
               $where .= " ) ";
           }
           }


           if (!empty($post['weight']))
               $where .= " and weight LIKE '%{$post['weight']}%'";

           if (isset($post['language']) && !empty($post['language'])) {

               $languageArr = explode(",", $post['language']);
			   
   if($languageArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($languageArr as $lan) {
			   $language = trim($lan);
                   
				   if ($i == 0)
                       $where .= " language LIKE '%{$language}%'";
                   else
                       $where .= " or language LIKE '%{$language}%'";
                   $i++;
               }
               $where .= " ) ";
           }
           }


           if (isset($post['maritalstatus']) && !empty($post['maritalstatus'])) {

               $maritalstatusArr = explode(",", $post['maritalstatus']);
   if($maritalstatusArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($maritalstatusArr as $maritalstatus) {
			   $maritalstatus=trim($maritalstatus);
                   if ($i == 0)
                       $where .= " marital_status LIKE '%{$maritalstatus}%'";
                   else
                       $where .= " or marital_status LIKE '%{$maritalstatus}%'";
                   $i++;
               }
               $where .= " ) ";
           }
           }

           if (isset($post['category']) && !empty($post['category'])) {

               $categoryArr = explode(",", $post['category']);
   if($categoryArr[0] != 'All'){
               $where .= " and ( ";
               $i = 0;
               foreach ($categoryArr as $category) {
			   $category=trim($category);
                   if ($i == 0)
                       $where .= " job_category LIKE '%{$category}%'";
                   else
                       $where .= " or job_category LIKE '%{$category}%'";
                   $i++;
               }
               $where .= " ) ";
           }
           }

           	if (isset($post['postedby']) && !empty($post['postedby'])) {

   				            $postedArr = explode(",", $post['postedby']);
   if($postedArr[0] != 'All'){
   				            $where .= " and ( ";
   				            $i = 0;
   				            foreach ($postedArr as $posted) {
   				                $posted=trim($posted);
								if ($i == 0){

   				                    if($posted == 'Self'){
   				                        $where .= " addedby = 0 ";
   				                    }else{
   				                        $where .= " addedby <> 0 ";
   				                    }


   				                }
   				                else{

   				                     if($posted == 'Self'){
   				                        $where .= " or addedby = 0 ";
   				                    }else{
   				                        $where .= " or addedby <> 0 ";
   				                    }


   				                }
   				                $i++;
   				            }
   				            $where .= " ) ";
   		        }
   		        }
   		     if (isset($post['jobtype']) && !empty($post['jobtype'])) {

   		            $jobtypeArr = explode(",", $post['jobtype']);
   		if($jobtypeArr[0] != 'All'  && $jobtypeArr[0] != 'Doesn`t Matter'){
   		            $where .= " and ( ";
   		            $i = 0;
   		            foreach ($jobtypeArr as $jobtype) {
					$jobtype=trim($jobtype);
   		                if ($i == 0)
   		                    $where .= " job_type LIKE '%{$jobtype}%'";
   		                else
   		                    $where .= " or job_type LIKE '%{$jobtype}%'";
   		                $i++;
   		            }
					$where .= " or job_type LIKE '%Any%'";
   		            $where .= " ) ";
   		  }
           }


           if (!empty($post['username']))
            $where .= " and u.username LIKE '%{$post['username']}%'";

        $where .= " and u.status=1";

		$query->joinLeft(array('au' => 'agency'), 'au.user_id = c.addedby', array('au.mobile as agencymobile'));
		$query->joinLeft(array('us' => 'users'), 'us.id = c.addedby', array('us.email as agencyemail'));
			
		 
			

		
        $query->where(" 1 = 1 {$where} ");
        $query->limit($limit, $start);
        $query->order(array("u.featured_candidate desc","u.id desc"));

       // echo $query;exit;

        $select = $db->fetchAll($query);

        return $select;
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchcandidateDataIndexCount($post) {

        foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}
		
		$where = ' ';
	              $db = $this->getAdapter();
	              $query = $db->select()
	                      ->from(array('c' => 'candidate'), array('*'))
	                      ->join(array('u' => 'users'), 'u.id = c.user_id', array('*'));


	              if (!empty($post['agentname'])) {
	                  $query->join(array('a' => 'agency'), 'a.user_id = c.addedby', array('*'));
	                  $where .= " and agency_name LIKE '%{$post['agentname']}%'";
	              }
	             // if (!empty($post['posted_by']))
	               //   $where .= " and ((key_skills LIKE '%{$post['posted_by']}%') or (role_id LIKE '%{$post['posted_by']}%') )";

	              //if (!empty($post['skills']) && $post['skills'] != 'Type Job Position, Designation')
	                //  $where .= " and ((key_skills LIKE '%{$post['skills']}%') or (role_id LIKE '%{$post['skills']}%') )";
					
					 if (!empty($post['skills']) && $post['skills'] != 'Type Job Position, Designation')
               {
				$skillsArr = explode(',',$post['skills']);
				$where .= " and ("; 
				$i=0;
				foreach($skillsArr as $v)
					{
					$v=trim($v);
					if($i++ >0)$where.=" or ";
					$where .= " ((key_skills LIKE '%{$v}%') or (role_id LIKE '%{$v}%') ) ";	
					}
				$where .=")";
				}

	             // if (!empty($post['gender']))
	             //     $where .= " and gender LIKE '%{$post['gender']}%'";


	              if (isset($post['gender']) && !empty($post['gender'])) {

	                  $genderArr = explode(",", $post['gender']);
	      			if(trim($genderArr[0]) != "Doesn`t Matter")  
						if($genderArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($genderArr as $gender) {
					  $gender=trim($gender);
	                      if ($i == 0)
	                          $where .= " gender = '{$gender}'";
	                      else
	                          $where .= " or gender = '{$gender}'";
	                      $i++;
	                  }

	                  $where .= " ) ";
	              }
	              }









	              if (isset($post['nationality']) && !empty($post['nationality'])) {

	                  $nationalityArr = explode(",", $post['nationality']);
	      if(trim($nationalityArr[0]) != "Doesn`t Matter")  
		  if($nationalityArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($nationalityArr as $nationality) {
					  $nationality=trim($nationality);
	                      if ($i == 0)
	                          $where .= " nationality LIKE '%{$nationality}%'";
	                      else
	                          $where .= " or nationality LIKE '%{$nationality}%'";
	                      $i++;
	                  }

	                  $where .= " ) ";
	              }
	              }


	              if (isset($post['location']) && !empty($post['location'])) {

	                  $locationArr = explode(",", $post['location']);
	      if(trim($locationArr[0]) != "Doesn`t Matter")  
			if($locationArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($locationArr as $location) {
					  $location=trim($location);
	                      if ($i == 0)
	                          $where .= " current_location LIKE '%{$location}%'";
	                      else
	                          $where .= " or current_location LIKE '%{$location}%'";
	                      $i++;
	                  }

	                  $where .= " ) ";
	              }
	              }

	              if (isset($post['job_roles']) && !empty($post['job_roles'])) {

	                  $rolesArr = explode(",", $post['job_roles']);
	      if(trim($rolesArr[0]) != "Doesn`t Matter")  
		  if($rolesArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($rolesArr as $roles) {
					  $roles=trim($roles);
	                      if ($i == 0)
	                          $where .= " role_id LIKE '%{$roles}%'";
	                      else
	                          $where .= " or role_id LIKE '%{$roles}%'";
	                      $i++;
	                  }
	                  $where .= " ) ";
	              }
	              }

	              if (isset($post['salary']) && !empty($post['salary'])) {

	                  $salaryArr = explode(",", $post['salary']);
	      if(trim($salaryArr[0]) != "Doesn`t Matter")  
		  if($salaryArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($salaryArr as $sal) {
					  $sal=trim($sal);
	                      if ($i == 0)
	                          $where .= " expected_salary LIKE '%{$sal}%'";
	                      else
	                          $where .= " or expected_salary LIKE '%{$sal}%'";
	                      $i++;
	                  }
	                  $where .= " ) ";
	              }
	              }

	              if (isset($post['experience']) && !empty($post['experience'])) {

	                  $experienceArr = explode(",", $post['experience']);
	      if(trim($experienceArr[0]) != "Doesn`t Matter")  
		  if($experienceArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($experienceArr as $experience) {
					  $exp = explode(' ',$experience);
				   $experience = $exp[0];
	                      if ($i == 0)
	                          $where .= " experience LIKE '%{$experience}%'";
	                      else
	                          $where .= " or experience LIKE '%{$experience}%'";
	                      $i++;
	                  }
	                  $where .= " ) ";
	              }
	              }

	              if (isset($post['religion']) && !empty($post['religion'])) {

	                  $religionArr = explode(",", $post['religion']);
	      if(trim($religionArr[0]) != "Doesn`t Matter")  
		  if($religionArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($religionArr as $religion) {
					  $religion=trim($religion);
	                      if ($i == 0)
	                          $where .= " religion LIKE '%{$religion}%'";
	                      else
	                          $where .= " or religion LIKE '%{$religion}%'";
	                      $i++;
	                  }
	                  $where .= " ) ";
	              }
	              }


	              if (!empty($post['weight']))
	                  $where .= " and weight LIKE '%{$post['weight']}%'";

	              if (isset($post['language']) && !empty($post['language'])) {

	                  $languageArr = explode(",", $post['language']);
	      if(trim($languageArr[0]) != "Doesn`t Matter")  
		  if($languageArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($languageArr as $language) {
					  $language=trim($language);
	                      if ($i == 0)
	                          $where .= " language LIKE '%{$language}%'";
	                      else
	                          $where .= " or language LIKE '%{$language}%'";
	                      $i++;
	                  }
	                  $where .= " ) ";
	              }
	              }


	              if (isset($post['maritalstatus']) && !empty($post['maritalstatus'])) {

	                  $maritalstatusArr = explode(",", $post['maritalstatus']);
	      if(trim($maritalstatusArr[0]) != "Doesn`t Matter")  
		  if($maritalstatusArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($maritalstatusArr as $maritalstatus) {
					  $maritalstatus=trim($maritalstatus);
	                      if ($i == 0)
	                          $where .= " marital_status LIKE '%{$maritalstatus}%'";
	                      else
	                          $where .= " or marital_status LIKE '%{$maritalstatus}%'";
	                      $i++;
	                  }
	                  $where .= " ) ";
	              }
	              }

	              if (isset($post['category']) && !empty($post['category'])) {

	                  $categoryArr = explode(",", $post['category']);
	      if(trim($categoryArr[0]) != "Doesn`t Matter")  
		  if($categoryArr[0] != 'All'){
	                  $where .= " and ( ";
	                  $i = 0;
	                  foreach ($categoryArr as $category) {
					  $category=trim($category);
	                      if ($i == 0)
	                          $where .= " job_category LIKE '%{$category}%'";
	                      else
	                          $where .= " or job_category LIKE '%{$category}%'";
	                      $i++;
	                  }
	                  $where .= " ) ";
	              }
	              }

	              	if (isset($post['postedby']) && !empty($post['postedby'])) {

	      				            $postedArr = explode(",", $post['postedby']);
	      if(trim($postedArr[0]) != "Doesn`t Matter")  
		  if($postedArr[0] != 'All'){
	      				            $where .= " and ( ";
	      				            $i = 0;
	      				            foreach ($postedArr as $posted) {
									$posted=trim($posted);
	      				                if ($i == 0){

	      				                    if($posted == 'Self'){
	      				                        $where .= " addedby = 0 ";
	      				                    }else{
	      				                        $where .= " addedby <> 0 ";
	      				                    }


	      				                }
	      				                else{

	      				                     if($posted == 'Self'){
	      				                        $where .= " or addedby = 0 ";
	      				                    }else{
	      				                        $where .= " or addedby <> 0 ";
	      				                    }


	      				                }
	      				                $i++;
	      				            }
	      				            $where .= " ) ";
	      		        }
	      		        }


	      		     if (isset($post['jobtype']) && !empty($post['jobtype'])) {

	      		            $jobtypeArr = explode(",", $post['jobtype']);
	      		if($jobtypeArr[0] != 'All'  && $jobtypeArr[0] != 'Doesn`t Matter'){
	      		            $where .= " and ( ";
	      		            $i = 0;
	      		            foreach ($jobtypeArr as $jobtype) {
							$jobtype=trim($jobtype);
	      		                if ($i == 0)
	      		                    $where .= " job_type LIKE '%{$jobtype}%'";
	      		                else
	      		                    $where .= " or job_type LIKE '%{$jobtype}%'";
	      		                $i++;
	      		            }
							$where .= " or job_type LIKE '%Any%'";
	      		            $where .= " ) ";
	      		  }
           }


        if (!empty($post['username']))
            $where .= " and u.username LIKE '%{$post['username']}%'";


        $where .= " and u.status=1";
        $query->where(" 1 = 1 {$where} ");
        //$query->limit($limit, $start);
        
		$query->order(array("u.featured_candidate desc","u.id desc"));
        $select = $db->fetchAll($query);

        return count($select);
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchjobDataIndex($post, $start, $limit) {

	foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}


        $where = ' ';
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('j' => 'jobs'), array('*'))
                ->join(array('u' => 'users'), 'u.id = j.employer_id', array('u.usertype'));





       if (isset($post['skills']) && !empty($post['skills']) && $post['skills'] != 'Type Job Position, Designation') {

            $skillsArr = explode(",", $post['skills']);
            if ($skillsArr[0] != 'All'  && $skillsArr[0] != 'Doesn`t Matter') {
                $where .= " and ( ";
                $i = 0;
                
				foreach ($skillsArr as $skills) {
					$skills=trim($skills);
                    if ($i == 0)
                        $where .= " ((category_name LIKE '%{$skills}%') or (sub_category_name LIKE '%{$skills}%') or (desired_profile LIKE '%{$skills}%'))";
                    else
                        $where .= " or ((category_name LIKE '%{$skills}%') or (sub_category_name LIKE '%{$skills}%') or (desired_profile LIKE '%{$skills}%'))";
                    $i++;
                }
                $where .= " ) ";
            }
        }





   //     if (!empty($post['location']) && $post['location'] != 'Type City name')
   //         $where .= " and job_location_city LIKE '%{$post['location']}%'";


  if (isset($post['location']) && !empty($post['location']) && $post['location'] != 'Type City name') {

	             $locationArr = explode(",", $post['location']);
	 if($locationArr[0] != 'All'  && $locationArr[0] != 'Doesn`t Matter'){
	             $where .= " and ( ";
	             $i = 0;
	             foreach ($locationArr as $location) {
	                 $location=trim($location);
					 if ($i == 0)
	                     $where .= " job_location_city LIKE '%{$location}%'";
	                 else
	                     $where .= " or job_location_city LIKE '%{$location}%'";
	                 $i++;
	             }
	             $where .= " ) ";
	         }
	         }






        if (isset($post['category']) && !empty($post['category'])) {

            $categoryArr = explode(",", $post['category']);
if($categoryArr[0] != 'All'  && $categoryArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($categoryArr as $category) {
                $category=trim($category);
				if ($i == 0)
                    $where .= " category_name LIKE '%{$category}%'";
                else
                    $where .= " or category_name LIKE '%{$category}%'";
                $i++;
            }
            $where .= " ) ";
        }
        }

        if (isset($post['job_roles']) && !empty($post['job_roles'])) {

            $rolesArr = explode(",", $post['job_roles']);
if($rolesArr[0] != 'All'  && $rolesArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($rolesArr as $roles) {
			$roles=trim($roles);
                if ($i == 0)
                    $where .= " sub_category_name LIKE '%{$roles}%'";
                else
                    $where .= " or sub_category_name LIKE '%{$roles}%'";
                $i++;
            }
            $where .= " ) ";
        }
        }
		

        if (!empty($post['employer_id']))
            $where .= " and employer_id = {$post['employer_id']}";

        if (isset($post['gender']) && !empty($post['gender'])) {

	                 $genderArr = explode(",", $post['gender']);
	     if($genderArr[0] != 'All'  && $genderArr[0] != 'Doesn`t Matter'){
	                 $where .= " and ( ";
	                 $i = 0;
	                 foreach ($genderArr as $gender) {
					 $gender=trim($gender);
	                     if ($i == 0)
	                         $where .= " gender = '{$gender}'";
	                     else
	                         $where .= " or gender = '{$gender}'";
	                     $i++;
	                 }
	                 $where .= " ) ";
	             }
	             }


        if (isset($post['salary']) && !empty($post['salary'])) {

            $salaryArr = explode(",", $post['salary']);
if($salaryArr[0] != 'All'  && $salaryArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($salaryArr as $sal) {
			$sal=trim($sal);
                if ($i == 0)
                    $where .= " salary LIKE '%{$sal}%'";
                else
                    $where .= " or salary LIKE '%{$sal}%'";
                $i++;
            }
            $where .= " ) ";
        }
        }

        if (isset($post['religion']) && !empty($post['religion'])) {

            $religionArr = explode(",", $post['religion']);
if($religionArr[0] != 'All'  && $religionArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($religionArr as $religion) {
			$religion=trim($religion);
                if ($i == 0)
                    $where .= " religion LIKE '%{$religion}%'";
                else
                    $where .= " or religion LIKE '%{$religion}%'";
                $i++;
            }
            $where .= " ) ";
        }
}

        if (isset($post['experience']) && !empty($post['experience'])) {

            $experienceArr = explode(",", $post['experience']);
if($experienceArr[0] != 'All'  && $experienceArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($experienceArr as $experience) {
			$exp = explode(' ',$experience);
				   $experience = $exp[0];
                if ($i == 0)
                    $where .= " experience LIKE '%{$experience}%'";
                else
                    $where .= " or experience LIKE '%{$experience}%'";
                $i++;
            }
            $where .= " ) ";
        }
}

        if (isset($post['language']) && !empty($post['language'])) {

            $languageArr = explode(",", $post['language']);
if($languageArr[0] != 'All' && $languageArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($languageArr as $language) {
			$language=trim($language);
                if ($i == 0)
                    $where .= " language LIKE '%{$language}%'";
                else
                    $where .= " or language LIKE '%{$language}%'";
                $i++;
            }
            $where .= " ) ";
        }
}

        if (isset($post['nationality']) && !empty($post['nationality'])) {

            $nationalityArr = explode(",", $post['nationality']);
if($nationalityArr[0] != 'All' && $nationalityArr[0] != 'Doesn`t Matter' ){
            $where .= " and ( ";
            $i = 0;
            foreach ($nationalityArr as $nationality) {
			$nationality=trim($nationality);
                if ($i == 0)
                    $where .= " nationality LIKE '%{$nationality}%'";
                else
                    $where .= " or nationality LIKE '%{$nationality}%'";
                $i++;
            }
            $where .= " ) ";
        }

        }

        if (isset($post['postedby']) && !empty($post['postedby'])) {

            $postedArr = explode(",", $post['postedby']);
            if ($postedArr[0] != 'All') {
                $where .= " and ( ";
                $i = 0;
                foreach ($postedArr as $posted) {
				$posted=trim($posted);
                    if ($i == 0) {

                        if ($posted == 'Self') {
                            $where .= " usertype = 2 ";
                        } else {
                            $where .= " usertype = 3 ";
                        }
                    } else {

                        if ($posted == 'Self') {
                            $where .= " or usertype = 2 ";
                        } else {
                            $where .= " or usertype = 3 ";
                        }
                    }
                    $i++;
                }
                $where .= " ) ";
            }
        }

             if (isset($post['jobtype']) && !empty($post['jobtype'])) {

		            $jobtypeArr = explode(",", $post['jobtype']);
if($jobtypeArr[0] != 'All'  && $jobtypeArr[0] != 'Doesn`t Matter'){
		            $where .= " and ( ";
		            $i = 0;
		            foreach ($jobtypeArr as $jobtype) {
						$jobtype=trim($jobtype);
		                if ($i == 0)
		                    $where .= " job_type LIKE '%{$jobtype}%'";
		                else
		                    $where .= " or job_type LIKE '%{$jobtype}%'";
		                $i++;
		            }
					$where .= " or job_type LIKE '%Any%'";
		            $where .= " ) ";
        }
}

	if (isset($post['searchdata']) && !empty($post['searchdata']) && $post['searchdata'] != 'Type Job Position, Designation') {
            $where .= " and (title LIKE '%{$post['searchdata']}%'";
            $where .= " or category_name LIKE '%{$post['searchdata']}%'";
            $where .= " or sub_category_name LIKE '%{$post['searchdata']}%'";
            $where .= " or job_type LIKE '%{$post['searchdata']}%'";
            $where .= " or job_location_city LIKE '%{$post['searchdata']}%'";
            $where .= " or gender LIKE '%{$post['searchdata']}%'";
            $where .= " or language LIKE '%{$post['searchdata']}%'";
            $where .= " or religion LIKE '%{$post['searchdata']}%')";		
        }


        $query->where(" 1 = 1 {$where} and j.status=1");

        $query->limit($limit, $start);
        
$query->order(array("is_featured desc","posted_date desc"));


        $select = $db->fetchAll($query);


        return $select;
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchjobDataIndexCount($post) {

        foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}

		$where = ' ';
	         $db = $this->getAdapter();
	         $query = $db->select()
	                 ->from(array('j' => 'jobs'), array('*'))
	                 ->join(array('u' => 'users'), 'u.id = j.employer_id', array('*'));



//	         if (!empty($post['skills']) && $post['skills'] != 'Type Job Position, Designation')
//	             $where .= " and desired_profile LIKE '%{$post['skills']}%'";



       if (isset($post['skills']) && !empty($post['skills']) && $post['skills'] != 'Type Job Position, Designation') {

            $skillsArr = explode(",", $post['skills']);
            if ($skillsArr[0] != 'All'  && $skillsArr[0] != 'Doesn`t Matter') {
                $where .= " and ( ";
                $i = 0;
                foreach ($skillsArr as $skills) {
				$skills=trim($skills);
                    if ($i == 0)
                        $where .= " ((category_name LIKE '%{$skills}%') or (sub_category_name LIKE '%{$skills}%') or (sub_category_name LIKE '%{$skills}%'))";
                    else
                        $where .= " or ((category_name LIKE '%{$skills}%') or (sub_category_name LIKE '%{$skills}%') or (sub_category_name LIKE '%{$skills}%'))";
                    $i++;
                }
                $where .= " ) ";
            }
        }





   //     if (!empty($post['location']) && $post['location'] != 'Type City name')
   //         $where .= " and job_location_city LIKE '%{$post['location']}%'";


  if (isset($post['location']) && !empty($post['location']) && $post['location'] != 'Type City name') {

	             $locationArr = explode(",", $post['location']);
	 if($locationArr[0] != 'All'  && $locationArr[0] != 'Doesn`t Matter'){
	             $where .= " and ( ";
	             $i = 0;
	             foreach ($locationArr as $location) {
				 $location=trim($location);
	                 if ($i == 0)
	                     $where .= " job_location_city LIKE '%{$location}%'";
	                 else
	                     $where .= " or job_location_city LIKE '%{$location}%'";
	                 $i++;
	             }
	             $where .= " ) ";
	         }
	         }






        if (isset($post['category']) && !empty($post['category'])) {

            $categoryArr = explode(",", $post['category']);
if($categoryArr[0] != 'All'  && $categoryArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($categoryArr as $category) {
			$category=trim($category);
                if ($i == 0)
                    $where .= " category_name LIKE '%{$category}%'";
                else
                    $where .= " or category_name LIKE '%{$category}%'";
                $i++;
            }
            $where .= " ) ";
        }
        }

        if (isset($post['job_roles']) && !empty($post['job_roles'])) {

            $rolesArr = explode(",", $post['job_roles']);
if($rolesArr[0] != 'All'  && $rolesArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($rolesArr as $roles) {
                $roles=trim($roles);
				if ($i == 0)
                    $where .= " sub_category_name LIKE '%{$roles}%'";
                else
                    $where .= " or sub_category_name LIKE '%{$roles}%'";
                $i++;
            }
            $where .= " ) ";
        }
        }


        if (!empty($post['employer_id']))
            $where .= " and employer_id = {$post['employer_id']}";

        if (isset($post['gender']) && !empty($post['gender'])) {

	                 $genderArr = explode(",", $post['gender']);
	     if($genderArr[0] != 'All'  && $genderArr[0] != 'Doesn`t Matter'){
	                 $where .= " and ( ";
	                 $i = 0;
	                 foreach ($genderArr as $gender) {
					 $gender=trim($gender);
	                     if ($i == 0)
	                         $where .= " gender = '{$gender}'";
	                     else
	                         $where .= " or gender = '{$gender}'";
	                     $i++;
	                 }
	                 $where .= " ) ";
	             }
	             }


        if (isset($post['salary']) && !empty($post['salary'])) {

            $salaryArr = explode(",", $post['salary']);
if($salaryArr[0] != 'All'  && $salaryArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($salaryArr as $sal) {
			$sal=trim($sal);
                if ($i == 0)
                    $where .= " salary LIKE '%{$sal}%'";
                else
                    $where .= " or salary LIKE '%{$sal}%'";
                $i++;
            }
            $where .= " ) ";
        }
        }

        if (isset($post['religion']) && !empty($post['religion'])) {

            $religionArr = explode(",", $post['religion']);
if($religionArr[0] != 'All'  && $religionArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($religionArr as $religion) {
			$religion=trim($religion);
                if ($i == 0)
                    $where .= " religion LIKE '%{$religion}%'";
                else
                    $where .= " or religion LIKE '%{$religion}%'";
                $i++;
            }
            $where .= " ) ";
        }
}

        if (isset($post['experience']) && !empty($post['experience'])) {

            $experienceArr = explode(",", $post['experience']);
if($experienceArr[0] != 'All'  && $experienceArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($experienceArr as $experience) {
			$exp = explode(' ',$experience);
				   $experience = $exp[0];
                if ($i == 0)
                    $where .= " experience LIKE '%{$experience}%'";
                else
                    $where .= " or experience LIKE '%{$experience}%'";
                $i++;
            }
            $where .= " ) ";
        }
}

        if (isset($post['language']) && !empty($post['language'])) {

            $languageArr = explode(",", $post['language']);
if($languageArr[0] != 'All' && $languageArr[0] != 'Doesn`t Matter'){
            $where .= " and ( ";
            $i = 0;
            foreach ($languageArr as $language) {
			$language=trim($language);
                if ($i == 0)
                    $where .= " language LIKE '%{$language}%'";
                else
                    $where .= " or language LIKE '%{$language}%'";
                $i++;
            }
            $where .= " ) ";
        }
}

        if (isset($post['nationality']) && !empty($post['nationality'])) {

            $nationalityArr = explode(",", $post['nationality']);
if($nationalityArr[0] != 'All' && $nationalityArr[0] != 'Doesn`t Matter' ){
            $where .= " and ( ";
            $i = 0;
            foreach ($nationalityArr as $nationality) {
			$nationality=trim($nationality);
                if ($i == 0)
                    $where .= " nationality LIKE '%{$nationality}%'";
                else
                    $where .= " or nationality LIKE '%{$nationality}%'";
                $i++;
            }
            $where .= " ) ";
        }

        }

        if (isset($post['postedby']) && !empty($post['postedby'])) {

            $postedArr = explode(",", $post['postedby']);
            if ($postedArr[0] != 'All') {
                $where .= " and ( ";
                $i = 0;
                foreach ($postedArr as $posted) {
				$posted=trim($posted);
                    if ($i == 0) {

                        if ($posted == 'Self') {
                            $where .= " usertype = 2 ";
                        } else {
                            $where .= " usertype = 3 ";
                        }
                    } else {

                        if ($posted == 'Self') {
                            $where .= " or usertype = 2 ";
                        } else {
                            $where .= " or usertype = 3 ";
                        }
                    }
                    $i++;
                }
                $where .= " ) ";
            }
        }

             if (isset($post['jobtype']) && !empty($post['jobtype'])) {

		            $jobtypeArr = explode(",", $post['jobtype']);
if($jobtypeArr[0] != 'All'  && $jobtypeArr[0] != 'Doesn`t Matter'){
		            $where .= " and ( ";
		            $i = 0;
		            foreach ($jobtypeArr as $jobtype) {
					$jobtype=trim($jobtype);
		                if ($i == 0)
		                    $where .= " job_type LIKE '%{$jobtype}%'";
		                else
		                    $where .= " or job_type LIKE '%{$jobtype}%'";
		                $i++;
		            }
					$where .= " or job_type LIKE '%Any%'";
		            $where .= " ) ";
        }
}

if (isset($post['searchdata']) && !empty($post['searchdata']) && $post['searchdata'] != 'Type Job Position, Designation') {
            $where .= " and (title LIKE '%{$post['searchdata']}%'";
            $where .= " or category_name LIKE '%{$post['searchdata']}%'";
            $where .= " or sub_category_name LIKE '%{$post['searchdata']}%'";
            $where .= " or job_type LIKE '%{$post['searchdata']}%'";
            $where .= " or job_location_city LIKE '%{$post['searchdata']}%'";
            $where .= " or gender LIKE '%{$post['searchdata']}%'";
            $where .= " or language LIKE '%{$post['searchdata']}%'";
            $where .= " or religion LIKE '%{$post['searchdata']}%')";		
        }


        $query->where(" 1 = 1 {$where} and j.status=1 ");
        //$query->limit($limit, $start);
        
$query->order(array("is_featured desc","posted_date desc"));
        //echo $query; exit;

        $select = $db->fetchAll($query);

        return count($select);
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchjobDataByOverallSearch($post, $start, $limit) {

 foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}

        $where = ' ';
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('j' => 'jobs'), array('*'));

        if (isset($post['searchdata']) && !empty($post['searchdata']) && $post['searchdata'] != 'Type Job Position, Designation') {
            $where .= " and title LIKE '%{$post['searchdata']}%'";
            $where .= " or category_name LIKE '%{$post['searchdata']}%'";
            $where .= " or sub_category_name LIKE '%{$post['searchdata']}%'";
            $where .= " or job_type LIKE '%{$post['searchdata']}%'";
            $where .= " or job_location_city LIKE '%{$post['searchdata']}%'";
            $where .= " or gender LIKE '%{$post['searchdata']}%'";
            $where .= " or language LIKE '%{$post['searchdata']}%'";
            $where .= " or religion LIKE '%{$post['searchdata']}%'";
			

        }

          $query->where(" 1=1 and status=1 {$where}  ");

        $query->limit($limit, $start);
        
$query->order(array("is_featured desc","posted_date desc"));

        $select = $db->fetchAll($query);

        return $select;
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchjobDataByOverallSearchCount($post) {

      foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}   $where = ' ';
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('j' => 'jobs'), array('*'));

		$post['searchdata']=trim($post['searchdata']);		
        if (isset($post['searchdata']) && !empty($post['searchdata']) && $post['searchdata'] != 'Type Job Position, Designation') {
            $where .= " and title LIKE '%{$post['searchdata']}%'";
            $where .= " or category_name LIKE '%{$post['searchdata']}%'";
            $where .= " or sub_category_name LIKE '%{$post['searchdata']}%'";
            $where .= " or job_type LIKE '%{$post['searchdata']}%'";
            $where .= " or job_location_city LIKE '%{$post['searchdata']}%'";
            $where .= " or gender LIKE '%{$post['searchdata']}%'";
            $where .= " or language LIKE '%{$post['searchdata']}%'";
            $where .= " or religion LIKE '%{$post['searchdata']}%'";


        }

        $query->where(" 1=1 and status=1 {$where} ");
        //$query->limit($limit, $start);
        
		$query->order(array("is_featured desc","posted_date desc"));
        $select = $db->fetchAll($query);

        return count($select);
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchcandidateDataByOverallSearch($post, $start, $limit) {

       foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}

		$where = ' ';
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('c' => 'candidate'), array('*'))
                ->join(array('u' => 'users'), 'u.id = c.user_id', array('*'));



        if (isset($post['searchdata']) && !empty($post['searchdata']) && $post['searchdata'] != 'Type Job Position, Designation') {
            $where .= " and experience LIKE '%{$post['searchdata']}%'";
            $where .= " or current_location LIKE '%{$post['searchdata']}%'";
            $where .= " or username LIKE '%{$post['searchdata']}%'";
            $where .= " or mobile LIKE '%{$post['searchdata']}%'";
            $where .= " or marital_status LIKE '%{$post['searchdata']}%'";
            $where .= " or phone LIKE '%{$post['searchdata']}%'";
            $where .= " or email LIKE '%{$post['searchdata']}%'";
            $where .= " or religion LIKE '%{$post['searchdata']}%'";
            $where .= " or job_category LIKE '%{$post['searchdata']}%'";
			$where .= " or role_id LIKE '%{$post['searchdata']}%'";
			$where .= " or key_skills LIKE '%{$post['searchdata']}%'";




        }
			 $query->where(" 1=1 and u.status=1 {$where} ");

        $query->limit($limit, $start);
        
$query->order(array("u.featured_candidate desc","u.id desc"));
        $select = $db->fetchAll($query);

        return $select;
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchcandidateDataByOverallSearchCount($post) {

        foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}

		$where = ' ';
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('c' => 'candidate'), array('*'))
                ->join(array('u' => 'users'), 'u.id = c.user_id', array('*'));



        if (isset($post['searchdata']) && !empty($post['searchdata']) && $post['searchdata'] != 'Type Job Position, Designation') {
            $where .= " and experience LIKE '%{$post['searchdata']}%'";
            $where .= " or current_location LIKE '%{$post['searchdata']}%'";
            $where .= " or username LIKE '%{$post['searchdata']}%'";
            $where .= " or mobile LIKE '%{$post['searchdata']}%'";
            $where .= " or marital_status LIKE '%{$post['searchdata']}%'";
            $where .= " or phone LIKE '%{$post['searchdata']}%'";
            $where .= " or email LIKE '%{$post['searchdata']}%'";
            $where .= " or religion LIKE '%{$post['searchdata']}%'";
            $where .= " or job_category LIKE '%{$post['searchdata']}%'";
			$where .= " or role_id LIKE '%{$post['searchdata']}%'";
			$where .= " or key_skills LIKE '%{$post['searchdata']}%'";


        }

			$query->where(" 1=1 and u.status=1 {$where} ");

        //$query->limit($limit, $start);
        $query->order("dob desc");

        $select = $db->fetchAll($query);

        return count($select);
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchcandidateDataByCategory($post, $start, $limit) {

       foreach($post as $k=>$c)
		{
			$post[$k]=trim($c);
		}
		$db = $this->getAdapter();
        $query = $db->select()
                ->from(array('c' => 'candidate'), array('*'))
                ->join(array('u' => 'users'), 'u.id = c.user_id', array('*'));




        if (!empty($post))
            $category = " and job_category LIKE '%{$post}%'";

        if (!empty($post))
            $query->where(" 1=1 and status=1 {$category}");




        $query->limit($limit, $start);

        $query->order("dob desc");

        $select = $db->fetchAll($query);

        return $select;
    }

    /**
     *
     * @param type $post
     * @return type
     */
    public function searchcandidateDataByCategoryCount($post) {

        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('c' => 'candidate'), array('*'))
                ->join(array('u' => 'users'), 'u.id = c.user_id', array('*'));


        if (!empty($post))
            $category = " and job_category LIKE '%{$post}%'";

        if (!empty($post))
             $query->where(" 1=1 and status=1 {$category}");

        // $query->limit($limit, $start);

        $query->order("dob desc");

        $select = $db->fetchAll($query);

        return count($select);
    }

}
