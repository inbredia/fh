<?php

class User_AgencyController extends Zend_Controller_Action {

    public function preDispatch() {

        $this->view->pageIdentity = $this->_request->getActionName();
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $this->view->userType = $auth->getIdentity()->usertype;
            $this->view->currentUserId = $auth->getIdentity()->id;
            //$this->view->prImg = $auth->getIdentity()->profileimage;
        }
    }

    public function init() {

        $userModel = new User_Model_Users();
        $this->_agencyModel = new User_Model_Users();
        $this->view->category = $userModel->getCategories();
        $defaultModel = new Default_Model_Datatable();
        $this->view->jobs = $defaultModel->getJobs();
        $this->view->jobsCount = $defaultModel->getJobsCount();
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{	
			$this->_searchModel = new Search_Model_Search();
			$id = $auth->getIdentity()->id;
			$this->view->arrApplied = $this->_searchModel->getAppliedJobsByCandidate($id);				
			
			$this->view->arrShortlisted = $this->_searchModel->getShortlistedJobsByCandidate($id);
		}		
		else{
		$this->view->arrApplied = array();
			$this->view->arrShortlisted = array();
		}
    }

		public function postDispatch(){
	$request = $this->getRequest();
		$return = $request->getParam('return');
		if($return == 1){
			$url = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
			$session = new Zend_Session_Namespace('url');
			$session->redirect = $url;
		}
	}
	
    public function indexAction() {

    }

   


   public function agencyregistrationAction() {

        $form = new User_Form_Agencyregistration(array(
                    'action' => '/agencyregistration',
                    'submitLabel' => 'Agency Registration'
                ));

        if ($this->_request->getPost()) {
            $formData = $this->_request->getPost();

            if ($form->isValid($formData)) {

                $userModel = new User_Model_Users();
                $id = $userModel->saveAgency($form->getValues(), $_POST);

                $this->_redirect('/signupsuccess?st=agency&uid=' . $id);
            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;
    }

    /**
     * This is to edit the agency
     */
    public function editagencyAction() {

        $form = new User_Form_Agencyregistration(array(
                    'action' => '/agencyregistration',
                    'submitLabel' => 'Edit Agency'
                ));
        $request = $this->getRequest();
			$this->view->st = $request->getParam('st');

			$auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity())
            $userId = $auth->getIdentity()->id;


        if ($this->_request->getPost()) {
            $formData = $this->_request->getPost();

            $userModel = new User_Model_Users();

            $userModel->updateAgency($formData, $userId, $_POST);
            $this->_redirect('/editagency?st=updated');
        }

        $userModel = new User_Model_Users();
        $results = $userModel->getagencyprofile($userId);
$this->view->password= $results['password'];
		
		
		$phoneM = $results['mobile'];
			$phnArr = explode('-',$phoneM);
			
	        $phoneP = $results['phone'];
			$phnArrP = explode('-',$phoneP);
        $formData = array(
            'fname' => $results['fname'],
            'lname' => $results['lname'],
            'username' => $results['username'],
            'password' => $results['password'],
			'repassword' => $results['password'],
            'email' => $results['email'],
            'agency_name' => $results['agency_name'],
            'contact_person_name' => $results['contact_person_name'],
            'country' => $results['country'],
             'mobile' => $phnArr[1],
            'fax' => $results['fax'],
            'phone' => $phnArrP[1],
            'agency_website' => $results['agency_website'],
            'about_agency' => $results['about_agency']
        );


        $form->populate($formData);
        $this->view->form = $form;
    }

    /**
     * This is to add candidate by agency
     */
    public function addcandidatebyagencyAction() {
        $form = new User_Model_Form_Candidateregistration(array(
                    'action' => '/addcandidatebyagency',
                    'submitLabel' => 'Registration'
                ));

        if ($this->_request->getPost()) {

            $formData = $this->_request->getPost();

            if ($form->isValid($formData)) {
		
                $id = $this->_agencyModel->saveCandidateByAgency($form->getValues(), $_POST, $this->view->currentUserId);
				
				if(!empty($_FILES)) {
					
						$allowedExts = array("gif", "jpeg", "jpg", "png");
						$temp = explode(".", $_FILES["file"]["name"]);
						$extension = end($temp);
								$new_file = $id.'_'.time().$_FILES['file']['name'];
							    move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $new_file);
							  //echo "Stored in: " . "uploads/" . $_FILES["file"]["name"];
								$userModel = new User_Model_Users();
								$arr = $userModel->insertphoto($new_file,$id);								
								$userModel->saveProfilePhotos($new_file,$id);					
				}
				$this->_redirect('/viewcandidatesaddedbyagency');
            } else {
                $form->populate($formData);
            }
        }
        $this->view->form = $form;
    }

    /**
       * This is to list the candidates added by agency
       */
      public function viewcandidatesaddedbyagencyAction() {


          // Paging Logic

          $page = 1; //Default page
          $limit = 10; //Records per page
          $start = 0; //starts displaying records from 0
          if (isset($_GET['page']) && $_GET['page'] != '') {
              $page = $_GET['page'];
          }

          $start = ($page - 1) * $limit;

          $this->view->limit = $limit;
          $this->view->start = $start;


          $this->view->addedcandidates = $this->_agencyModel->getCandidatesByAgencyId($this->view->currentUserId, $start, $limit);
          $this->view->addedcandidatestotalCount = $this->_agencyModel->getCandidatesByAgencyIdTotalCount($this->view->currentUserId);
      }

    /**
     * This is to edit the candidates added by agency
     */
    public function editcandiateaddedbyagencyAction() {

        $candidateId = $this->getRequest()->getParam('uid');

        $form = new User_Model_Form_Candidateregistration(array(
                    'action' => '/editcandiateaddedbyagency',
                    'submitLabel' => 'Registration'
                ));

        $auth = Zend_Auth::getInstance();
		$request = $this->getRequest();
			$this->view->st = $request->getParam('st');




            
//print_r($_POST);exit;
            //if ($form->isValid($formData)) {
if($_POST){
		    $formData = $this->_request->getPost();
			$id = $this->_agencyModel->updateAgencyCandidateById($_POST, $candidateId, $this->view->currentUserId, $_POST);
				
				if(!empty($_FILES)) {
					
						$allowedExts = array("gif", "jpeg", "jpg", "png");
						$temp = explode(".", $_FILES["file"]["name"]);
						$extension = end($temp);
								$new_file = $id.'_'.time().$_FILES['file']['name'];
							    move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $new_file);
							  //echo "Stored in: " . "uploads/" . $_FILES["file"]["name"];
								$userModel = new User_Model_Users();
								$arr = $userModel->insertphoto($new_file,$candidateId);								
								$userModel->saveProfilePhotos($new_file,$candidateId);					
				}
				
				
                $this->_redirect('/editcandiateaddedbyagency?st=updated&uid=' . $id);
            } else {
       //         $form->populate($formData);
       

        $results = $this->_agencyModel->getCandidateById($candidateId);

        $dob = $results['dob'];
        $this->view->dob = explode("-", $dob);
        $this->view->language = $results['language'];
        $this->view->job_category = $results['job_category'];
        $this->view->job_sub_category = $results['role_id'];
        $this->view->pref = $results['pref'];
		$this->view->weight = $results['weight'];
        $this->view->citypref = $results['citypref'];
        $this->view->keyarea = $results['key_skills'];
        $phoneM = $results['phone'];
        $phnArr = explode('-', $phoneM);
        $this->view->location_id = $results['current_location'] . '-' . $phnArr[0];
        $this->view->marital_status = $results['marital_status'];
        $this->view->aboutme = $results['aboutme'];
        $this->view->current_location = $results['current_location'];
        $this->view->nationality = $results['nationality'];
        $this->view->visa_type = $results['visa_type'];
        $this->view->visa_date_expiry = $results['visa_date_expiry'];
        $this->view->expected_salary = $results['expected_salary'];
        $this->view->basic_education = $results['basic_education'];
        $this->view->video_url = $results['video_url'];
        $this->view->newsletter = $results['newsletter'];
        $this->view->offers = $results['offers'];
		
		$this->view->candidateImg = $results['profile_image'];

		$fnam = explode("Agency-",$results['fname']);
        $formData = array(
            'fname' => $fnam[1],
            'lname' => $results['lname'],
            'email' => $results['email'],
            'dob' => $results['dob'],
            'height' => $results['height'],
            'weight' => $results['weight'],
            'nationality' => $results['nationality'],
            'current_location' => $results['current_location'],
            'mobile' => $results['mobile'],
            'gender' => $results['gender'],
            'marital_status' => $results['marital_status'],
            'phone' => $phnArr[1],
            'language' => $results['language'],
            'job_category' => $results['job_category'],
            'role_id' => $results['role_id'],
            'pref' => $results['pref'],
            'citypref' => $results['citypref'],
            'expected_salary' => $results['expected_salary'],
            'dayoffpref' => $results['dayoffpref'],
            'basic_education' => $results['basic_education'],
            'key_skills' => $results['key_skills'],
            'abtyou' => $results['aboutme'],
            'newsletter' => $results['newsletter'],
            'offers' => $results['offers'],
            'religion' => $results['religion'],
            'notice' => $results['notice'],
            'experience' => $results['experience'],
			'jobtype' => $results['job_type'],
            'visa_type' => $results['visa_type'],
            'visa_date_expiry' => $results['visa_date_expiry'],
            'video_url' => $results['video_url'],
        );


        $form->populate($formData);
        $this->view->form = $form;
		}
    }

	function agencynotificationsAction()
	{
		$auth = Zend_Auth::getInstance();
			if($auth->hasIdentity() && $auth->getIdentity()->usertype==3)
						$uid = $auth->getIdentity()->id;
			else
					$this->_redirect(WEBSITE);
					
		$this->view->title="Jobs Notifications";
		$userModel = new User_Model_Users();	
		$this->view->notifications = $userModel->getEmployerNotifications($uid);
		

	}	
	

	function agencycandidatenotificationsAction()
	{
		$auth = Zend_Auth::getInstance();
			if($auth->hasIdentity() && $auth->getIdentity()->usertype==3)
						$uid = $auth->getIdentity()->id;
			else
					$this->_redirect(WEBSITE);
					
		$this->view->title="Candidate Notifications";
		$userModel = new User_Model_Users();	
		$this->view->notifications = $userModel->getcandidatedNotificationsByAgency($uid);
			
		//echo "<pre>";print_r($this->view->notifications);exit;
	}	

	function getmyjobsAction()
	{
		$auth = Zend_Auth::getInstance();
			if($auth->hasIdentity() && $auth->getIdentity()->usertype==3)
						$uid = $auth->getIdentity()->id;
			else
					$this->_redirect(WEBSITE);
					$this->_helper->layout->disableLayout();
					
		$request = $this->getRequest();
		if($_POST)
		{
		
		$cid = $request->getParam('cid');
		$chk = $request->getParam('chk');
		
		$userModel = new User_Model_Users();
		$arr = $userModel->candidateapply($cid,$id);

		$session = new Zend_Session_Namespace('url');

		$arrChck = explode('?',$session->redirect);
		if(count($arrChck) == 1)
			$this->_redirect($session->redirect."?st=apply");
		else
			$this->_redirect($session->redirect."&st=apply");

		}		
		$this->view->cid = $request->getParam('id');			
		$this->view->title="Candidate Notifications";
		$userModel = new User_Model_Users();	
		$this->view->notifications = $userModel->getcandidatedNotificationsByAgency($uid);
		$this->view->myjobs = $userModel->myjobs($uid);

	}

	function getmycandidatesAction()
	{
		$auth = Zend_Auth::getInstance();
			if($auth->hasIdentity() && $auth->getIdentity()->usertype==3)
						$uid = $auth->getIdentity()->id;
			else
					$this->_redirect(WEBSITE);
					$this->_helper->layout->disableLayout();
					
		$request = $this->getRequest();
		if($_POST)
		{
		
		$jobid = $request->getParam('jid');
		$chk = $request->getParam('chk');
		$submit = $request->getParam('submit');
		
		$userModel = new User_Model_Users();
		if(is_array($chk) && count($chk))
		{
			if($submit == 'Shortlist')
			{
					foreach($chk as $val)
							$arr = $userModel->jobshortlist($jobid,$val);
								$this->_redirect(WEBSITE."profile?page=shortlisted&st=shortlisted");	
							}
			else	{
					foreach($chk as $val)
							$arr = $userModel->jobapply($jobid,$val);
								$this->_redirect(WEBSITE."profile?page=applied&st=apply");
							}

		}

		
		
		}		
		$this->view->job_id = $request->getParam('jobid');		
		$this->view->act = $request->getParam('act');				
		
		$userModel = new User_Model_Users();			
		$this->view->mycandidates = $this->_agencyModel->getCandidatesByAgencyId($uid, 0, 1000);
		
		$candidatesArr = $this->_agencyModel->getAgencyCandidatesByJobs($this->view->job_id);
		if($this->view->act=='shortlist')
		{
					$this->view->candidateArr = $candidatesArr[0];
		}
		if($this->view->act=='apply')
		{
					$this->view->candidateArr = $candidatesArr[1];
		}

	}
	
	function candidatestatusAction()
	{
			$auth = Zend_Auth::getInstance();
			if($auth->hasIdentity() && $auth->getIdentity()->usertype==3)
						$uid = $auth->getIdentity()->id;
			else
					$this->_redirect(WEBSITE);
			
			$request = $this->getRequest();
			$cand = $request->getParam('candidate_id');	
			$st = $request->getParam('st');	
			$userModel = new User_Model_Users();				
			$res = $userModel->candidatestatus($cand,$st,$uid);
			if($res == 1)
				$this->_redirect(WEBSITE.'viewcandidatesaddedbyagency?st=change');
			else	
				$this->_redirect(WEBSITE.'viewcandidatesaddedbyagency?st=error');
	}
	

}