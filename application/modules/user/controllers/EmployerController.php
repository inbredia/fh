<?php

class User_EmployerController extends Zend_Controller_Action {

    public function preDispatch() {

        $this->view->pageIdentity = $this->_request->getActionName();
    }

	public function postDispatch(){
	$request = $this->getRequest();
		$return = $request->getParam('return');
		if($return == 1){
			$url = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
			$session = new Zend_Session_Namespace('url');
			$session->redirect = $url;
		}
	}
	
	
    public function init() {
			$userModel = new User_Model_Users();
			$defaultModel = new Default_Model_Datatable();
			$this->view->category = $defaultModel->getmainCategories();
			$this->view->jobs = $defaultModel->getJobs();
			$auth = Zend_Auth::getInstance();

			if($auth->hasIdentity())
				{
				if($auth->getIdentity()->last_login == NULL)
					$this->view->lastLogin =  $date = Date("F d, Y");
				else
					$this->view->lastLogin = $auth->getIdentity()->last_login;
				$id = $auth->getIdentity()->id;
				$this->view->myjobs = $userModel->myjobscount($id);
				$this->view->shortlistcount = $userModel->shortlistcandidatecount($id);
				$this->view->expressinterest = $userModel->expressinterestcount($id);
				$this->view->myaccountbalance = $userModel->getemployerinfo($id);
			$this->view->prImg = $auth->getIdentity()->profileimage;
			
				}
			$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{	
			$id = $auth->getIdentity()->id;
			$this->_searchModel = new Search_Model_Search();$this->view->arrApplied = $this->_searchModel->getAppliedJobsByCandidate($id);				
			
			$this->view->arrShortlisted = $this->_searchModel->getShortlistedJobsByCandidate($id);
		}		
		else{
		$this->view->arrApplied = array();
			$this->view->arrShortlisted = array();
		}
				}

    public function indexAction() {

    }

	public function employerpackageAction() {
        $this->_helper->layout->setLayout('noright');
    }

	public function buypackageAction() {
        $auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
		$request = $this->getRequest();
		$package = $request->getParam('package');
		$price = $request->getParam('price');

		if($package == "quickpost")
		{
			$job = 1;
			$bulksms = 100;
			$candidateinterest = 20;
			$userModel = new User_Model_Users();
			$userModel->buypackage($id,'quickpost',$price,$job,$bulksms,$candidateinterest);
		} else
		if($package == "quickinterest")
		{
			$job = 0;
			$bulksms = 0;
			$candidateinterest = 20;
			$userModel = new User_Model_Users();
			$userModel->buypackage($id,'quickinterst',$price,$job,$bulksms,$candidateinterest,$featured);
		} else
		if($package == "custom")
		{
			$job = $request->getParam('jobs');;
			$bulksms = $request->getParam('bulksms');;
			$candidateinterest = $request->getParam('candidateinterest');;
			$featured = $request->getParam('featured');;
			$userModel = new User_Model_Users();
			$userModel->buypackage($id,'custom',$price,$job,$bulksms,$candidateinterest,$featured);
		}
		$this->_redirect(WEBSITE."employerpackage?st=purchased");
    }



	public function interestedcandidatesAction() {
        $auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
		$request = $this->getRequest();
		$page = $request->getParam('page');
		$jobid = $request->getParam('jobid');
		if(!$page)
			$page=0;
		$userModel = new User_Model_Users();
		$this->view->title = "Candidates Interested in Job";
		$this->view->interestedcandidates = $userModel->interestedcandidates($jobid,$page);

    }

	public function myjobsAction() {
        $auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
		$request = $this->getRequest();
		$page = $request->getParam('page');
		$q = $request->getParam('mobileverification');
		if($q==1)
			$this->view->verification =1;
			if(!$page)
			$page=0;
		$userModel = new User_Model_Users();
		$this->view->title = "Live Jobs";
		
		$job_st = $request->getParam('job_st');
		if($job_st=='unapproved'){$this->view->title = "Unapproved Jobs";
		$this->view->myjobs = $userModel->myunapprovedjobs($id,$page);}
		else if($job_st=='expired'){$this->view->title = "Expired Jobs";
		$this->view->myjobs = $userModel->expiredjobs($id,$page);}
		else
		$this->view->myjobs = $userModel->myjobs($id,$page);

    }
    public function employerregistrationAction() {

        $form = new User_Form_Employerregistration(array(
                    'action' => '/employerregistration',
                    'submitLabel' => 'Employer Registration'
                ));

        if ($this->_request->getPost()) {
            $formData = $this->_request->getPost();

            if ($form->isValid($formData)) {

                $userModel = new User_Model_Users();
                $id = $userModel->saveEmployer($form->getValues(), $_POST);
                $this->_redirect('/signupsuccess?st=emp&uid=' . $id);
            } else {
                $form->populate($formData);
            }
        }

        $this->view->form = $form;

    }



		public function profileAction()
	{

		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;

		$userModel = new User_Model_Users();
		$searchModel = new Search_Model_Search();

		$request = $this->getRequest();
		$page = $request->getParam('page');

		$this->view->userType = $auth->getIdentity()->usertype;
		if($this->view->userType == 2 || $this->view->userType == 3)
		{
				$this->view->showbuttons = true;
				if($page == "expressed")
				{
				$this->view->title = "Applied Candidates";
				$this->view->fjobs = $searchModel->getAppliedCandidates($id);
				
				$this->view->showbuttons = false;
				}else
				if($page == "shortlisted")
				{$this->view->title = "Shortlisted Candidates";
				$this->view->fjobs = $searchModel->getShortlistedCandidates($id);
				//$this->view->arrCandidates = $searchModel->getInterestedCandidatesByEmployer($id);
				$this->view->showbuttons = true;
				}else
				{$this->view->title = "Featured Candidates";
				$this->view->fjobs = $userModel->getFeaturedCandidates();
				}

				$this->view->uid = $id;
				//$this->view->shortlistedCount = $userModel->getShortlistedCandidateCount($id);
				//$this->view->appliedCount = $userModel->getExpressedCandidateCount($id);

		}

		
		$this->view->uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
		$arr = $userModel->getprofile($id);
		$this->view->me = $arr[0];
		$this->view->page = "profile";
		if(isset($auth->getIdentity()->profileimage))
			$this->view->prImg = $auth->getIdentity()->profileimage;
	}


	function candidateinterestAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
		
			if($auth->getIdentity()->express_interest > 0 && $auth->getIdentity()->status ==1)
			{
				$request = $this->getRequest();
		$cid = $request->getParam('id');
		$ref = $request->getParam('ref');
		
		$userModel = new User_Model_Users();
		$arr = $userModel->candidateapply($cid,$id);
		
		$return = $request->getParam('return');
		
		
		$session = new Zend_Session_Namespace('url');
		
		if($return!='')
			$session->redirect = $return;
		
		$arrChck = explode('?',$session->redirect);
		if(count($arrChck) == 1)
			$this->_redirect($session->redirect."?st=apply");
		else
			$this->_redirect($session->redirect."&st=apply");
}else
{
		$session = new Zend_Session_Namespace('url');

		$arrChck = explode('?',$session->redirect);
		if(count($arrChck) == 1)
			$this->_redirect($session->redirect."?st=nobalance");
		else
			$this->_redirect($session->redirect."&st=nobalance");



}




	}


	function shortlistedcandidatesAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
				$request = $this->getRequest();
		$cid = $request->getParam('id');
		$userModel = new User_Model_Users();
		$arr = $userModel->shortlistedcandidates($cid,$id);
		

		$return = $request->getParam('return');
		
		
		$session = new Zend_Session_Namespace('url');
		
		if($return!='')
			$session->redirect = $return;
		

		$arrChck = explode('&',$session->redirect);
		if(count($arrChck) == 1)
			$this->_redirect($session->redirect."?st=shortlisted");
		else
			$this->_redirect($session->redirect."&st=shortlisted");


	}


	function candidateshortlistAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
				$request = $this->getRequest();
		$cid = $request->getParam('id');
		$userModel = new User_Model_Users();
		$arr = $userModel->candidateshortlist($cid,$id);
		$session = new Zend_Session_Namespace('url');

		$arrChck = explode('?',$session->redirect);
		if(count($arrChck) == 1)
			$this->_redirect($session->redirect."?st=shortlisted");
		else
			$this->_redirect($session->redirect."&st=shortlisted");


	}

	function employernotificationsAction()
	{
		$auth = Zend_Auth::getInstance();
			if($auth->hasIdentity() && $auth->getIdentity()->usertype==2)
						$uid = $auth->getIdentity()->id;
			else
					$this->_redirect(WEBSITE);

		$this->view->title="Notifications";
		$userModel = new User_Model_Users();
		$this->view->notifications = $userModel->getEmployerNotifications($uid);


	}


	    /**
	     * This is to edit the employer
	     */
	    public function editemployerAction() {

	        $form = new User_Form_Employerregistration(array(
	                    'action' => '/employerregistration',
	                    'submitLabel' => 'Edit Employer'
	                ));

			$request = $this->getRequest();
			$this->view->st = $request->getParam('st');

	        $auth = Zend_Auth::getInstance();
	        if ($auth->hasIdentity())
	            $userId = $auth->getIdentity()->id;


	        if ($this->_request->getPost()) {
	            $formData = $this->_request->getPost();

	            $userModel = new User_Model_Users();

	            $userModel->updateEmployer($formData, $userId, $_POST);
	            $this->_redirect('/editemployer?st=updated');
	        }

	        $userModel = new User_Model_Users();
	        $results = $userModel->getemployerprofile($userId);

			$phoneM = $results['mobile'];
			$phnArr = explode('-',$phoneM);
			
	        $phoneP = $results['phone'];
			$phnArrP = explode('-',$phoneP);
			$this->view->notification = $results['notification_method'];
			$formData = array(
	            'fname' => $results['fname'],
	            'lname' => $results['lname'],
	            'username' => $results['username'],
	            'password' => $results['password'],
	            'email' => $results['email'],
	            'company_name' => $results['company_name'],
	            'employer_type' => $results['employer_type'],
	            'industry_type' => $results['industry_type'],
	            'no_of_employee' => $results['no_of_employees'],
	            'url' => $results['url'],
	            'company_profile' => $results['company_profile'],
	            'country' => $results['country'],
	            'mobile' => $phnArr[1],
	            'fax' => $results['fax'],
	            'phone' => $phnArrP[1]
	        );


	        $form->populate($formData);
	        $this->view->form = $form;
	    }

// Provide options to select paypal or NTFS account transfer
	public function buypackageoptionsAction() {
        $auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
		$request = $this->getRequest();
	
         	$this->view->package = $request->getParam('package');
         	$this->view->price = $request->getParam('price');
		$package = $request->getParam('package');
		$price = $request->getParam('price');

		
		if($package == "quickpost")
		{
			$this->view->job = 1;
			$this->view->bulksms = 50;
			$this->view->candidateinterest = 25;
			$this->view->candidateadd = 5;
			$this->view->featured = 1;
		} 
		else if($package == "quickinterest")
		{
			$this->view->job = 0;
			$this->view->bulksms = 0;
			$this->view->candidateinterest = 20;
			$this->view->candidateadd = 5;
		} 
		else if($package == "custom")
		{
			$this->view->job = $request->getParam('jobs');
			$this->view->bulksms = $request->getParam('bulksms');
			$this->view->candidateinterest = $request->getParam('candidateinterest');
			$this->view->featured = $request->getParam('featured');
			$this->view->candidateadd = $request->getParam('candidateadd');
		
		}
		else if($package == "quickfeatured")
		{
			$this->view->featured_candidate = 1;
		
		}
		else if($package == "featuredsaving")
		{
			$this->view->featured_candidate = 2;
		
		}
		else if($package == "quickcandidatepremium")
		{
			$this->view->featured_candidate = 1;
		
		}
		else if($package == "candidatepremiumsaving")
		{
			$this->view->featured_candidate = 2;
		
		}



    }

	// For ntfs details 
	public function packagebybankaccountAction() {
        $auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
		
		$request = $this->getRequest();
		$package = $request->getParam('package');
		$price = $request->getParam('price');
		$job = $request->getParam('jobs');;
		$bulksms = $request->getParam('bulksms');;
		$candidateinterest = $request->getParam('candidateinterest');
		$candidateadd = $request->getParam('candidateadd');
		$candidateadd = $candidateadd?$candidateadd:0;
		$featured = $request->getParam('featured');
		$featured_candidate = $request->getParam('featured_candidate');

		$userModel = new User_Model_Users();
		if($price > 0){
		$userModel->buypackage($id,$package,$price,$job,$bulksms,$candidateinterest,$featured);
		
		$userModel->saveOrder($id,$package,$price,$job,$bulksms,$candidateinterest,$featured,0,$featured_candidate,$candidateadd);
		$this->_redirect('packagebybankaccount');
		}

    }


	public function recommendAction()
	{
		$this->view->message = 0;
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);


		$request = $this->getRequest();

		if ($this->_request->getPost()) {
	            $formData = $this->_request->getPost();

		$email = $formData['email'];
		//$message = $formData['message'];
		if($email == '')
		{
			$this->_redirect(WEBSITE."recommend?st=error");
		}
		
		$subject = "Welcome to Fastest Hiring.com "; 

		$msgData = "Hi, <br><br> One of your friend has shared you the Fastest Profile website.Please have a look at the below mentioned url, Thankyou. <br><br>Url : <a href='www.fastesthiring.com'>FastestHiring.com</a> <br>"; 	



	            $userModel = new User_Model_Users();

		    $userModel->sendEmail($email,$subject,$msgData);
	            $this->view->message = 1;
	    
$this->_redirect(WEBSITE."recommend?st=success");
		}
		
		


	}
	
	
	
	
	public function paypalcancelAction()
	{
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);


		$request = $this->getRequest();
		 $formData = $this->_request->getPost();
		
		echo "<pre>";
		print_r($formData); exit;

	}
	
	
	public function checkoutcompleteAction()
	{
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);

		 
		$request = $this->getRequest();
		$package = $request->getParam('package');
		$price = $request->getParam('price');
		$job = $request->getParam('jobs');
		$bulksms = $request->getParam('bulksms');
		$candidateinterest = $request->getParam('candidateinterest');
		$featured = $request->getParam('featured');
		
		$featured_candidate = $request->getParam('featured_candidate');
		$addcandidates = $request->getParam('candidateadd');

		$userModel = new User_Model_Users();
		$userModel->buypackage($id,$package,$price,$job,$bulksms,$candidateinterest,$featured);
		$userModel->saveOrder($id,$package,$price,$job,$bulksms,$candidateinterest,$featured,1,$featured_candidate?$featured_candidate:0,$addcandidates?$addcandidates:0);
		
		$auth = Zend_Auth::getInstance();
		
		$auth->getIdentity()->job_post = $auth->getIdentity()->job_post + $job;
		$auth->getIdentity()->featured_job = $auth->getIdentity()->featured_job + $featured;
		$auth->getIdentity()->bulk_sms = $auth->getIdentity()->bulk_sms + $bulksms;
		$auth->getIdentity()->express_interest = $auth->getIdentity()->express_interest + $candidateinterest;
		$auth->getIdentity()->add_candidates = $auth->getIdentity()->add_candidates + $addcandidates;
		$auth->getIdentity()->featured_candidate = $auth->getIdentity()->featured_candidates + $featured_candidates;
		
		
		$this->_redirect(WEBSITE.'buypackageoptions?st=purchased');
		 
			
		
		//echo "<pre>";
		//print_r($_REQUEST); exit;

	}
	
	
	
	public function myipnAction()
	{
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);


		$request = $this->getRequest();
		 $formData = $this->_request->getPost();
		
			$userModel = new User_Model_Users();
		$userModel->buypackage($id,$package,$price,$job,$bulksms,$candidateinterest,$featured);
		$userModel->saveOrder($id,$package,$price,$job,$bulksms,$candidateinterest,$featured);
		
		$this->_redirect(WEBSITE);

	}

		
	public function myordersAction()
	{
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);


		$userModel = new User_Model_Users();
		$this->view->myorders = $userModel->getmyorders($id);		

	}
	
	
	public function myordersajaxAction()
	{
		
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);

		$this->_helper->layout->disableLayout();	
		$userModel = new User_Model_Users();
		$myorders = $userModel->getmyorders($id);
		$json = Zend_Json::encode($myorders);
		echo $json;exit;

		

	}


	public function changepasswordAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
		
		if($_POST)
		{
			if($_POST['oldpassword'] == $auth->getIdentity()->password)
			{
					$userModel = new User_Model_Users();
					$userModel->changepassword($_POST['newpassword'],$id);
					$auth->getIdentity()->password = $_POST['newpassword'];
					$this->_redirect(WEBSITE.'changepassword?st=success');
			}
			else
					$this->_redirect(WEBSITE.'changepassword?st=error');
		}			
	}


	public function getinterestedemployersAction()
	{
		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
			$id = $auth->getIdentity()->id;
		else
			$this->_redirect(WEBSITE);
		$request = $this->getRequest();
		$candidate_id = $request->getParam('candidate_id');
	
		$userModel = new User_Model_Users();
		$this->view->employers = $userModel->getinterestedemployers($candidate_id);
		$this->_helper->layout->disableLayout();				
	}	


	
}