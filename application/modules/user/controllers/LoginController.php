<?php

class User_LoginController extends Zend_Controller_Action
{

	public function preDispatch()
	{

		$this->view->pageIdentity = $this->_request->getActionName();

	}

    public function init()
    {
       		$defaultModel = new Default_Model_Datatable();
		$this->view->jobs = $defaultModel->getJobs();
		$userModel = new User_Model_Users();
		$this->view->category = $defaultModel->getmainCategories();

		$auth = Zend_Auth::getInstance();
		if($auth->hasIdentity())
		{	
			$this->_searchModel = new Search_Model_Search();$id = $auth->getIdentity()->id;
			$this->view->arrApplied = $this->_searchModel->getAppliedJobsByCandidate($id);				
			
			$this->view->arrShortlisted = $this->_searchModel->getShortlistedJobsByCandidate($id);
		}		
		else{
		$this->view->arrApplied = array();
			$this->view->arrShortlisted = array();
		}
    }
	public function postDispatch(){
	$request = $this->getRequest();
		$return = $request->getParam('return');
		if(isset($return) && $return!=''){
			$url = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
			$session = new Zend_Session_Namespace('url');
			$session->redirect = $return;
		}
	}

	public function indexAction()
	{
		$form = new User_Model_Form_Login(array(
          'action' => '/index',
          'submitLabel' => 'Login'
		));


		 if ($this->_request->getPost()) {
          $formData = $this->_request->getPost();

          if ($form->isValid($formData)) {
    				if ($this->_process($form->getValues())) {
					// We're authenticated! Redirect to the home page

						$auth = Zend_Auth::getInstance();
						$type = $auth->getIdentity()->usertype;
						$userModel = new User_Model_Users();
						$userModel->updateLoginTime($auth->getIdentity()->id);
						
						$request = $this->getRequest();
						$return = $request->getParam('return');
						/*if(isset($return) && ($return!=''))
						{
							$this->_redirect(urldecode($return));
						}
						
			
						$session = new Zend_Session_Namespace('url');
						
						
						
						if(isset($session->redirect) && ($session->redirect!=''))
						{
							$redirURL = $session->redirect;
							$session->redirect ='';
							$this->_redirect($redirURL);
						}
						
						if(isset($session->redirect) && ($session->redirect!=''))
						{
							$redirURL = $session->redirect;
							$session->redirect ='';
							$this->_redirect($redirURL);
						}*/
						if($type == 0)
							$this->_redirect('/profile');
						else if($type == 2)
							$this->_redirect('/myjobs');
						 else if($type == 3)
							$this->_redirect('/myjobs');

					}
					else
					{
					$this->_redirect('/login');
					}

          } else {
					$auth = Zend_Auth::getInstance();
					if($auth->hasIdentity())
					{
						$this->_redirect(WEBSITE);
					}
					
					$form->populate($formData);
          }
      }
		$this->view->form = $form;



	}



	protected function _process($values)
	{
		// Get our authentication adapter and check credentials
		$adapter = $this->_getAuthAdapter();


		$adapter->setIdentity($values['username']);
		$adapter->setCredential($values['password']);

		$auth = Zend_Auth::getInstance();
		$result = $auth->authenticate($adapter);
		if ($result->isValid()) {

		$user = $adapter->getResultRowObject();
		$auth->getStorage()->write($user);

		return true;
		}
		return false;
	}


		protected function _getAuthAdapter() {

		$dbAdapter = Zend_Db_Table::getDefaultAdapter();
		$authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

		$authAdapter->setTableName('users')
		->setIdentityColumn('username')
		->setCredentialColumn('password');
//		->setCredentialTreatment('SHA1(CONCAT(?,salt))');

		return $authAdapter;
		}

		public function logoutAction()
    {
        Zend_Auth::getInstance()->clearIdentity();


        return $this->_redirect("/");
    }
}