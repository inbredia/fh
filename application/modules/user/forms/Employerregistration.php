<?php

class User_Form_Employerregistration extends Zend_Form {

    public function init() {
        // Set the method for the display form to POST
        $this->setMethod('post');

        // This is for First_name Field
        $this->addElement('text', 'fname', array(
            'label' => '*First Name:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

        // This is for Last Name Field
        $this->addElement('text', 'lname', array(
            'label' => 'Last Name:',
           // 'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

        // This is for desired username Field

        $this->addElement('text', 'username', array(
            'label' => '*Username:',
            // 'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20)),
                array('Db_NoRecordExists', true, array(
                        'table' => 'users',
                        'field' => 'username',
                        'messages' => array(
                            'recordFound' => 'Username already taken'
                        )
                    )
                )
            )
        ));

        // This is for Password
        $this->addElement('password', 'password', array(
            'label' => '*Password:',
            // 'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20))
            )
        ));

        // This is for Confirm Password
        $this->addElement('password', 'repassword', array(
            'label' => '*Confirm Password:',
            // 'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20))
            )
        ));

        // This is for Email Address
        $this->addElement('text', 'email', array(
            'label' => '*Email address:',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                'EmailAddress',
                array('Db_NoRecordExists', true, array(
                        'table' => 'users',
                        'field' => 'email',
                        'messages' => array(
                            'recordFound' => 'Email already taken'
                        )
                    )
                )
            )
        ));

        // This is for country field
       // $this->addElement('select', 'country', array(
       //     'multiOptions' =>
       //     array('0' => 'Select','UAE' => 'United Arab Emirates', 'Oman' => 'Oman', 'Qatar' => 'Qatar', 'Bahrain' => 'Bahrain', 'Kuwait' => 'Kuwait', 'Saudi Arabia' => 'Saudi Arabia'
       //     ),
       //     'setValue' => '0',
       //     'required' => true,
       //     'label' => '*Country:',
      //  ));

        // This is for Mobile
          $this->addElement('text', 'mobile', array(
            'label' => '-',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

        // This is for Employer Type
        $this->addElement('select', 'employer_type', array(
            'multiOptions' =>
           array('2' => 'Individual', '1' => 'Company' ),
            'setValue' => '',
            'required' => true,
            'label' => '*Employer Type:',
        ));

        // This is for Company Name

          $this->addElement('text', 'company_name', array(
            'label' => '*Company Name:',
            //'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

          //This is for Industry Type

         $this->addElement('select', 'industry_type', array(
		            'multiOptions' =>
		             array(

		                        '0' => 'Select',
		                        'Air Conditioning / Refrigeration' => 'Air Conditioning / Refrigeration'  ,
		                        'Advertising / PR / Events'  => 'Advertising / PR / Events'  ,
		                        'Agriculture / Dairy / Poultry'  => 'Agriculture / Dairy / Poultry'  ,
		                        'Airlines / Aviation'  => 'Airlines / Aviation'  ,
		                        'Architecture / Interior Designing'  => 'Architecture / Interior Designing' ,
		                        'Automotive / Auto Ancillary'  => 'Automotive / Auto Ancillary'  ,
		                        'Banking / Financial Services / Broking'  => 'Banking / Financial Services / Broking'  ,
		                        'Chemicals / PetroChemical'  => 'Chemicals / PetroChemical '  ,
		                        'Construction'  => 'Construction'  ,
		                        'Consumer Durables / Consumer Electronics'  => 'Consumer Durables / Consumer Electronics',
		                        'FMCG / Foods / Beverage '=> 'FMCG / Foods / Beverage ' ,
		                        'Courier / Logistics / Transportation / Warehousing ' => 'Courier / Logistics / Transportation / Warehousing ' ,
		                        'Defence / Military / Government ' => 'Defence / Military / Government'  ,
		                        'Education / Training / Teaching ' => 'Education / Training / Teaching '  ,
		                        'Export / Import / General Trading  '=> 'Export / Import / General Trading '  ,
		                        'Fertilizers / Pesticides ' => 'Fertilizers / Pesticides'  ,
		                        'Fresher - No Industry Experience / Not Employed / Intern  '=> 'Fresher - No Industry Experience / Not Employed / Intern  ' ,
		                        'Gems & Jewellery'  => 'Gems & Jewellery'  ,
		                        'Hotels / Hospitality / Tourism / Recreative ' => 'Hotels / Hospitality / Tourism / Recreative'  ,
		                        'Industrial Products / Heavy Machinery ' => 'Industrial Products / Heavy Machinery ' ,
		                        'Insurance ' => 'Insurance'  ,
		                        'Internet / E-Commerce / Dotcom  '=> 'Internet / E-Commerce / Dotcom'  ,
		                        'IT - Hardware & Networking'  => 'IT - Hardware & Networking'  ,
		                        'IT - Software Services'  => 'IT - Software Services'  ,
		                        'Media / Entertainment / Publishing'  => 'Media / Entertainment / Publishing'  ,
		                        'Medical / Healthcare / Diagnostics / Medical Devices'  => 'Medical / Healthcare / Diagnostics / Medical Devices' ,
		                        'Metals / Steel / Iron / Aluminum / Foundry / Electroplating'  => 'Metals / Steel / Iron / Aluminum / Foundry / Electroplating' ,
		                        'Mining / Forestry / Fishing'  => 'Mining / Forestry / Fishing' ,
		                        'NGO / Social Services'  => 'NGO / Social Services'  ,
		                        'Office Automation / Equipment / Stationery'  => 'Office Automation / Equipment / Stationery'  ,
		                        'Paper'  => 'Paper'  ,
		                        'Oil & Gas / Petroleum'  => 'Oil & Gas / Petroleum'  ,
		                        'Pharma / Biotech / Clinical Research'  => 'Pharma / Biotech / Clinical Research' ,
		                        'Plastics / Rubber'  => 'Plastics / Rubber'  ,
		                        'Power / Energy'  => 'Power / Energy'  ,
		                        'Printing / Packaging'  => 'Printing / Packaging'  ,
		                        'Real Estate'  => 'Real Estate'  ,
		                        'Recruitment / Placement Firm'  => 'Recruitment / Placement Firm'  ,
		                        'Restaurants / Catering ' => 'Restaurants / Catering'  ,
		                        'Retail'  => 'Retail'  ,
		                        'Security / Law Enforcement'  => 'Security / Law Enforcement'  ,
		                        'Shipping / Freight'  => 'Shipping / Freight' ,
		                        'Telecom / ISP ' => 'Telecom / ISP '  ,
		                        'Textiles / Garments / Accessories / Fashion'  => 'Textiles / Garments / Accessories / Fashion' ,
		                        'Tyres ' => 'Tyres'  ,
		                        'Other '=> 'Other'  ,
		                        ),
		            'setValue' => '',
		            'required' => true,
		            'label' => '*Industry Type::',
		        ));

        //This is for No of employees

          $this->addElement('text', 'no_of_employee', array(
            'label' => 'No of employee:',
            //'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
        // This is for company website Url

          $this->addElement('text', 'url', array(
            'label' => 'Company website url:',
            //'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
        // This is for company profile

          $this->addElement('textarea', 'company_profile', array(
            'label' => 'Company Profile:',
            //'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20000))
            )
        ));

          //This is for phone

          $this->addElement('text', 'phone', array(
            'label' => '-',
            //'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

          // This is for Fax

          $this->addElement('text', 'fax', array(
            'label' => 'Fax:',
            //'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

          $this->addElement(new User_Model_Form_Element_Locationselect('country'));

    }

}