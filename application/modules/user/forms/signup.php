<?php
class User_Form_Signup extends Zend_Form
{
    public function init()
    {
        // Set the method for the display form to POST
        $this->setMethod('post');
 
        
        $this->addElement('text', 'username', array(
            'label'      => 'Username:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                 array('validator' => 'StringLength', 'options' => array(0, 20)),
				 array('Db_NoRecordExists', true, array(
                                'table' => 'users',
                                'field' => 'username',
                                'messages' => array(
                                        'recordFound' => 'Username already taken'
                                )
                        )
                        )
            )
        ));
		
		
        $this->addElement('password', 'password', array(
            'label'      => 'Password:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20))
            )
        ));
		
		$this->addElement('password', 'repassword', array(
            'label'      => 'Confirm Password:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 20))
            )
        ));
		
		
		$this->addElement('text', 'company', array(
            'label'      => 'Company Name:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 100))
            )
        ));
		
		
		$this->addElement('select', 'ctype', array(
            'label'      => 'Company Type:',
			'required'   => true,
            'multiOptions' => array(
                array('1' => 'Corporate House','2' => 'Placement Consultant')
            )
        ));
		
		$this->addElement('select', 'country', array(
            'label'      => 'Country:',
			'required'   => true,
            'multiOptions' => array(
                array('US' => 'US','UK' => 'UK')
            )
        ));
		
		$this->addElement('text', 'city', array(
            'label'      => 'City:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
	
		$this->addElement('textarea', 'cprofile', array(
            'label'      => 'Company Profile:',
            'required'   => true,
			'rows'	=> 4,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 250))
            )
        ));
		
		
		$this->addElement('text', 'url', array(
            'label'      => 'Website URL:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
		
		$this->addElement('text', 'fname', array(
            'label'      => 'First Name:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
		
		$this->addElement('text', 'lname', array(
            'label'      => 'Last Name:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
		
		$this->addElement('text', 'address', array(
            'label'      => 'Address:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
		
		$this->addElement('text', 'contact', array(
            'label'      => 'Contact:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
		
		$this->addElement('text', 'mobile', array(
            'label'      => 'Mobile:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
		
		$this->addElement('text', 'fax', array(
            'label'      => 'Fax:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));
		
		// Add an email element
        $this->addElement('text', 'email', array(
            'label'      => 'Email address:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                'EmailAddress',
				array('Db_NoRecordExists', true, array(
                                'table' => 'users',
                                'field' => 'email',
                                'messages' => array(
                                        'recordFound' => 'Email already taken'
                                )
                        )
						)
            )
        ));
		
		
		
	}
}