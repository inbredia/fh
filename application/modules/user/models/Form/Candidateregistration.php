<?php
class User_Model_Form_Candidateregistration extends Zend_Form
{
    public function init()
    {
        // Set the method for the display form to POST
        $this->setMethod('post');


        $this->addElement('text', 'username', array(
            'label'      => '*Desired Username:',
            //'required'   => true,
            'filters'    => array('StringTrim'),
			'attribs' => array('data-validate'  => 'validate(required)'),
            'validators' => array(
                 array('validator' => 'StringLength', 'options' => array(0, 20)),
				 array('Db_NoRecordExists', true, array(
                                'table' => 'users',
                                'field' => 'username',
                                'messages' => array(
                                        'recordFound' => 'Username already taken'
                                )
                        )
                        )
            )
        ));


        $this->addElement('password', 'password', array(
            'label'      => '*Password:',
            //'required'   => true,
            'filters'    => array('StringTrim'),

			'validators' => array(
                array('validator' => 'StringLength', 'options' => array(2, 50))
            )
        ));

		$this->addElement('password', 'repassword', array(
            'label'      => '*Confirm Password:',
            //'required'   => true,
            'filters'    => array('StringTrim'),

			'validators' => array(
                array('validator' => 'StringLength', 'options' => array(2, 50))
            )
        ));





		$this->addElement('multiselect', 'preference', array(

            'multiOptions' =>
                array('All'=>'All','UAE' => 'United Arab Emirates','Oman' => 'Oman','Qatar'=>'Qatar','Bahrain'=>'Bahrain','Kuwait'=>'Kuwait','Saudi Arabia'=>'Saudi Arabia'
            ),
			'setValue' => 'All'


        ));

		$this->addElement('multiselect', 'city', array(


					'RegisterInArrayValidator' => false


        ));



		$this->addElement('select', 'notice', array(
            'label'      => 'Notice/Availability:',
			'required'   => true,

            'multiOptions' =>
                array('now' => 'Now','1 week' => '1 week','2 week'=>'2 week','1 month'=>'1 month','2 month'=>'2 month'
            )
        ));

		$this->addElement('select', 'jobtype', array(
            'label'      => '*Job Type:',
			'required'   => true,

            'multiOptions' =>
                array("Any" => 'Doesn\'t Matter','Full Time' => 'Full Time','Part Time'=>'Part Time','Live in'=>'Live in','Contract'=>'Contract'
            )
        ));




		$this->addElement('select', 'offpreference', array(
            'label'      => 'Day Off Preference:',
			'multiOptions' => array(
                'Once in a week' => 'Once in a week','Twice in a week' => 'Twice in a week','Once in a month'=>'Once in a month','Once Bi Weekly'=>'Once Bi Weekly'
            )
        ));





		$this->addElement('select', 'maritalstatus', array(
            'label'      => '*Marital Status:',
			'required'   => true,

			'multiOptions' => array(
                '' => 'Select','Single' => 'Single','Married' => 'Married','Divorced' =>'Divorced','Widowed'=>'Widowed'
            )
        ));


		$this->addElement('select', 'salary', array(
            'label'      => '*Expected Salary:',
            'multiOptions' => array(
                '' => 'Select Salary',
				'0-500 AED' => '0-500 AED',
				'500-1000 AED' => '500-1000 AED',
				'1000-1500 AED' => '1000-1500 AED',
				'1500-2000 AED' => '1500-2000 AED',
				'2000-2500 AED' => '2000-2500 AED',
				'2500-3000 AED' => '2500-3000 AED',
				'3000-3500 AED' => '3000-3500 AED',
				'3500-4000 AED' => '3500-4000 AED',
				'4000-5000 AED' => '4000-5000 AED',
				'5000-10000 AED' => '5000-10000 AED',
				'10000+ AED' => '10000+ AED'
            )
        ));



		$this->addElement('text', 'fname', array(
            'label'      => '*First Name:',
            'required'   => true,
			'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));


		$this->addElement('select', 'height', array(
            'label'   => '*Height:',

			'required'   => true,
            'multiOptions' => array(
                   ''=>'Please Select Height','4\' 5\'\' - 134cm' => '4\' 5\'\' - 134cm','4\' 6\'\' - 137cm' => '4\' 6\'\' - 137cm','4\' 7\'\' - 139cm' => '4\' 7\'\' - 139cm','4\' 8\'\' - 142cm' => '4\' 8\'\' - 142cm','4\' 9\'\' - 144cm' => '4\' 9\'\' - 144cm','4\' 10\'\' - 147cm' => '4\' 10\'\' - 147cm','4\' 11\'\' - 149cm' => '4\' 11\'\' - 149cm','5\' - 152cm' => '5\' - 152cm','5\' 1\'\' - 154cm' => '5\' 1\'\' - 154cm','5\' 2\'\' - 157cm' => '5\' 2\'\' - 157cm','5\' 3\'\' - 160cm' => '5\' 3\'\' - 160cm','5\' 4\'\' - 162cm' => '5\' 4\'\' - 162cm','5\' 5\'\' - 165cm' => '5\' 5\'\' - 165cm' ,'5\' 6\'\' - 167cm' => '5\' 6\'\' - 167cm','5\' 7\'\' - 170cm' => '5\' 7\'\' - 170cm','5\' 8\'\' - 172cm' => '5\' 8\'\' - 172cm','5\' 9\'\' - 175cm' => '5\' 9\'\' - 175cm','5\' 10\'\' - 177cm' => '5\' 10\'\' - 177cm'  ,'5\' 11\'\' - 180cm' => '5\' 11\'\' - 180cm','6\' - 182cm' => '6\' - 182cm','6\' 1\'\' - 185cm' => '6\' 1\'\' - 185cm','6\' 2\'\' - 187cm' => '6\' 2\'\' - 187cm','6\' 3\'\' - 190cm' => '6\' 3\'\' - 190cm','6\' 4\'\' - 193cm' => '6\' 4\'\' - 193cm','6\' 5\'\' - 195cm' => '6\' 5\'\' - 195cm','6\' 6\'\' - 198cm' => '6\' 6\'\' - 198cm','6\' 7\'\' - 200cm' => '6\' 7\'\' - 200cm','6\' 8\'\' - 203cm' => '6\' 8\'\' - 203cm','6\' 9\'\' - 205cm' => '6\' 9\'\' - 205cm','6\' 10\'\' - 208cm' => '6\' 10\'\' - 208cm','6\' 11\'\' - 210cm' => '6\' 11\'\' - 210cm','7\' - 213cm' => '7\' - 213cm'
            )
        ));


		$this->addElement('select', 'religion', array(
            'label'   => '*Religion:',

			'required'   => true,
            'multiOptions' => array(
                   ''=>'Please Select Religion','Hindu'=>'Hindu','Muslim'=>'Muslim','Christian'=>'Christian','Sikh'=>'Sikh','Parsi'=>'Parsi','Jain'=>'Jain','Buddhist'=>'Buddhist','Jewish'=>'Jewish','No Religion'=>'No Religion'
            )
        ));



		$this->addElement('text', 'weight', array(
            'label'      => '*Weight:',
			'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));


		$this->addElement('select', 'visa', array(
            'label'      => '*Visa Type:',

            'multiOptions' => array(
                '' => 'Select Visa Type','Maid' => 'Maid','Visit' => 'Visit','Employment' => 'Employment','Husband/Father' => 'Husband/Father','Other' => 'Other'
            )
        ));

		$this->addElement('text', 'datepicker', array(
            'label'      => '*Visa Expiry Date:'


        ));

		$this->addElement('select', 'experience', array(
            'label'      => '*Total Experience:',

			'required'   => true,
            'multiOptions' => array(
                '' => 'Select Total Experience',
				'Fresher' => 'Fresher',
				'0-1' => '0-1 year',
				'1-2' => '1-2 year',
				'2-3' => '2-3 year',
				'3-5' => '3-5 year',
				'5-10' => '5-10 year',
				'10+' => '10+ years'
            )
        ));

		$this->addElement('text', 'lname', array(
            'label'      => ' Last Name:',
            'filters'    => array('StringTrim')
                    ));

		$this->addElement('text', 'phone', array(
            'label'      => 'Phone:',
            'filters'    => array('StringTrim')
                    ));


		$this->addPrefixPath('App_Form', 'App/Form/');


		$this->addElement('date', 'date_of_birth', array(
		'label' => '*Date of birth:',
		  'required'   => true,
		'startYear' =>'1940',
		'stopYear' =>'2013'
		));




		$this->addElement('text', 'mobile', array(
            'label'      => '*Mobile:',
          //  'required'   => true,
            'filters'    => array('StringTrim')
        ));

		$this->addElement('radio', 'gender', array(
			'label'=>'*Gender',
				'separator'=>'',
			'multiOptions'=>array(
				'male' => 'Male',
				'female' => 'Female'
			),

		));



		$this->addElement('text', 'keyarea', array(

            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(0, 200))
            )
        ));

		$this->addElement('textarea', 'abtyou', array(
            'label'      => '*About me:',

            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                array('validator' => 'StringLength', 'options' => array(45, 20000))
            )
        ));
		

		// Add an email element
        $this->addElement('text', 'email', array(
            'label'      => 'Email address:',
            'filters'    => array('StringTrim')

        ));




		$this->addElement(new User_Model_Form_Element_Countryselect('country_id'));
		$this->addElement(new User_Model_Form_Element_Categoryselect('category_id'));
		$this->addElement(new User_Model_Form_Element_Educationselect('education_id'));
		$this->addElement(new User_Model_Form_Element_Subcategoryselect('sub_category_id'));
		$this->addElement(new User_Model_Form_Element_Locationselect('location_id'));
		$this->addElement(new User_Model_Form_Element_Languageselect('language'));

	}
}