<?php
class User_Model_Form_Element_Locationselect extends Zend_Form_Element_Select
{
    public function init()
    {
        $oLocationTb = new User_Model_Users();
        $this->addMultiOption(0, 'Please select...');
		$this->setRequired(true);
		
		$this->class="select margin-topZ";
        foreach ($oLocationTb->getLocation() as $oCountry) {
            $this->addMultiOption($oCountry['nicename'].'-'.$oCountry['phonecode'], $oCountry['nicename']);
        }
    
	}
}