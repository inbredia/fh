<?php

class User_Model_Users extends Default_Model_Datatable
{

    protected $_name = 'users';

	public function getUsers()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('u'=>'users'),array('j.title'));
		$select = $db->fetchAll($query);
		return $select;

	}

	public function jobshortlist($job,$user)
	{
			$db = $this->getAdapter();
			$query = $db->select()
					->from(array('s'=>'shortlist_jobs'),array('*'))
					->where('s.job_id='.$job)
					->where('s.candidate_id='.$user);
			$select = $db->fetchAll($query);

			if(count($select) > 1)
				return 0;

			$data['job_id']=$job;
			$data['candidate_id']=$user;
			$select =$db->insert('shortlist_jobs',$data);
			return 1;
	}

	public function candidateshortlist($cid,$eid)
	{
			$db = $this->getAdapter();
			$query = $db->select()
					->from(array('s'=>'shortlist_candidate'),array('*'))
					->where('s.employer_id='.$cid)
					->where('s.candidate_id='.$eid);
			$select = $db->fetchAll($query);

			if(count($select) > 1)
				return 0;

			$data['employer_id']=$eid;
			$data['candidate_id']=$cid;
			$select =$db->insert('shortlist_candidate',$data);
			return 1;
	}

	public function candidateapply($cid,$eid)
	{
			$db = $this->getAdapter();
			$query = $db->select()
					->from(array('s'=>'apply_candidate'),array('*'))
					->where('s.employer_id='.$cid)
					->where('s.candidate_id='.$eid);

			$select = $db->fetchAll($query);

			if(count($select) > 1)
				return 0;

			$data['employer_id']=$eid;
			$data['candidate_id']=$cid;
			$select =$db->insert('apply_candidate',$data);
			return 1;
	}


	public function jobapply($job,$user)
	{
			$db = $this->getAdapter();
			$query = $db->select()
				->from(array('s'=>'apply_jobs'),array('*'))
				->where('s.job_id='.$job)
					->where('s.candidate_id='.$user);
			$select = $db->fetchAll($query);
			if(count($select) > 1)
				return;

			$data['job_id']=$job;
			$data['candidate_id']=$user;
			$select =$db->insert('apply_jobs',$data);
			return 1;
	}

	public function getIndustry()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('u'=>'industry'),array('*'));
		$select = $db->fetchAll($query);
		return $select;

	}

	public function deletephoto($photo,$id)
	{
		$db = $this->getAdapter();
 	  	$query = $db->delete('photos',"user_id=".$id." and photo='".$photo."'");


		return;

	}

	public function verify($uid,$code)
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('u'=>'users'),array('u.id'))
				->where("u.verification_code ='".$code."' and u.id=".$uid);
		$select = $db->fetchAll($query);
		if(count($select))
			{
				$data['verified'] = 1;
			  	$db->update('users',$data,"id=$uid");


				return 1;
			}
			else
			return 0;
		return;
	}

	public function getCountries()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('c'=>'country'),array('*'))
				->order(array('order_by','nicename'));
		$select = $db->fetchAll($query);
		return $select;

	}
	public function getCategories()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('c'=>'category'),array('*'))
				->where('parent_category_id = 0');
		$select = $db->fetchAll($query);
		return $select;

	}

	public function getSubCategories($id=1000)
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('c'=>'category'),array('*'))
				->where('parent_category_id = ?',$id);
		$select = $db->fetchAll($query);
		return $select;

	}
	public function getEducation()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('e'=>'education'),array('*'));

		$select = $db->fetchAll($query);
		return $select;

	}

	public function getLocation()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('c'=>'country'),array('*'))
				->order(array('order_for_location','nicename'));

		$select = $db->fetchAll($query);
		return $select;

	}

	public function getFeaturedJobsForCandidate()
	{
		$db = $this->getAdapter();
		$query = $db->select()
				->from(array('j'=>'jobs'),array('*'))
				->joinLeft(array('s'=>'shortlist_jobs'),'j.id = s.job_id')
				->joinLeft(array('a'=>'apply_jobs'),'j.id = a.job_id',array('a.candidate_id as jobseeker_id'))
				->where('j.is_featured = 1')
				->order('id desc')
				->limit('5');


		$select = $db->fetchAll($query);
		return $select;

	}
	public function getAppliedCount($uid)
	{
		$db = $this->getAdapter();
		$query = $db->select()
				->from(array('j'=>'apply_jobs'),array('*'))
				->where('j.candidate_id ='.$uid);

		$select = $db->fetchAll($query);
		return count($select);
	}

	public function getShortlistedCount($uid)
	{
		$db = $this->getAdapter();
		$query = $db->select()
				->from(array('j'=>'shortlist_jobs'),array('*'))
				->where('j.candidate_id ='.$uid);

		$select = $db->fetchAll($query);
		return count($select);
	}
	public function getAppliedJobsForCandidate($userId)
	{
		$db = $this->getAdapter();
		$query = $db->select()
				->from(array('j'=>'jobs'),array('*'))
				->joinInner(array('a'=>'apply_jobs'),'j.id = a.job_id')

				->where('a.candidate_id ='.$userId)
				;

		$select = $db->fetchAll($query);
		return $select;

	}

	public function getShortlistedJobsForCandidate($userId)
	{
		$db = $this->getAdapter();
		$query = $db->select()
				->from(array('j'=>'jobs'),array('*'))
				->joinInner(array('s'=>'shortlist_jobs'),'j.id = s.job_id')
				->joinLeft(array('a'=>'apply_jobs'),'j.id = a.job_id',array('a.candidate_id as jobseeker_id'))
				->where('s.candidate_id ='.$userId)
				;

		$select = $db->fetchAll($query);
		return $select;

	}

	public function getJobLocation()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('c'=>'city'),array('distinct(country) as country'));


		$select = $db->fetchAll($query);
		return $select;

	}



	public function saveUser($val)
	{
		$db = $this->getAdapter();

		$verificationCode = time().rand(11111,99999);
		$data['username'] = $val['username'];
		$data['email'] = $val['email'];
		$data['password'] = $val['password'];
		$data['fname'] = $val['fname'];
		$data['lname'] = $val['lname'];

		$data['verification_code'] = $verificationCode;
		$data['status'] = 1;
		$data['usertype'] = 1;



 	  	$db->insert('users',$data);
		$id = $db->lastinsertid();

		$data1['user_id'] = $id;
		$data1['company_name'] = $val['company'];
		$data1['company_profile'] = $val['cprofile'];
		$data1['url'] = $val['url'];
		$data1['fname'] = $val['fname'];
		$data1['lname'] = $val['lname'];
		$data1['country'] = $val['country'];
		$data1['city'] = $val['city'];
		$data1['address'] = $val['address'];
		$data1['contact'] = $val['contact'];
		$data1['mobile'] = $val['mobile'];
		$data1['fax'] = $val['fax'];
		$data1['key_skills'] = $arr['keyskills'];



		$data1['job_category'] = implode(",",$val['category_id']);
		$data1['role_id'] = implode(",",$val['sub_category_id']);

		$db->insert('companydetails',$data1);

		$message = "Dear ".$val['username'].",<br><br>Thanks for signing up with FastestHiring.com. Please verify your details by clicking on the below link.<br><br>";
		$message .= WEBSITE."/verify/c/".$verificationCode;
		$message .="<br><br>Thanks,<br>Support Team";


		//$this->sendEmail($val['email'],'Sign Up Email',$message);

		return $id;

	}

	public function saveCandidate($val,$arr)
	{
		$db = $this->getAdapter();


		$verificationCode = time().rand(11111,99999);
		$data['username'] = $val['username'];
		$data['email'] = $val['email'];
		$data['password'] = $val['password'];
		$data['verification_code'] = $verificationCode;
		$data['fname'] = $val['fname'];
		$data['lname'] = $val['lname'];
		$data['status'] = 1;
		$data['usertype'] = 0;


 	  	$db->insert('users',$data);
		$id = $db->lastinsertid();

		$data1['user_id'] = $id;
		$data1['weight'] = $val['weight'];
		$data1['height'] = $val['height'];
		$data1['nationality'] = $val['country_id'];
		$loc = explode('-',$val['location_id']);

		$data1['current_location'] = $loc[0];
		$data1['mobile'] = $val['mobile'];
		$data1['gender'] = $val['gender'];
		$data1['marital_status'] = $val['maritalstatus'];
		$data1['mobile'] = $val['mobile'];
		$data1['phone'] = $val['phone'];


		$data1['role_id'] = $val['role_id'];

		$pref='';
		foreach($val['preference'] as $v)
			$pref .= $v.",";
		$data1['pref'] = $pref;
		$data1['notice'] = $val['notice'];
		$data1['dayoffpref'] = $val['offpreference'];
		$data1['expected_salary'] = $val['salary'];
		$data1['religion'] = $val['religion'];
		$data1['visa_type'] = $val['visa'];
		$data1['visa_date_expiry'] = $val['datepicker'];
		$data1['experience'] = $val['experience'];
		$data1['dob'] = $val['date_of_birth'];
		$data1['aboutme'] = addslashes($val['abtyou']);
		$data1['basic_education'] = $val['education_id'];
		$data1['key_skills'] = $arr['keyskills'];



		$data1['job_category'] = implode(",",$val['category_id']);
		$data1['role_id'] = implode(",",$val['sub_category_id']);
		$data1['language'] = implode(",",$val['language']);
		$data1['citypref'] = implode(",",$val['city']);

		$db->insert('candidate',$data1);

		foreach($val['language'] as $l)
		{
			$data2['language_id'] =$l;
			$data2['user_id'] =$id;
			$db->insert('user_language',$data2);
		}
		$number = trim($val['mobile']);
		$number = str_replace("-","",$number);
		$number = str_replace(" ","",$number);

		$message = "Fastest Hiring Verification Code:".$verificationCode;

		$this->sendsms($number,$message);
		//$message = "Dear ".$val['username'].",<br><br>Thanks for signing up with FastestHiring.com. Please verify your details by clicking on the below link.<br><br>";
		//$message .= WEBSITE."/verify/c/".$verificationCode;
		//$message .="<br><br>Thanks,<br>Support Team";


		//$this->sendEmail($val['email'],'Sign Up Email',$message);

		return;

	}


	public function updateCandidate($val,$userId,$arr)
	{
		$db = $this->getAdapter();


		$data['email'] = $val['email'];
		$data['fname'] = $val['fname'];
		$data['lname'] = $val['lname'];


 	  	$db->update('users',$data,"id=$userId");



		$data1['weight'] = $val['weight'];
		$data1['height'] = $val['height'];
		$data1['nationality'] = $val['country_id'];


		$data1['current_location'] = $val['location_id'];
		$data1['mobile'] = $val['mobile'];
		$data1['gender'] = $val['gender'];
		$data1['marital_status'] = $val['maritalstatus'];
		$data1['mobile'] = $val['mobile'];
		$data1['phone'] = $val['phone'];

		$data1['job_category'] = implode(",",$val['category_id']);
		$data1['role_id'] = implode(",",$val['sub_category_id']);
		$data1['language'] = implode(",",$val['language']);
		$data1['citypref'] = implode(",",$val['city']);
		$data1['pref'] = implode(",",$val['preference']);


		$data1['notice'] = $val['notice'];
		$data1['dayoffpref'] = $val['offpreference'];
		$data1['expected_salary'] = $val['salary'];
		$data1['religion'] = $val['religion'];
		$data1['visa_type'] = $val['visa'];
		$data1['visa_date_expiry'] = $val['datepicker'];
		$data1['experience'] = $val['experience'];
		//$data1['dob'] = $val['date_of_birth'];
		$data1['aboutme'] = addslashes($val['abtyou']);
		$data1['basic_education'] = $val['education_id'];
		$data1['key_skills'] = $arr['keyskills'];


		 	  	$db->update('candidate',$data1,"user_id=$userId");

		return;

	}


	public function sendEmail($email,$subject,$message)
	{
		    $mail = new Zend_Mail();

			$mail->setBodyHtml($message);
			$mail->setFrom('info@fastesthiring.com', 'Fastest Hiring');
			$mail->addTo($email);
			$mail->setSubject($subject);
			$mail->send();

	}



	public function getUsersCount()
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('u'=>'users'),array('count(*) as c'));
		$select = $db->fetchAll($query);
		return $select[0]['c'];

	}

	public function getsubcat($cats)
	{
		$db = $this->getAdapter();
 	  	$query = $db->select()
				->from(array('c'=>'category'),array('*'))
				->where("parent_category_id IN ('".$cats."')");
				//echo $query;exit;
		$select = $db->fetchAll($query);

		return $select;


	}


	 // This is for employer registration
    public function saveEmployer($val, $post) {

        $db = $this->getAdapter();
        $data = array();
        $verificationCode = time() . rand(11111, 99999);
        $data['username'] = $val['username'];
        $data['fname'] = $val['fname'];
        $data['lname'] = $val['lname'];
        $data['email'] = $val['email'];
        $data['password'] = $val['password'];
        $data['verification_code'] = $verificationCode;
        $data['status'] = 1;
        $data['usertype'] = 2;

        $db->insert('users', $data);
        $id = $db->lastinsertid();

        $data2 = array();

        $data2['user_id'] = $id;
        $data2['country'] = $val['country'];
        $data2['mobile'] = $val['mobile'];
        $data2['employer_type'] = $val['employer_type'];
        $data2['company_name'] = $val['company_name'];
        $data2['industry_type'] = $val['industry_type'];
        $data2['no_of_employees'] = $val['no_of_employee'];
        $data2['url'] = $val['url'];
        $data2['company_profile'] = $val['company_profile'];
        $data2['phone'] = $val['phone'];
        $data2['fax'] = $val['fax'];

        $db->insert('employer', $data2);


        $message = "Dear " . $val['username'] . ",<br><br>Thanks for signing up with FastestHiring.com. Please verify your details by clicking on the below link.<br><br>";
        $message .= WEBSITE . "/verify/c/" . $verificationCode;
        $message .="<br><br>Thanks,<br>Support Team";

        //$this->sendEmail($val['email'],'Sign Up Email',$message);

        return $id;
    }



	public function getcity($cats)
	{

		$db = $this->getAdapter();
		$_Val = 0;
		if (strpos($cats,"'All'") !== false) {
			$_Val = 1;
		}

		if($_Val ==1)
				$query = $db->select()
					->from(array('c'=>'city'),array('*'))
					->where("1");
		else
				$query = $db->select()
					->from(array('c'=>'city'),array('*'))
					->where("country IN (".$cats.")");


		$select = $db->fetchAll($query);

		return $select;


	}

	public function getskills($q)
	{

		$db = $this->getAdapter();
		 $query = $db->select()
					->from(array('c'=>'category'),array('category as value','category as name'))
					->where("parent_category_id > 0")
					->where("category like '%".$q."%'");


		$select = $db->fetchAll($query);

		return $select;
	}
	public function getlanguages()
	{

		$db = $this->getAdapter();
		 $query = $db->select()
					->from(array('c'=>'language'),array('*'));


		$select = $db->fetchAll($query);

		return $select;
	}

	public function getprofile($id)
	{
		 $db = $this->getAdapter();
		$query = $db->select()
					->from(array('u'=>'users'),array('*'))
					->joinLeft(array('c'=>'candidate'),'u.id = c.user_id')
					->where("u.id = ?",$id);

		$select = $db->fetchAll($query);

		$queryImg = $db->select()
					->from(array('u'=>'photos'),array('*'))
					->where("u.user_id = ?",$id);

		$selectImg = $db->fetchAll($queryImg);

		$select[0]['images'] = $selectImg;


		return $select;

	}

	public function insertphoto($file,$user)
	{
		$data['user_id'] = $user;
		$data['photo'] = $file;
		$db = $this->getAdapter();
		$db->insert('photos',$data);


	}
	public function getphotos($user)
	{

		 $db = $this->getAdapter();
		$query = $db->select()
					->from(array('u'=>'photos'),array('*'))
					->where("u.user_id = ?",$user);

		$select = $db->fetchAll($query);

		return $select;


	}

	public function saveProfilePhotos($ppArr,$id)
	{

		$data['profile_image'] = $ppArr;
		$db = $this->getAdapter();
		$db->update('users',$data,"id=".$id);
		return;
	}

	public function sendsms($number,$message)
	{
			//Please Enter Your Details
			$user="AnkurGakhar"; //your username
			$password="36890943"; //your password
			$mobilenumbers=$number; //enter Mobile numbers comma seperated

			$message = $message; //enter Your Message
			$senderid="SMSCntry"; //Your senderid
			$messagetype="N"; //Type Of Your Message
			$DReports="Y"; //Delivery Reports
			$url="http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
			$message = urlencode($message);
			$ch = curl_init();
			if (!$ch){die("Couldn't initialize a cURL handle");}
			$ret = curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt ($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt ($ch, CURLOPT_POSTFIELDS,
			"User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports");
			$ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//If you are behind proxy then please uncomment below line and provide your proxy ip with port.
			// $ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
			$curlresponse = curl_exec($ch); // execute
			if(curl_errno($ch))
			echo 'curl error : '. curl_error($ch);
			if (empty($ret)) {
			// some kind of an error happened
			die(curl_error($ch));
			curl_close($ch); // close cURL handler
			} else {
			$info = curl_getinfo($ch);
			curl_close($ch); // close cURL handler
			return;
	//		echo $curlresponse; //echo "Message Sent Succesfully" ;
		//	exit;
			}
	}

 /**
     * This is for saving the agency profile
     * @param type $val
     * @param type $post
     * @return type
     */
    public function saveAgency($val, $post) {

        $db = $this->getAdapter();
        $data = array();
        $verificationCode = time() . rand(11111, 99999);
        $data['username'] = $val['username'];
        $data['email'] = $val['email'];
        $data['password'] = $val['password'];
        $data['verification_code'] = $verificationCode;
        $data['status'] = 1;
        $data['usertype'] = 3;

        $db->insert('users', $data);
        $id = $db->lastinsertid();

        $data2 = array();

        $data2['user_id'] = $id;
        $data2['agency_name'] = $val['agency_name'];
        $data2['contact_person_name'] = $val['contact_person_name'];
        $data2['country'] = $val['country'];
        $data2['mobile'] = $val['mobile'];
        $data2['fax'] = $val['fax'];
        $data2['phone'] = $val['phone'];
        $data2['agency_website'] = $val['agency_website'];
        $data2['about_agency'] = $val['about_agency'];

        $db->insert('agency', $data2);


        $message = "Dear " . $val['username'] . ",<br><br>Thanks for signing up with FastestHiring.com. Please verify your details by clicking on the below link.<br><br>";
        $message .= WEBSITE . "/verify/c/" . $verificationCode;
        $message .="<br><br>Thanks,<br>Support Team";

        //$this->sendEmail($val['email'],'Sign Up Email',$message);

        return $id;
    }


	public function saveJob($data)
	{
		$db = $this->getAdapter();



 	  	$db->insert('jobs',$data);
		$id = $db->lastinsertid();

		//$this->sendsms($number,$message);
		//$message = "Dear ".$val['username'].",<br><br>Thanks for signing up with FastestHiring.com. Please verify your details by clicking on the below link.<br><br>";
		//$message .= WEBSITE."/verify/c/".$verificationCode;
		//$message .="<br><br>Thanks,<br>Support Team";


		//$this->sendEmail($val['email'],'Sign Up Email',$message);

		return $id;

	}

	public function approvejob($job)
	{
		$db = $this->getAdapter();
		$data['status'] =1;
		$db->update('jobs',$data,"id=$job");
		return;
	}

   /**
     *
     * @param type $id
     * @return type
     */
        public function getagencyprofile($id) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('u' => 'users'), array('*'))
                ->joinLeft(array('a' => 'agency'), 'u.id = a.user_id')
                ->where("u.id = ?", $id);

        $select = $db->fetchRow($query);

        $queryImg = $db->select()
                ->from(array('u' => 'photos'), array('*'))
                ->where("u.user_id = ?", $id);

        $selectImg = $db->fetchRow($queryImg);

        $select['images'] = $selectImg;


        return $select;
    }



    /**
     * This will update the agency
     * @param type $val
     * @param type $userId
     * @param type $arr
     * @return type
     */
    public function updateAgency($val, $userId, $arr) {
        $db = $this->getAdapter();



        $data['email'] = $val['email'];
        $data['password'] = $val['password'];

        $db->update('users', $data, "id=$userId");

        $data1['agency_name'] = $val['agency_name'];
        $data1['contact_person_name'] = $val['contact_person_name'];
        $data1['country'] = $val['country'];


        $data1['mobile'] = $val['mobile'];
        $data1['fax'] = $val['fax'];
        $data1['phone'] = $val['phone'];
        $data1['agency_website'] = $val['agency_website'];
        $data1['about_agency'] = $val['about_agency'];


        $db->update('agency', $data1, "user_id=$userId");

        return;
    }

    /**
     * This is to save candidate by agency
     * @param type $val
     * @param type $arr
     * @return type
     */
      public function saveCandidateByAgency($val, $arr, $currentUserId) {


        $db = $this->getAdapter();

        $verificationCode = time() . rand(11111, 99999);
        $data['username'] = $val['username'];
        $data['email'] = $val['email'];
        $data['password'] = 'qwerty@123'; //Default password for candidates added by agency
        $data['verification_code'] = $verificationCode;
        $data['fname'] = $val['fname'];
        $data['lname'] = $val['lname'];
        $data['status'] = 1;
        $data['usertype'] = 0;


        $db->insert('users', $data);
        $id = $db->lastinsertid();

        $data1['user_id'] = $id;
        $data1['weight'] = $val['weight'];
        $data1['height'] = $val['height'];
        $data1['nationality'] = $val['country_id'];
        $loc = explode('-', $val['location_id']);

        $data1['current_location'] = $loc[0];
        $data1['mobile'] = $val['mobile'];
        $data1['gender'] = $val['gender'];
        $data1['marital_status'] = $val['maritalstatus'];
        //$data1['mobile'] = $val['mobile'];  // Mobile number is not needed
        $data1['phone'] = $val['phone'];


        $data1['role_id'] = $val['role_id'];

        $pref = '';
        foreach ($val['preference'] as $v)
            $pref .= $v . ",";
        $data1['pref'] = $pref;
        $data1['notice'] = $val['notice'];
        $data1['dayoffpref'] = $val['offpreference'];
        $data1['expected_salary'] = $val['salary'];
        $data1['religion'] = $val['religion'];
        $data1['visa_type'] = $val['visa'];
        $data1['visa_date_expiry'] = $val['datepicker'];
        $data1['addedby'] = $currentUserId;
        $data1['experience'] = $val['experience'];
        $data1['dob'] = $val['date_of_birth'];
        $data1['aboutme'] = addslashes($val['abtyou']);
        $data1['basic_education'] = $val['education_id'];
        $data1['key_skills'] = $arr['keyskills'];
        $data1['job_category'] = implode(",", $val['category_id']);
        $data1['role_id'] = implode(",", $val['sub_category_id']);
        $data1['language'] = implode(",", $val['language']);
        $data1['citypref'] = implode(",", $val['city']);



        $db->insert('candidate', $data1);

        foreach ($val['language'] as $l) {
            $data2['language_id'] = $l;
            $data2['user_id'] = $id;
            $db->insert('user_language', $data2);
        }
        $number = trim($val['mobile']);
        $number = str_replace("-", "", $number);
        $number = str_replace(" ", "", $number);

        $message = "Fastest Hiring Verification Code:" . $verificationCode;

        //$this->sendsms($number, $message);
        //$message = "Dear ".$val['username'].",<br><br>Thanks for signing up with FastestHiring.com. Please verify your details by clicking on the below link.<br><br>";
        //$message .= WEBSITE."/verify/c/".$verificationCode;
        //$message .="<br><br>Thanks,<br>Support Team";
        //$this->sendEmail($val['email'],'Sign Up Email',$message);

        return $id;
    }


	function getFeaturedCandidates()
	{
		$db = $this->getAdapter();
		$query = $db->select()
					->from(array('c'=>'candidate'),array('*'))
					->joinLeft(array('u'=>'users'),'u.id = c.user_id')
					->joinLeft(array('s'=>'shortlist_candidate'),'c.user_id = s.candidate_id')
					->joinLeft(array('a'=>'apply_candidate'),'c.user_id = a.candidate_id',array('a.employer_id as jobseeker_id'))
					->limit('5')
					->order('c.user_id desc');

			$select = $db->fetchAll($query);
		return $select;
	}

	function interestedcandidates($jid,$page)
	{
		$db = $this->getAdapter();
		$query = $db->select()
					->from(array('a'=>'apply_jobs'),array('*'))
					->joinLeft(array('c'=>'candidate'),'a.candidate_id = c.user_id')
					->joinLeft(array('u'=>'users'),'c.user_id = u.id')
					->where('a.job_id ='.$jid)
					->order('a.posted_date desc');

		$select = $db->fetchAll($query);
		return $select;
	}


		function myjobscount($uid)
		{
			$db = $this->getAdapter();
			$query = $db->select()
					->from(array('j'=>'jobs'),array('*'))
					->where('employer_id='.$uid)
					->where('status != -1');

			$select = $db->fetchAll($query);
		return count($select);

		}
		function shortlistcandidatecount($uid)
		{
			$db = $this->getAdapter();
			$query = $db->select()
					->from(array('s'=>'shortlist_candidate'),array('*'))
					->where('employer_id='.$uid);


			$select = $db->fetchAll($query);
		return count($select);

		}

		function expressinterestcount($uid)
		{
			$db = $this->getAdapter();
			$query = $db->select()
					->from(array('s'=>'apply_candidate'),array('*'))
					->where('employer_id='.$uid);


			$select = $db->fetchAll($query);
		return count($select);

		}

		function myjobs($uid)
		{
			$db = $this->getAdapter();
			$query = $db->select()
					->from(array('j'=>'jobs'),array('*'))
					->where('employer_id='.$uid);


			$select = $db->fetchAll($query);
			return $select;

		}

		function getRoles()
		{
			$db = $this->getAdapter();
			$query = $db->select()
					->from(array('c'=>'category'),array('*'))
					->where('parent_category_id != 0')
					->order('category');
			$select = $db->fetchAll($query);
			return $select;
				}


    /**
     * This function will return all the candidates added by agency
     * @param type $agencyId
     * @return type
     */
    public function getCandidatesByAgencyId($agencyId, $start, $limit) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('c' => 'candidate'), array('*'))
                ->join(array('u' => 'users'), 'c.user_id = u.id', array('*'))
                ->where('c.addedby=' . $agencyId)
                 ->limit($limit, $start);

        $select = $db->fetchAll($query);
        return $select;
    }

    /**
     * This function will return the candidate by id
     * @param type $agencyId
     * @return type
     */
    public function getCandidateById($candidateId) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('c' => 'candidate'), array('*'))
                ->join(array('u' => 'users'), 'c.user_id = u.id', array('*'))
                ->where('u.id=' . $candidateId);

        $select = $db->fetchRow($query);
        return $select;
    }


    /**
     * Update candidate by id added by agency
     * @param type $val
     * @param type $userId
     * @param type $arr
     * @return type
     */
    public function updateAgencyCandidateById($val, $userId, $agencyId, $arr) {

        $db = $this->getAdapter();


        $data['email'] = $val['email'];
        $data['fname'] = $val['fname'];
        $data['lname'] = $val['lname'];


        $db->update('users', $data, "id=$userId");



        $data1['weight'] = $val['weight'];
        $data1['height'] = $val['height'];
        $data1['nationality'] = $val['country_id'];


        $data1['current_location'] = $val['location_id'];
//        $data1['mobile'] = $val['mobile'];
        $data1['gender'] = $val['gender'];
        $data1['marital_status'] = $val['maritalstatus'];
        $data1['mobile'] = $val['mobile'];
        $data1['phone'] = $val['phone'];

        $data1['job_category'] = implode(",", $val['category_id']);
        $data1['role_id'] = implode(",", $val['sub_category_id']);
        $data1['language'] = implode(",", $val['language']);
        $data1['citypref'] = implode(",", $val['city']);
        $data1['pref'] = implode(",", $val['preference']);


        $data1['notice'] = $val['notice'];
        $data1['dayoffpref'] = $val['offpreference'];
        $data1['expected_salary'] = $val['salary'];
        $data1['religion'] = $val['religion'];
        $data1['visa_type'] = $val['visa'];
        $data1['visa_date_expiry'] = $val['datepicker'];
        $data1['experience'] = $val['experience'];
        //$data1['dob'] = $val['date_of_birth'];
        $data1['aboutme'] = addslashes($val['abtyou']);
        $data1['basic_education'] = $val['education_id'];
        $data1['key_skills'] = $arr['keyskills'];


        $db->update('candidate', $data1, "user_id=$userId");

        return $userId;
    }


    /**
     * This function will return all the candidates added by agency total count for paging
     * @param type $agencyId
     * @return type
     */
    public function getCandidatesByAgencyIdTotalCount($agencyId) {
        $db = $this->getAdapter();
        $query = $db->select()
                ->from(array('c' => 'candidate'), array('*'))
                ->join(array('u' => 'users'), 'c.user_id = u.id', array('*'))
                ->where('c.addedby=' . $agencyId);


        $select = $db->fetchAll($query);
        return count($select);
    }


}