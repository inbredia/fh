<?php session_start();
require_once("../codelibrary/inc/variables.php");
require_once("../codelibrary/inc/functions.php");
validate_admin();


	$sqljobs = "select * from jobs where status=0";
		
        $sqljobsres = mysql_query($sqljobs);
	$sqljobsnum = mysql_num_rows($sqljobsres);
        
        $sqlemployer = "select * from users where usertype=2 and status='0'";
		
        $sqlemployerres = mysql_query($sqlemployer);
	$sqlemployernum = mysql_num_rows($sqlemployerres);

       $sqlcandidate = "select * from candidate where video_url=1";
		
        $sqlcandidateres = mysql_query($sqlcandidate);
	$sqlcandidatenum = mysql_num_rows($sqlcandidateres);


if($_REQUEST['act']=='expirejobs')
{
     $sql_jobs = "select * from jobs where (posted_date < DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) and status!=-2";		
		
        $sql_jobs_res = mysql_query($sql_jobs);
	$sqljobsnum = mysql_num_rows($sql_jobs_res);
	
	$sqljob = "update jobs set status=-2 where (posted_date < DATE_SUB(CURDATE(), INTERVAL 1 MONTH))  and status!=-2";		
    mysql_query($sqljob);
	

}

if($_REQUEST['act']=='updatepackages')
{
     
	$sqljob = "update users set job_post=15 where (job_post < 15) and (usertype != 0)";		
    mysql_query($sqljob);
	
	$sqljob = "update users set bulk_sms=50 where (bulk_sms < 50)  and (usertype != 0)";		
    mysql_query($sqljob);
	
	$sqljob = "update users set express_interest=15 where (express_interest < 15)  and (usertype != 0)";		
    mysql_query($sqljob);
	
}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo ucfirst(SITE_ADMIN_TITLE);?></title>
<link href="codelibrary/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php include("header.inc.php");?>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="209" valign="top" class="rightBorder">
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><?php include("left_menu.inc.php");?></td>
        </tr>

      </table>
    <br />
    <br /></td>
    <td width="1"><img src="image/spacer.gif" width="1" height="1" /></td>
    <td height="400" align="center" valign="top">
			<table width="100%"  border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="21" align="left" class="adminGrey">Admin Home</td>
          </tr>
          <tr>
            <td height="400" align="center" valign="top"><br>
              <table width="80%" height="200" border="0" cellpadding="5" cellspacing="0">
                <tr align="center">
                      <td class="txt" align="left" valign="top">
					  Welcome to <?php echo ucfirst(SITE_TITLE);?> Administration Suite<br>
                        Please use the navigation links on the left side to access 
                        different sections of the main administration suite.</td>
                </tr>
                <tr align="center">
                  <td>&nbsp;</td>
                </tr>
                <tr align="center">
                  <td>&nbsp;</td>
                </tr>
                  <tr align="left">
                  <td colspan="2" class="heading">Pending Requests</td> 
                </tr> 
                  
                  <tr align="left" class="txt">
                      <td ><a href="unapprovedjobs_list.php">Unapproved Jobs Requests (<?=$sqljobsnum?>)</a></td> <td ><a href="unapprovedemployer_list.php">Unapproved Employers Requests (<?=$sqlemployernum?>)</a></td> 
                </tr> 
                   <tr align="left">
                  <td colspan="2" class="heading"><a href="candidate_list.php">Candidate Video Url Requests (<?=$sqlcandidatenum?>)</a></td> 
                </tr> 
				  </tr>
                  <tr align="left">
                  <td colspan="2" class="heading"><br><br><br><br>Expire Jobs Manually</td> 
                </tr> 
                  <?if($_REQUEST['act']=='expirejobs'){?>
				  <tr align="left" class="txt">
                      <td ><font color=red><?=$sqljobsnum?> Jobs expired successfully.</font></td> 
                </tr> 
				  <?}?>
                <tr align="left" class="txt">
                      <td ><a href="admin_home.php?act=expirejobs">Expire Jobs (Limit 30 Days)</a></td> 
					  
                </tr> 
                   
				<tr align="left">
                  <td colspan="2" class="heading"><br><br><br><br>Update Packages </td> 
                </tr> 
                  <?if($_REQUEST['act']=='updatepackages'){?>
				  <tr align="left" class="txt">
                      <td ><font color=red> Packages Updated successfully.</font></td> 
                </tr> 
				  <?}?>
                <tr align="left" class="txt">
                      <td ><a href="admin_home.php?act=updatepackages">Package Update (Limit 7 Days)</a></td> 
					  
                </tr> 
				
				
				 
				 <tr align="left">
                  <td colspan="2" class="heading"><br><br><br><br>Expire Featured Candidate Manually</td> 
                </tr> 
                  <?if($_REQUEST['act']=='expirejobs'){?>
				  <tr align="left" class="txt">
                      <td ><font color=red><?=$sqljobsnum?> Jobs expired successfully.</font></td> 
                </tr> 
				  <?}?>
                <tr align="left" class="txt">
                      <td ><a href="admin_home.php?act=expirefeatured">Expire Featured Candidate (Limit 30 Days)</a></td> 
					  
                </tr> 
              </table>
            </td>
          </tr>
        </table>
		</td>
  </tr>
</table>
<?php include("footer.inc.php");?>
</body>
</html>
