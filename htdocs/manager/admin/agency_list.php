<?php
session_start();
require_once("../codelibrary/inc/variables.php");
require_once("../codelibrary/inc/functions.php");
validate_admin();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo ucfirst(SITE_ADMIN_TITLE);?></title>
<link href="codelibrary/css/style.css" rel="stylesheet" type="text/css" />
<script src="../codelibrary/js/script_tmt_validator.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
function checkall(objForm)
{
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++){
		if (objForm.elements[i].type=='checkbox')
			objForm.elements[i].checked=objForm.check_all.checked;
	}
}

function del_prompt(frmobj,comb,id)
{
	if(comb=='Delete'){
		if(confirm ("Are you sure you want to delete Record(s)")){
			frmobj.action = "agency_del.php";
			frmobj.submit();
			}
		else{
			return false;
		}
	}
	else if(comb=='Deactivate'){
		frmobj.action = "agency_del.php";
		frmobj.submit();
	}
	else if(comb=='Activate'){
		frmobj.action = "agency_del.php";
		frmobj.submit();
	}
	else if(comb=='Special'){
		frmobj.action = "agency_del.php";
		frmobj.submit();
	}
	else if(comb=='Remove Special'){
		frmobj.action = "agency_del.php";
		frmobj.submit();
	}

}
</script>
</head>
<body>
<?php include("header.inc.php");?>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="180" valign="top" class="rightBorder">
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><?php include("left_menu.inc.php");?></td>
        </tr>
        <tr>
          <td width="23">&nbsp;</td>
        </tr>
      </table>
    <br />
    <br /></td>
   <td width="1" bgcolor="#5367D0"><img src="image/spacer.gif" width="1" height="1" /></td>
    <td width="1"><img src="image/spacer.gif" width="1" height="1" /></td>
    <td height="400" align="center" valign="top">
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="21" align="left" class="txt">
				<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="title">
                    <tr bgcolor="#EDEDED">
                      <td width="100%" height="25"><img src="image/heading_icon.gif" width="16" height="16" hspace="5">Manage Agency </td>
<!--                      <td width="24%" align="right"><input name="b1" type="button" class="button" id="b1" value="Add user" onClick="location.href='user_addf.php'">-->
                      &nbsp;</td>
                    </tr>
              </table>
			</td>
          </tr>
		  <?php $start=0;
if(isset($_GET['start'])) $start=$_GET['start'];
$pagesize=20;
if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];
$order_by='u.id';
if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];
$order_by2='desc';
if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];
$sql=executeQuery("select *,(select count(*) from jobs where employer_id=u.id) as num_jobs,(select count(*) from candidate where addedby=u.id) as num_candidate  from users u left join agency a on u.id=a.user_id where u.usertype=3 order by $order_by $order_by2 limit $start,$pagesize");
$reccnt=mysql_num_rows(executeQuery("select * from users where usertype=3"));?>
          <tr>
            <td height="400" align="center" valign="top"><br>
              <table width="98%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                  <td height="347" align="center" valign="top">
				  <span class="warning"><?php print $_SESSION['sess_msg']; session_unregister('sess_msg'); $sess_msg='';?></span>
				  <br />
				  <form name="frm_list" method="post" >
				  <table width="98%" border="0" align=center cellpadding="4" cellspacing="1"  class="greyBorder">
				  <?php if($reccnt>0){?>
				  	<tr bgcolor="#4096AF" class="blueBackground">
					<TD width="16%" align="center"><strong>Sr. Number. </strong></TD>
					  <TD width="12%" align="center"><strong>Username<?php echo sort_arrows('username')?></strong></TD>
					  <TD width="20%" align="center"><strong>Email<?php echo sort_arrows('email')?></strong></TD>
<!--					  <TD width="26%" align="center"><strong>Image<?php echo sort_arrows('logo')?></strong></TD>-->
					<TD width="26%" align="center"><strong>Mobile</strong></TD>
					<TD width="26%" align="center"><strong>Num of Jobs</strong></TD>
					<TD width="26%" align="center"><strong>Num Of Candidates</strong></TD>
					  <TD width="10%" align="center"><strong>Status <?php echo sort_arrows('status')?></strong></TD>
<!--					  <td width="7%" align="center"><b>Action</b></td>-->
					  <td width="9%" align="center" class="heading">
					  <input name="check_all" type="checkbox" id="check_all" value="check_all" onClick="checkall(this.form)">
					  </td>
					</tr>
					<?php $i=0;
					while($line=mysql_fetch_array($sql)){
					$className = ($className == "evenRow")?"oddRow":"evenRow";
					$i++;?>
					<tr class="<?php print $className?>">
					<TD align="center" class="txt" ><?php echo $i?></TD>
					 <TD align="center" class="txt" ><?=$line['username']?></TD>
					 <TD align="center" class="txt" ><?php echo $line['email'];?></TD>
					 <TD align="center" class="txt" ><?php echo $line['mobile'];?></TD>
					 <TD align="center" class="txt" ><?php echo $line['num_jobs'];?></TD>
					 <TD align="center" class="txt" ><?php echo $line['num_candidate'];?></TD>
<!--					 <TD align="center" class="txt" ><img src="../upload_image/thumb/<?php echo $line['image']?>" width="50" height="50" /></TD>-->
					  <TD align="center" class="txt"><?php if($line['status']==1){?>Activated<?php }else{?>Deactivated<?php }?></TD>
<!--					  <td valign="middle" align="center"><a href="user_addf.php?id=<?php print $line[id]?>" class="orangetxt">Edit</a></td>-->
					  <td width="9%" valign="middle" align="center"><input type="checkbox" name="ids[]" value="<?php print $line[0]?>"></td>
					</tr>
					<?php }?>
					<?php $className = ($className == "evenRow")?"oddRow":"evenRow";?>
					<tr align="right" class="<?php print $className?>">
					  <td colspan="8"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
					 <tr>
							<td width="26%"  align="left">

							<?php include("../codelibrary/inc/paging.inc.php");?></td><td width="74%" align="right">

							<input type="submit" name="submit" value="Activate" class="button" onclick="return del_prompt(this.form,this.value,'agency_del.php')">
							<input type="submit" name="submit" value="Deactivate" class="button" onclick="return del_prompt(this.form,this.value,'agency_del.php')">
							<input type="submit" name="submit" value="Delete" class="button" onclick="return del_prompt(this.form,this.value,'agency_del.php')">
							</td></tr>
                      </table></td>
					</tr>
			     <?php }else{?>
				    <tr align="center" class="oddRow">
					  <td colspan="8" class="warning">Sorry, There are currently no record to display</td>
					</tr>
				 <?php }?>
			     </table>
				 </form>
				 </td>
			   </tr>
			   <tr align="center">
                 <td>&nbsp;</td>
               </tr>
               <tr align="center">
                 <td>&nbsp;</td>
               </tr>
            </table>
         </td>
       </tr>
     </table>
	</td>
	<td width="20" valign="top" bgcolor="#EDEDED">&nbsp;</td>
  </tr>
</table>
<?php include("footer.inc.php");?>
</body>
</html>