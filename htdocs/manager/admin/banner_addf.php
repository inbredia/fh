<?php
session_start();
require_once("../codelibrary/inc/variables.php");
require_once("../codelibrary/inc/functions.php");
include(FCK_PATH . "fckeditor.php");
validate_admin();
@extract($_REQUEST);



if ($_POST['submitForm'] == "yes") {



    if ($_FILES['image']['size'] > 0) {
        $image1 = date("YmdHis") . $_FILES['image']['name'];
        move_uploaded_file($_FILES['image']['tmp_name'], "../upload_images/banner/" . $image1);
       // @resize_img("../upload_images/banner/" . $image1, 100, 70, false, 80, 0, "");
    }


    if ($id == '') {
        $sql = "insert into tbl_banner set banner_name='{$banner_name}' ,position='{$position}', link='{$link}', status=1, image='{$image1}' ";
      
        executeUpdate($sql);
        $_SESSION[sess_msg] = "Successfully Added";
    } else {

        if(!empty($image1))
            $imageQuery = " , image='{$image1}' ";
        else
            $imageQuery = " ";
        
        
        $sql = "update tbl_banner set banner_name='{$banner_name}' ,position='{$position}', link='{$link}' {$imageQuery}  where  id = '$id'";
        executeUpdate($sql);
        $_SESSION[sess_msg] = "Successfully Updated";
    }

    header("Location: banner_list.php");
    exit();
}
if ($id) {
    $sql = "select * from tbl_banner where id='$id'";
    $result = executeQuery($sql);
    $num = mysql_num_rows($result);
    if ($line = ms_stripslashes(mysql_fetch_array($result))) {
        @extract($line);
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?php echo ucfirst(SITE_ADMIN_TITLE); ?></title>
        <link href="codelibrary/css/style.css" rel="stylesheet" type="text/css" />
        <script language="javascript">
            function validate_form(obj)
            {
                if(obj.video_url.value == '')
                {
                    alert(" Please Enter Video Url" );
                    return false;
                }else if(obj.displayname.value == '')
                {
                    alert(" Please Enter Display Name");
                    return false;
                }
                else
                {
                    return true;
                } 
            }
        </script>




    </head>
    <body >
        <?php include("header.inc.php"); ?>
        <table width="100%"  border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="180" valign="top" class="rightBorder">
                    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center"><?php include("left_menu.inc.php"); ?></td>
                        </tr>
                        <tr>
                            <td width="23">&nbsp;</td>
                        </tr>
                    </table>
                    <br />
                    <br /></td>
                <td width="1" bgcolor="#5367D0"><img src="image/spacer.gif" width="1" height="1" /></td>
                <td width="1"><img src="image/spacer.gif" width="1" height="1" /></td>
                <td height="400" align="center" valign="top">
                    <table width="100%"  border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td height="21" align="left" class="txt">
                                <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="title">
                                    <tr bgcolor="#EDEDED">
                                        <td width="76%" height="25"><img src="image/heading_icon.gif" width="16" height="16" hspace="5">Manage Banner</td>
                                        <td width="24%" align="right"><input name="b1" type="button" class="button" id="b1" value="Banner Manager" onClick="location.href='banner_list.php'">
                                                &nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="400" align="center" valign="top"><br>
                                    <form action="banner_addf.php" method="post" enctype="multipart/form-data" name="frm"    onsubmit="return validate_form(this)">
                                        <input type="hidden" name="submitForm" value="yes">
                                            <input type="hidden" name="id" class="txtfld" value="<?php echo $id; ?>">
                                                <table width="68%" border="0" align=center cellpadding="4" cellspacing="0" class="greyBorder">
                                                    <TR align="left"bgcolor="#4096AF"> 
                                                        <TD height="25" colspan="2" class="blueBackground">
                                                            <?php if ($id == '') { ?>
                                                                Add New
                                                            <?php } else { ?>
                                                                Edit
                                                            <?php } ?> 
                                                            banner Details</TD>
                                                    </TR>
                                                    <?php if ($_SESSION['sess_msg'] != '') { ?>
                                                        <tr>
                                                            <td colspan="2" align="center"  class="warning"><?php print $_SESSION['sess_msg'];
                                                        session_unregister('sess_msg');
                                                        $sess_msg = ''; ?></td>
                                                        </tr>
<?php } ?>
                                                    <tr class="oddRow">
                                                        <td class="txt" align="right" colspan="2"><span class="warning">*</span> - Required Fields</td>
                                                    </tr>
                                                    <tr class="oddRow">
                                                        <td width="32%" align="right" class="bldTxt">Banner Name :</td>
                                                        <td width="68%" align="left"><input type="text" name="banner_name" size="45" class="txtfld" value="<?php echo $line["banner_name"]; ?>" /> <span class="warning">*</span></td>
                                                    </tr>
                                                    <tr class="evenRow">
                                                        <td width="32%" align="right" class="bldTxt">Banner Position :</td>
                                                        <td width="68%" align="left">

                                                            <select name="position">
                                                                <option value="">Select Position</option>
                                                                <option value="Left" <?php if ($line['position'] == 'Left') { ?> Selected<? } ?> > Left [60 x 120]</option>
                                                                <option value="Right" <?php if ($line['position'] == 'Right') { ?> Selected<? } ?>> Right [120 x 300]</option>
                                                                <option value="Top" <?php if ($line['position'] == 'Top') { ?> Selected<? } ?>> Top [300 x 120]</option>
                                                                <option value="Bottom" <?php if ($line['position'] == 'Bottom') { ?> Selected<? } ?>> Bottom [300 x 120]</option>


                                                            </select>

                                                            <span class="warning">*</span></td>
                                                    </tr>
                                                    <tr class="oddRow">
                                                        <td width="32%" align="right" class="bldTxt">Banner Link :</td>
                                                        <td width="68%" align="left"><input type="text" name="link" size="45" class="txtfld"  value="<?php echo $line['link'] ?>" /> <span class="warning">*</span></td>
                                                    </tr>

                                                    <tr class="evenRow">
                                                        <td width="32%" align="right" class="bldTxt">Upload Banner :</td>
                                                        <td width="68%" align="left"><input type="file" name="image"  /><br />
                                                            <?php if ($line['image']) {
                                                                ?>
                                                                <img src="../upload_images/banner/<?= $line['image'] ?>" />
<? } ?>
                                                        </td>
                                                    </tr>


                                                    <TR class="oddRow">
                                                        <TD align=center colspan=100%><input type="submit" class="button" value="Submit"/> <input type="reset" name="reset" class="button" value="Reset" /></TD>
                                                    </TR>
                                                </table>
                                                </form>
                                                </td>
                                                </tr>
                                                </table>
                                                </td>
                                                <td width="20" valign="top" bgcolor="#EDEDED">&nbsp;</td>
                                                </tr>
                                                </table>
<?php include("footer.inc.php"); ?>
                                                </body>
                                                </html>