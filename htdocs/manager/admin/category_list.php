<?php 
session_start();
require_once("../codelibrary/inc/variables.php");
require_once("../codelibrary/inc/functions.php");
validate_admin();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo ucfirst(SITE_ADMIN_TITLE);?></title>
<link href="codelibrary/css/style.css" rel="stylesheet" type="text/css" />
<script src="../codelibrary/js/script_tmt_validator.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
function checkall(objForm)
{
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++){
		if (objForm.elements[i].type=='checkbox') 
			objForm.elements[i].checked=objForm.check_all.checked;
	}
}

function del_prompt(frmobj,comb,id)
{
	if(comb=='Delete'){
		if(confirm ("Are you sure you want to delete Record(s)")){
			frmobj.action = "category_del.php";
			frmobj.submit();
			}
		else{ 
			return false;
		}
	}
	else if(comb=='Deactivate'){
		frmobj.action = "category_del.php";
		frmobj.submit();
	}
	else if(comb=='Activate'){
		frmobj.action = "category_del.php";
		frmobj.submit();
	}
	else if(comb=='Feature'){
		frmobj.action = "category_del.php";
		frmobj.submit();
	}
	else if(comb=='Remove Feature'){
		frmobj.action = "category_del.php";
		frmobj.submit();
	}
	
}
</script>
</head>
<body>
<?php include("header.inc.php");?>
<table width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="180" valign="top" class="rightBorder">
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><?php include("left_menu.inc.php");?></td>
        </tr>
        <tr>
          <td width="23">&nbsp;</td>
        </tr>
      </table>
    <br />
    <br /></td>
   <td width="1" bgcolor="#5367D0"><img src="image/spacer.gif" width="1" height="1" /></td>
    <td width="1"><img src="image/spacer.gif" width="1" height="1" /></td>
    <td height="400" align="center" valign="top">
		<table width="100%"  border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="21" align="left" class="txt">
				<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="title">
                    <tr bgcolor="#EDEDED">
                      <td width="76%" height="25"><img src="image/heading_icon.gif" width="16" height="16" hspace="5">Manage Category </td>
                      <td width="24%" align="right"><input name="b1" type="button" class="button" id="b1" value="Add Category" onClick="location.href='category_addf.php'">
                      &nbsp;</td>
                    </tr>
              </table>
			</td>
          </tr>
		  <?php $start=0;
if(isset($_GET['start'])) $start=$_GET['start'];
$pagesize=20;
if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];
$order_by='id';
if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];
$order_by2='desc';
if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];
$sql=executeQuery("select * from category order by $order_by $order_by2 limit $start,$pagesize");
$reccnt=mysql_num_rows(executeQuery("select * from category "));?>
          <tr>
            <td height="400" align="center" valign="top"><br>
              <table width="98%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                  <td height="347" align="center" valign="top">
				  <span class="warning"><?php print $_SESSION['sess_msg']; session_unregister('sess_msg'); $sess_msg='';?></span>
				  <br />
				  <form name="frm_list" method="post" >
				  <table width="98%" border="0" align=center cellpadding="4" cellspacing="1"  class="greyBorder">
				  <?php if($reccnt>0){?>
				  	<tr bgcolor="#4096AF" class="blueBackground">
					<TD width="9%" align="center"><strong>S.No. </strong></TD>
					  <TD width="36%" align="left"><strong>Category <?php echo sort_arrows('category')?></strong></TD>
					  <TD width="25%" align="center"><strong>Parent</strong></TD>
					  
					  <td width="6%" align="center"><b>Action</b></td>
					  <td width="4%" align="center" class="heading"> 
					  <input name="check_all" type="checkbox" id="check_all" value="check_all" onClick="checkall(this.form)">
					  </td>
					</tr>
					<?php $i=0;
					while($line=mysql_fetch_array($sql)){
					$className = ($className == "evenRow")?"oddRow":"evenRow";
					$i++;?>
					<tr class="<?php print $className?>">
					<TD align="center" class="txt" ><?php echo $i?></TD> 
					 <TD align="left" class="txt" ><?php echo ucwords($line['category']);?></TD>
					 <?php $got=mysql_query("select * from category where id='".$line['parent_category_id']."'");
					 		$gotrow=mysql_num_rows($got);
							if($gotrow>0)
							{
							$gg=mysql_fetch_assoc($got);
							$pat=$gg['category'];
							}
							else
							{
								$pat='';
							}?>
					 <TD align="center" class="txt" >&nbsp;<?=$pat?></TD>
					  
					  <td valign="middle" align="center"><a href="category_addf.php?id=<?php print $line[0]?>" class="orangetxt">Edit</a></td>
					  <td width="4%" valign="middle" align="center"><input type="checkbox" name="ids[]" value="<?php print $line[0]?>"></td>
					</tr>
					<?php }?>
					<?php $className = ($className == "evenRow")?"oddRow":"evenRow";?>
					<tr align="right" class="<?php print $className?>"> 
					  <td colspan="7"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
					 <tr>
							<td  align="left" class="txt">
							<br /><?php include("../codelibrary/inc/paging.inc.php");?></td><td align="right">
							<!--<input type="submit" name="Submit" value="Feature" class="button" onclick="return del_prompt(this.form,this.value,'category_del.php')">
							<input type="submit" name="Submit" value="Remove Feature" class="button" onclick="return del_prompt(this.form,this.value,'category_del.php')">
							<input type="submit" name="Submit" value="Activate" class="button" onclick="return del_prompt(this.form,this.value,'category_del.php')">
							<input type="submit" name="Submit" value="Deactivate" class="button" onclick="return del_prompt(this.form,this.value,'category_del.php')">
							<input type="submit" name="Submit" value="Delete" class="button" onclick="return del_prompt(this.form,this.value,'category_del.php')">-->
							</td></tr>
                      </table></td>
					</tr>
			     <?php }else{?>
				    <tr align="center" class="oddRow">
					  <td colspan="7" class="warning">Sorry, There are currently no record to display</td>
					</tr>
				 <?php }?>
			     </table>
				 </form>
				 </td>
			   </tr>
			   <tr align="center">
                 <td>&nbsp;</td>
               </tr>
               <tr align="center">
                 <td>&nbsp;</td>
               </tr>
            </table>
         </td>
       </tr>
     </table>
	</td>
	<td width="20" valign="top" bgcolor="#EDEDED">&nbsp;</td>
  </tr>
</table>
<?php include("footer.inc.php");?>
</body>
</html>