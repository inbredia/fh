<?php
session_start();
require_once("../codelibrary/inc/variables.php");
session_unregister("sess_msg");
session_unregister("sess_uid");
session_unregister("sess_username");
$_COOKIE['sess_admin_id']="";
$_COOKIE['sess_username']="";
setcookie("sess_admin_id", "", time()-3600*24);
setcookie("sess_username","", time()-3600*24);
session_destroy();

header("Location: index.php");

?>