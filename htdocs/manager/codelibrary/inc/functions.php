<?php
/******************************************************************************
*		File : functions.php                                              *
*       Date Created : Wednesday 11 July 2007, 10:57 AM                       *
*       Date Modified : Wednesday 11 July 2007, 10:57 AM                      *
*       File Comment : This file contain functions which will use in coding.  *
*******************************************************************************/

/* Checking if session is created for user login or not */

function checkUserLogin()
{
	if(!isset($_SESSION['UserId']) && (!isset($_SESSION['UserName'])))
	{
		return false;
	}
	else if($_SESSION['UserId']=="" && $_SESSION['UserName']=="")
	{
		return false;
	}
	else
	{
		return true;
	}
}

function url_con($url)
{
	//$url=str_replace("+","|",$url);
	$url=trim($url);
	$url=str_replace("&","|",$url);
	return str_replace(" ","+",$url);
}

function url_decon($url)
{
	$url=trim($url);
	$url=str_replace("|","&",$url);
	//return str_replace("|","+",$url);
	//return str_replace("-"," ",$url);
	return str_replace("+"," ",$url);
}
// This funtion is used for validating the front side users that he is logged in or not.

function validate_user()
{	
	if($_SESSION['sess_uid']=='')
	{
		ms_redirect("login.php?back=".urlencode($_SERVER['REQUEST_URI']));
	}
}


// This funtion is used for validating the admin side users that he is logged in or not.
function validate_admin()
{
	if($_SESSION['sess_admin_id']=='')
	{
		ms_redirect("index.php?back=$_SERVER[REQUEST_URI]");
	}
}

/*

function getCity($id){

$name = 'id not found';
  if($id){
    $nameArr = mysql_fetch_assoc(executeQuery("select city from tbl_city where id= '$id'"));
	return $nameArr['city'];
  }	 
}

function getState($id){

$name = 'id not found';
  if($id){
    $nameArr = mysql_fetch_assoc(executeQuery("select state from tbl_state where id= '$id'"));
	return $nameArr['state'];
  }	 
}

function getCountry($id){

$name = 'id not found';
  if($id){
    $nameArr = mysql_fetch_assoc(executeQuery("select country from tbl_country where id= '$id'"));
	return $nameArr['country'];
  }	 
}
*/

function getCategory($id){

$name = 'id not found';
  if($id){
    $nameArr = mysql_fetch_assoc(executeQuery("select category from tbl_category where id = '$id'"));
	return $nameArr['category'];
  }	 
}

function rating($val){

	if(round($val)<=10)
		$rat="<img src='images/p0.jpg'>";
	if(round($val)>10 && round($val)<=20)
		$rat="<img src='images/p1.jpg'>";
	if(round($val)>20 && round($val)<=30)
		$rat="<img src='images/p2.jpg'>";
	if(round($val)>30 && round($val)<=40)
		$rat="<img src='images/p3.jpg'>";
	if(round($val)>40 && round($val)<=50)
		$rat="<img src='images/p4.jpg'>";
	if(round($val)>50 && round($val)<=60)
		$rat="<img src='images/p5.jpg'>";
	if(round($val)>60 && round($val)<=70)
		$rat="<img src='images/p6.jpg'>";
	if(round($val)>70 && round($val)<=80)
		$rat="<img src='images/p7.jpg'>";
	if(round($val)>80 && round($val)<=90)
		$rat="<img src='images/p8.jpg'>";
	if(round($val)>90 && round($val)<100)
		$rat="<img src='images/p9.jpg'>";
	if(round($val)==100)
		$rat="<img src='images/p10.jpg'>";
		
				echo $rat;
	return $rat;
	
}


// to format the textarea data on pages
function format_paragraph($data)
{
	return "<br>".nl2br($data);
}
// For Executing Query. This function returns a argument which contain recordset 
// object through it user can retrieve values of table.
function executeQuery($sql)
{
	$result = mysql_query($sql) or die("<span style='FONT-SIZE:11px; FONT-COLOR: #000000; font-family=tahoma;'><center>An Internal Error has Occured. Please report following error to the webmaster.<br><br>".$sql."<br><br>".mysql_error()."'</center></FONT>");
	return $result;
} 

// This function returns a recordset object that contain first record data.
function getSingleResult($sql)
{
	$response = "";
	$result = mysql_query($sql) or die("<center>An Internal Error has Occured. Please report following error to the webmaster.<br><br>".$sql."<br><br>".mysql_error()."'</center>");
	if ($line = mysql_fetch_array($result)) {
		$response = $line['0'];
	} 
	return $response;
} 

// For Executing Query. This function update the table by desired data.
function executeUpdate($sql)
{
	mysql_query($sql) or die("<center>An Internal Error has Occured. Please report following error to the webmaster.<br><br>".$sql."<br><br>".mysql_error()."'</center>");
}

// It returns the path of current file.
function getCurrentPath()
{
	global $_SERVER;
	return "http://" . $_SERVER['HTTP_HOST'] . getFolder($_SERVER['PHP_SELF']);
}

// This function adjusts the decimal point of argumented parameter and return the adjusted value.
function adjustAfterDecimal($param)
{
	if(strpos($param,'.')== "")
	{
		$final_value=$param.".00";
		return  $final_value;
	}
	$after_decimal  = substr($param , strpos($param,'.')+1, strlen($param) );	
	$before_decimal = substr($param,0 ,  strpos($param,'.'));
	if(strlen($after_decimal)<2)
	{
		if(strlen($after_decimal)==1)
		{
			$final_value=$param."0";
		}
		if(strlen($after_decimal)==0)
		{
			$final_value.="$param.00";
		}
	}
	else
	{
		$trim_value = substr($after_decimal,0,2);
		$final_value.=$before_decimal.".".$trim_value;
	}
	return $final_value;
}	

// This function is used for redirecting the file on desired file.
function ms_redirect($file, $exit=true, $sess_msg='')
{
	header("Location: $file");
	exit();
}
// This function is used by the paging functions.
function get_qry_str($over_write_key = array(), $over_write_value= array())
{
	global $_GET;
	$m = $_GET;
	if(is_array($over_write_key)){
		$i=0;
		foreach($over_write_key as $key){
			$m[$key] = $over_write_value[$i];
			$i++;
		}
	}else{
		$m[$over_write_key] = $over_write_value;
	}
	$qry_str = qry_str($m);
	return $qry_str;
} 
// This function is used by the paging functions.
function qry_str($arr, $skip = '')
{
	$s = "?";
	$i = 0;
	foreach($arr as $key => $value) {
		if ($key != $skip) {
			if(is_array($value)){
				foreach($value as $value2){
					if ($i == 0) {
						$s .= "$key%5B%5D=$value2";
					$i = 1;
					} else {
						$s .= "&$key%5B%5D=$value2";
					} 
				}		
			}else{
				if ($i == 0) {
					$s .= "$key=$value";
					$i = 1;
				} else {
					$s .= "&$key=$value";
				} 
			}
		} 
	} 
	return $s;
} 

function cust_send_mail($email_to,$emailto_name,$email_subject,$email_body,$email_from,$reply_to,$html=true)
{
	require_once "class.phpmailer.php";
	global $SITE_NAME;
	$mail = new PHPMailer();
	$mail->IsSMTP(); // send via SMTP
	$mail->Mailer   = "mail"; // SMTP servers

	$mail->From     = $email_from;
	$mail->FromName = $SITE_NAME;
	$mail->AddAddress($email_to,$emailto_name); 
	$mail->AddReplyTo($reply_to,$SITE_NAME);
	$mail->WordWrap = 50;                              // set word wrap
	$mail->IsHTML($html);                              // send as HTML
	$mail->Subject  =  $email_subject;
	$mail->Body     =  $email_body;
	$mail->Send();	
	return true;
}


// This function is the replacement of the print_r function. It will work only on the local mode.
function ms_print_r($var)
{
	global $local_mode;
	if ($local_mode || $debug) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
	}
} 
// This function is used to add slashes to a variable.
function add_slashes($param)
{
	$k_param = addslashes(stripslashes($param));
	return $k_param;
} 
// This function is used to strip slashes to a whole array.
function ms_stripslashes($text)
{
	if (is_array($text)) {
		$tmp_array = Array();
		foreach($text as $key => $value) {
			$tmp_array[$key] = ms_stripslashes($value);
			} 
		return $tmp_array;
	} else {
		return stripslashes($text);
	} 
} 
// This function is used to add slashes to whole array.
function ms_addslashes($text)
{
	if (is_array($text)) {
		$tmp_array = Array();
		foreach($text as $key => $value) {
			$tmp_array[$key] = ms_addslashes($value);
		} 
		return $tmp_array;
	} else {
		return addslashes(stripslashes($text));
	} 
} 
// This function is used to add strip html.
function html2text($html)
{
	$search = array ("'<head[^>]*?> = .*?</head>'si", // Strip out javascript
		"'<script>[^>]*?>.*?</script>'si", // Strip out javascript
		"'<[\/\!]*?[^<>]*?>'si", // Strip out html tags
		"'([\r\n])[\s]+'", // Strip out white space
		"'&(quot|#34);'i", // Replace html entities
		"'&(amp|#38);'i",
		"'&(lt|#60);'i",
		"'&(gt|#62);'i",
		"'&(nbsp|#160);'i",
		"'&(iexcl|#161);'i",
		"'&(cent|#162);'i",
		"'&(pound|#163);'i",
		"'&(copy|#169);'i",
		"'&#(\d+);'e"); // evaluate as php
	$replace = array ("",
		"",
		"",
		"\\1",
		"\"",
		"&",
		"<",
		">",
		" ",
		chr(161),
		chr(162),
		chr(163),
		chr(169),
		"chr(\\1)");
	$text = preg_replace ($search, $replace, $html); 
	return $text;
} 
// This function is used to generate sorting arrow in a listing.
function sort_arrows($column){
	global $_SERVER;
	return '<A HREF="'.$_SERVER['PHP_SELF'].get_qry_str(array('order_by','order_by2'), array($column,'asc')).'"><img src="image/white_up.gif" BORDER="0"></A> <A HREF="'.$_SERVER['PHP_SELF'].get_qry_str(array('order_by','order_by2'), array($column,'desc')).'"><img src="image/white_down.gif" BORDER="0"></A>';
}

function sort_arrows_front($column){
	global $_SERVER;
	return '<A HREF="'.$_SERVER['PHP_SELF'].get_qry_str(array('order_by','order_by2'), array($column,'asc')).'"><img src="image/trans_up.gif" BORDER="0"></A> <A HREF="'.$_SERVER['PHP_SELF'].get_qry_str(array('order_by','order_by2'), array($column,'desc')).'"><img src="image/trans_down.gif" BORDER="0"></A>';
}
// This function is used to unlink a file.
function unlink_file( $file_name , $folder_name )
{
	$file_path = $folder_name."/".$file_name;
	@chmod ($folder_name , 0777);
	@touch($file_path);
	@unlink($file_path);
	return true;	
}
// This function is used to show an image in listing.
function showImage($imageName,$type) {
	if($type=='member'){
		if(is_file(SITE_FS_PATH."/user_images/thumb/".$imageName)) {
			$rtVar = "<img src='../user_images/thumb/".$imageName."'>";
		} else {
			$rtVar ='<img src="image/no-image.jpg" width="107" height="105">';
		}
	}	
	return	$rtVar;
}

function dd_date_format($date) {
	if($date) {
		list($y,$m,$d)=explode("-",$date);		
		return "$m/$d/$y";
	}
}

function calcDateDiff ($date1 = 0, $date2 = 0) { 

   // $date1 needs to be greater than $date2. 
   // Otherwise you'll get negative results. 
   
   if ($date2 > $date1) 
       return FALSE; 

    $seconds  = $date1 - $date2; 

   // Calculate each piece using simple subtraction 

  $weeks     = floor($seconds / 604800); 
   $seconds -= $weeks * 604800; 

   $days       = floor($seconds / 86400); 
   $seconds -= $days * 86400; 

   $hours      = floor($seconds / 3600); 
   $seconds -= $hours * 3600; 

   $minutes   = floor($seconds / 60); 
   $seconds -= $minutes * 60; 

   // Return an associative array of results 
   return array( "weeks" => $weeks, "days" => $days, "hours" => $hours, "minutes" => $minutes, "seconds" => $seconds); 
} 

function front_date_format($date) {
	/*if($date) {
		list($y,$m,$d)=explode("-",$date);		
		$front_date=date("d F Y",mktime(0,0,0,$m,$d,$y));
		return $front_date;
	}*/

	if($date!='' && $date!='0000-00-00') {
		//list($y,$m,$d)=explode("-",$date);		
		//return "$m/$d/$y";
		
		$diff = calcDateDiff(time(), $date);
if ($diff = calcDateDiff(time(), $date)) { 

if($diff['weeks']) {
	if($diff['weeks']==1) {
		echo $diff['weeks']." week ago";
	} else {
		echo $diff['weeks']." weeks ago";
	}
}else if($diff['days']) {
	if($diff['days']==1) {
		echo $diff['days']." day ago";
	} else {
		echo $diff['days']." days ago";
	}
} 
else if($diff['hours']){
	if($diff['hours']==1) {
	echo $diff['hours']. " hour ago";
	} else {
	echo $diff['hours']. " hours ago";
	}
} 
else if($diff['minutes']){
	if($diff['minutes']==1) {
	echo $diff['minutes']. " minute ago";
	} else {
	echo $diff['minutes']. " minutes ago";
	}
} 
else  {
 if($diff['seconds']==0)
 echo	"1 second ago";
 else
echo $diff['seconds']. " seconds ago";
}
} 

													
	}
}

function resize_img($imgPath, $maxWidth, $maxHeight, $directOutput = true, $quality = 90, $verbose,$imageType)
{
   // get image size infos (0 width and 1 height,
   //     2 is (1 = GIF, 2 = JPG, 3 = PNG)
  
     $size = getimagesize($imgPath);
		$arr=explode(".",$imgPath);		
   // break and return false if failed to read image infos
     if(!$size){
       if($verbose && !$directOutput)echo "<br />Not able to read image infos.<br />";
       return false;
     }

   // relation: width/height
     $relation = $size[0]/$size[1];
	 
	 $relation_original = $relation;
   
   
   // maximal size (if parameter == false, no resizing will be made)
     $maxSize = array($maxWidth?$maxWidth:$size[0],$maxHeight?$maxHeight:$size[1]);
   // declaring array for new size (initial value = original size)
     $newSize = $size;
   // width/height relation
     $relation = array($size[1]/$size[0], $size[0]/$size[1]);


	if(($newSize[0] > $maxWidth))
	{
		$newSize[0]=$maxSize[0];
		$newSize[1]=$newSize[0]*$relation[0];
		$newSize[0]=$maxWidth;
		$newSize[1]=$newSize[0]*$relation[0];		
		
	}
	
		$newSize[0]=$maxWidth;
		
		if($maxHeight)
		{
		$newSize[1]=$maxHeight;		
		}
		else
		{
		$newSize[1]=$newSize[0]*$relation[0];
		}
     // create image
       switch($size[2])
       {
         case 1:
           if(function_exists("imagecreatefromgif"))
           {
             $originalImage = imagecreatefromgif($imgPath);
           }else{
             if($verbose && !$directOutput)echo "<br />No GIF support in this php installation, sorry.<br />";
             return false;
           }
           break;
         case 2: $originalImage = imagecreatefromjpeg($imgPath); break;
         case 3: $originalImage = imagecreatefrompng($imgPath); break;
         default:
           if($verbose && !$directOutput)echo "<br />No valid image type.<br />";
           return false;
       }


     // create new image

       $resizedImage = imagecreatetruecolor($newSize[0], $newSize[1]); 

       imagecopyresampled($resizedImage, $originalImage,0, 0, 0, 0,$newSize[0], $newSize[1], $size[0], $size[1]);
		$rz=$imgPath;
     // output or save
       if($directOutput)
		{
         imagejpeg($resizedImage);
		 }
		 else
		{
			
			 $rz=preg_replace("/\.([a-zA-Z]{3,4})$/","".$imageType.".".$arr[count($arr)-1],$imgPath);
         		imagejpeg($resizedImage, $rz, $quality);
         }
     // return true if successfull
       return $rz;
}


    function sendsms($number, $message) {
        
        
        
        //Please Enter Your Details
        $user = "AnkurGakhar"; //your username
        $password = "36890943"; //your password
        $mobilenumbers = $number; //enter Mobile numbers comma seperated

        $message = $message; //enter Your Message
        $senderid = "SMSCntry"; //Your senderid
        $messagetype = "N"; //Type Of Your Message
        $DReports = "Y"; //Delivery Reports
        $url = "http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
        $message = urlencode($message);
        $ch = curl_init();
        if (!$ch) {
            die("Couldn't initialize a cURL handle");
        }
        $ret = curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "User=$user&passwd=$password&mobilenumber=$mobilenumbers&message=$message&sid=$senderid&mtype=$messagetype&DR=$DReports");
        $ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //If you are behind proxy then please uncomment below line and provide your proxy ip with port.
        // $ret = curl_setopt($ch, CURLOPT_PROXY, "PROXY IP ADDRESS:PORT");
        $curlresponse = curl_exec($ch); // execute
        if (curl_errno($ch))
            echo 'curl error : ' . curl_error($ch);
        if (empty($ret)) {
            // some kind of an error happened
            die(curl_error($ch));
            curl_close($ch); // close cURL handler
        } else {
            $info = curl_getinfo($ch);
            curl_close($ch); // close cURL handler
            return;
            //		echo $curlresponse; //echo "Message Sent Succesfully" ;
            //	exit;
        }
    }


?>
