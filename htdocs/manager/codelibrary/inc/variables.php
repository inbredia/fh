<?php
session_start();
/******************************************************************************
*		File : config.inc.php                                                 *
*       Date Created : Wednesday 11 July 2007, 10:53 AM                       *
*       Date Modified : Wednesday 11 July 2007, 10:53 AM                      *
*       File Comment : This file contain functions which will use in coding.  *
*******************************************************************************/
if($_COOKIE['cook_uid']!='' && $_COOKIE['cook_uid']!='deleted')
{
$_SESSION['sess_uid']=$_COOKIE['cook_uid'];
}
if($_COOKIE['cook_username']!='')
{
$_SESSION['cook_username']=$_COOKIE['cook_username'];
}
if($_COOKIE['cook_email']!='')
{
$_SESSION['cook_email']=$_COOKIE['cook_email'];
}

if($_SERVER['HTTP_HOST']=="localhost" || $_SERVER['HTTP_HOST']=="s3" || $_SERVER['HTTP_HOST']=="system5") {
	// Config setting for localhost.
	define(DBSERVER,"localhost");
	define(DBNAME,"tcdj_25_10");
	define(DBUSER,"root");
	define(DBPASS,"");
	define(SITE_URL,"http://localhost/");
	define(FCK_PATH,"../FCKeditor/");
	define(SITE_PATH,"http://localhost/");
} else {
	// Config setting for live server.
	define(DBSERVER,"localhost");
	define(DBNAME,"fastest_hiring");
	define(DBUSER,"fastest_hiring");
	define(DBPASS,"Priyanka@1");
	define(SITE_PATH,"http://www.fastesthiring.com/");
	define(FCK_PATH,"../FCKeditor/");
}

// Database Connection Establishment String
mysql_connect(DBSERVER,DBUSER,DBPASS);

// Database Selection String
mysql_select_db(DBNAME);

// Some common settings
define(SITE_TITLE,"FastestHiring - A place for people to share things they're willing to do.");
define(SITE_ADMIN_TITLE,"FastestHiring - Secure Admin Area");
define(PAGING_SIZE,15);



refreshParameterValues($_REQUEST);
refreshParameterValues($_POST);
refreshParameterValues($_GET);

function refreshParameterValues($arr)
{
	foreach($arr as $key=>$val){
		if(is_array($val))
			$arr[$key] = refreshParameterValues($val);
		else{
			$arr[$key] = mysql_real_escape_string($val);
		}
	}
	return $arr;
}
?>
