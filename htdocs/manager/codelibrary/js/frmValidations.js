/*	JavaScript for from validation	*/

	function checkEmail(myForm)
	{
		if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myForm.value))
		{
			return (true);
		}
		alert("Invalid e-mail address! please re-enter");
		return (false);
	}

    function trim(inputString) 
	{
		if (typeof inputString != "string") { return inputString; }
		var retValue = inputString;
		var ch = retValue.substring(0, 1);
		while (ch == " ") 
		{ 
			retValue = retValue.substring(1, retValue.length);
			ch = retValue.substring(0, 1);
		}
		ch = retValue.substring(retValue.length-1, retValue.length);
		while (ch == " ") 
		{
			retValue = retValue.substring(0, retValue.length-1);
			ch = retValue.substring(retValue.length-1, retValue.length);
		}
		while (retValue.indexOf("  ") != -1) 
		{ 
			retValue = retValue.substring(0, retValue.indexOf("  ")) + retValue.substring(retValue.indexOf("  ")+1, retValue.length); 
		}
		return retValue; 
	}

/*	Validation for Login form	*/

    function checkLogin(myForm)
	{
		if(myForm.username.value=="")
		{
			alert("Please enter username!");
			myForm.username.focus();
			return false;
		}
		if(myForm.userpass.value=="")
		{
			alert("Please enter password!");
			myForm.userpass.focus();
			return false;
		}
		return true;
	}

/*	Validation for profile image upload		*/

	function checkUploadImg(myForm)
	{
		if(myForm.user_img.value=="")
		{	
			alert("Please select an image to upload!");
			myForm.user_img.focus();
			return false;
		}
		return true;
	}
		
/*	Validation for Forgot Password form 	*/

	function checkForgotPassword(myForm)
	{
	    if(myForm.username.value=="")
		{
			alert("Please enter username");
			myForm.username.focus();
			return false;
		}
		if(myForm.email.value=="")
		{
			alert("Please enter email address");
			myForm.email.focus();
			return false;
		}
		if(checkEmail(myForm.email)==false)
		{			
			myForm.email.focus();
			return false; 
		}
		return true;
	}

/*	Validation for User Registration	*/

	function checkRegister(myForm)
	{
		if(myForm.username.value=="")
		{
			alert("Please enter username.");
			myForm.username.focus();
			return false
		}
		if(myForm.userpass.value=="")
		{
			alert("Please enter password.");
			myForm.userpass.focus();
			return false
		}
		if(myForm.conf_userpass.value=="")
		{
			alert("Please re-enter your password.");
			myForm.conf_userpass.focus();
			return false
		}
		if(myForm.userpass.value != myForm.conf_userpass.value)
		{
			alert("Your password and confirm password does not match, please re-enter.");
			myForm.conf_userpass.focus();
			return false
		}
		if(myForm.email.value=="")
		{
			alert("Please enter email address");
			myForm.email.focus();
			return false;
		}
		if(checkEmail(myForm.email)==false)
		{			
			myForm.email.focus();
			return false; 
		}
		if(myForm.display_name.value=="")
		{
			alert("Please enter your display name.");
			myForm.display_name.focus();
			return false
		}
		if(!myForm.agree.checked==true)
		{
			alert("You must agree to our terms and condition to proceed registration.");
			myForm.agree.focus();
			return false
		}		
		return true;
	}
	
	