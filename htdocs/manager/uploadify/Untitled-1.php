<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="image/icon/favicon.ico">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Registration</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="uploadify/uploadify.css" type="text/css" />

<link rel="stylesheet" href="uploadify/uploadify.jGrowl.css" type="text/css" />
<script type="text/javascript" src="js/func.js"></script>
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/jquery.uploadify.js"></script>
<script type="text/javascript" src="js/jquery.jgrowl_minimized.js"></script>
<script type="text/javascript">
$(document).ready(function() {
$("#fileUploadname3").fileUpload({
		'uploader': 'uploadify/uploader.swf',
		'cancelImg': 'uploadify/cancel.png',
		'script': 'uploadify/upload_name.php',
		'folder': 'upload_images/user',
		'multi': true,
		'displayData': 'percentage',
		'fileDesc': 'Image Files',
		'fileExt': '*.jpg;*.jpeg;*.png;*.gif',
		'sizeLimit': 5194304,'scriptData':  {'uid':'22','namenew':'0'},
		onSelect:function()
		{
			
			document.getElementById('show').style.display='';
		}, 
		onError: function (event, queueID ,fileObj, errorObj) {
			var msg;
			if (errorObj.status == 404) {
				alert('Could not find upload script.');
				msg = 'Could not find upload script.';
			} else if (errorObj.type === "HTTP")
				msg = errorObj.type+": "+errorObj.status;
			else if (errorObj.type ==="File Size")
				msg = fileObj.name+'<br>'+errorObj.type+' larger than: '+Math.round(errorObj.sizeLimit/1024)+'KB';
			else
				msg = errorObj.type+": "+errorObj.text;
			$.jGrowl('<p></p>'+msg, {
				theme: 	'error',
				header: 'ERROR',
				sticky: true
			});			
			$("#fileUploadname3" + queueID).fadeOut(250, function() { $("#fileUploadname3" + queueID).remove()});
			return false;
		},
		onComplete: function (evt, queueID, fileObj, response, data) {
		//alert(response+data);
		},onAllComplete: function()
		{
		location.href='thanks.php';
		}
		
	});
	$('#name1').bind('click', function(){
	var va;
	if(document.getElementById('name1').checked)
      va=1;
	  else if(document.getElementById('name2').checked)
      va=2;
	   else if(document.getElementById('name3').checked)
      va=3;
		$('#fileUploadname3').fileUploadSettings('scriptData','&uid=22&namenew='+va);
	});
		$('#name2').bind('click', function(){
	var va;
	if(document.getElementById('name1').checked)
      va=1;
	  else if(document.getElementById('name2').checked)
      va=2;
	   else if(document.getElementById('name3').checked)
      va=3;
		$('#fileUploadname3').fileUploadSettings('scriptData','&uid=22&namenew='+va);
	});
		$('#name3').bind('click', function(){
	var va;
	if(document.getElementById('name1').checked)
      va=1;
	  else if(document.getElementById('name2').checked)
      va=2;
	   else if(document.getElementById('name3').checked)
      va=3;
		$('#fileUploadname3').fileUploadSettings('scriptData','&uid=22&namenew='+va);
	});
	

	
	
		
});
</script>
<script>
function valid(obj)

	{
	//if(obj.marital_status.value=='')
	myOption = -1;
  for (i=obj.name.length-1; i > -1; i--)
	{
	if (obj.name[i].checked) {
	myOption = i; i = -1;

	}
	}
	if (myOption == -1) {
     alert("You must selected Display Option!");
     return false;
 }
	else
	{
	return true;
	}
}

</script>
</head>

<body>
<table width="947" align="center" cellpadding="0"  cellspacing="0" class="border_leftright">
  <tr>
    <td align="left" valign="top"><table width="100%" align="left" cellpadding="0"  cellspacing="0">
      <tr>
        <td height="42" align="right" valign="top" class="headr_topbg"><table width="126" align="right" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center" valign="top" class="black14 top6"><a href="#">FAQ</a><a id="loginid" href="javascript:void(0);">
			<img src="image/login.jpg" alt="login" width="69" height="22" hspace="9"  id="loginimg"  style="margin-bottom:5px; " border="0" align="middle"></a><br><div class="loginbox" style="top:22px; position:absolute; display:none; background-color:#E08300;z-index:100; width:300px; padding:6px; right:45px;"><p class="loginbox1" style="display:none " align="left">
			<form name="frmlogin" id="frmlogin" method="post"  style="margin:0px; padding:0px; "  action="">
			<input type="hidden" name="logged" value="yes">
			
			 
			<table cellpadding="0" cellspacing="0" border="0" width="100%"  class="white_txt" style="margin-top:15px; margin-bottom:15px; ">
					<tr>
					<td width="32%" height="24" align="left" valign="top">Login (Email ID)</td>
					<td width="68%" align="left" valign="top"><input type="text" name="email" id="email" class="inputbox"></td>
					</tr>
					<tr>
					<td width="32%"  align="left" valign="top"></td>
					<td width="68%" align="left" valign="top" id="l_login_id"   class="l_login black11">Please enter Email ID</td>
					</tr>
					<tr>
					<td height="24" align="left" valign="top">Password</td>
					<td align="left" valign="top"><input type="password" name="password" id="password" class="inputbox"></td>
					</tr>
					<tr>
					<td width="32%"  align="left" valign="top"></td>
					<td width="68%" align="left" valign="top" class="l_pass black11">Please enter password</td>
					</tr>
					<tr>
					<td height="32" align="left" valign="top" style="border-bottom:1px solid #FFEECB; ">&nbsp;</td>
					<td align="left" valign="top" style="border-bottom:1px solid #FFEECB; "><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="35%" align="left" valign="middle"><input type="submit" name="Submit" value="Submit"></td>
                        <td width="65%" align="left" valign="middle">&nbsp;&nbsp;<a href="#" class="white11">Forgot Password?</a></td>
                      </tr>
                    </table></td>
					</tr>
					<tr>
					<td height="22" align="left" valign="top">&nbsp;</td>
					<td align="left" valign="top"><input type="checkbox" name="checkbox" value="checkbox">
					  Remember me </td>
					</tr>
					<tr>
					<td align="left" valign="top">&nbsp;</td>
					<td align="left" valign="top"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="71%" align="left" valign="middle">&nbsp;</td>
                        <td width="29%" align="right" valign="middle"><a href="javascript:void(0)" class="white_txt" onClick="$('#loginid').click()"><strong>Close</strong></a></td>
                      </tr>
                    </table></td>
					</tr>
			</table>
</form></p></div></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="left" valign="top"><table width="100%" align="left" cellpadding="0"  cellspacing="0">
          <tr>
            <td width="384" align="left" valign="top"><a href="index.php"><img src="image/logo.jpg" border="0" width="384" height="158" alt=""></a></td>
            <td align="left" valign="bottom"><table width="100%"  cellspacing="0" cellpadding="0">
              <tr>
                <td align="right" valign="bottom" class="top4"><a href="#"><img src="image/img_livehelp.jpg" alt="" width="279" height="115" hspace="9" border="0"></a></td>
              </tr>
              <tr>
                <td align="left" valign="top"><table width="100%" align="left" cellpadding="0"  cellspacing="0">
                  <tr>
                    <td width="12" align="left" valign="top"><img src="image/topbar_left.jpg" width="12" height="39" alt=""></td>
                    <td align="left" valign="middle" style=" background-image:url(images/topbar_bg.jpg);background-repeat:repeat-x; "><table width="100%" align="left" cellpadding="0" cellspacing="0" class="black16">
                        <tr align="center" valign="top">
                          <td width="79"><a href="#">Offers </a></td>
                          <td width="2"><img src="image/topbar_seprater.jpg" width="2" height="22" alt=""></td>
                          <td width="107"> <a href="registration1.php">Register</a> </td>
                          <td width="2"><img src="image/topbar_seprater.jpg" width="2" height="22" alt=""> </td>
                          <td><a href="#">Tours</a> </td>
                          <td width="2"><img src="image/topbar_seprater.jpg" width="2" height="22" alt=""></td>
                          <td><a href="#">Plans</a> </td>
                          <td width="2"><img src="image/topbar_seprater.jpg" width="2" height="22" alt=""></td>
                          <td> <a href="#">Features</a></td>
                          <td width="2"><img src="image/topbar_seprater.jpg" width="2" height="22" alt=""></td>
                          <td> <a href="#">Search</a></td>
                        </tr>
                    </table></td>
                    <td width="19" align="left" valign="top"><img src="image/topbar_right.jpg" width="19" height="39" alt=""></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td height="48" align="center" valign="top" style="background-image:url(images/topbar_orang_sadow.jpg); "><table width="852" align="right" cellpadding="0" cellspacing="0" class="black14" style="margin-top:10px; ">
          <tr>
            <td align="left" valign="top"><a href="#">Bengali</a>  &nbsp;| &nbsp;<a href="#">Gujarati</a> &nbsp;| &nbsp;<a href="#">Hindi</a>&nbsp; | &nbsp;<a href="#">Kannada</a>&nbsp; | &nbsp;<a href="#">Malayalam</a>&nbsp; | &nbsp;<a href="#">Marathi</a> &nbsp;| &nbsp;<a href="#">Marwari</a> &nbsp;| &nbsp;<a href="#">Punjabi</a> &nbsp;| &nbsp;<a href="#">Sindhi</a> &nbsp;| <a href="#">Tamil</a> &nbsp;| <a href="#">Telugu</a>&nbsp; | <a href="#">Urdu</a> | <a href="#">More</a><a href="#"><img src="image/img_bullet.jpg" alt="bullet" width="8" height="4" hspace="5" border="0" align="middle"></a></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="pad3" height="430"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="grayborder">
      <tr>
        <td align="left" valign="middle" id="grad"><table width="98%"  border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="51%" height="36" align="left" valign="middle">&nbsp; <span class="green22">Upload Your Photo</span></td>
            <td width="49%" align="right" valign="middle" class="orange12">I'll do this later>></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="left" valign="top" bgcolor="#FEFEFE" class="pad10"><table width="100%"  border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" >
          <tr>
            <td align="left" valign="top" class="pad10"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="500" align="left" valign="top" class="bordergray pad10">
				<form action="profile_photo.php" method="post" enctype="multipart/form-data"  name="frm_1"  onsubmit="return valid(this)">
			    <input type="hidden" name="submitForm" value="yes">
				
				
				<table width="100%"  border="0" cellspacing="0" cellpadding="0">
                   				  
				 <tr>
                    <td align="left" valign="top" class="top4 green18" colspan="2">Basic Photo Upload</td>
                    <td align="left" valign="top" class="top4">&nbsp;&nbsp;&nbsp;</td>
                  </tr>

				  <tr>
                    <td colspan="2" align="left" valign="top" class="top4" ><b>Note:</b></td>
                    </tr>
                  <tr>
				  <td colspan="2" align="left" valign="top">
				  <ul>
				  <li>Upload up to 3 photo in JPEG,GIF, format only</li>
				  <li>Photo Size Limit in 5MB each</li>
				  <li>Your Photo will be screened before activation</li>
				  </ul>
				  </td>
				  </tr>
                   <tr>
                    <td align="left" valign="top" class="top4 green18" colspan="2">Profile Photo</td>
                    <td align="left" valign="top" class="top4">&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left" valign="top" class="top4"></td>
                    <td align="left" valign="top" class="top4"><div id="fileUploadname3">Please wait...</div><div id="show" style="display: none; padding-top: 5px;" class="small_blue">Click Browse again to choose more pictures to upload</div>
					<br />
					<!--<input class="upload_button" type="button" onClick="javascript:$('#fileUploadname3').fileUploadStart();" name="submit1" value="Upload">&nbsp;-->You can select multiple Image File. jpg, gif, png only.
					<div style="padding-left:52px;" class="orange12">Read Instructions</div></td>
					 
                  </tr>
				
                  <tr><td colspan="2" class="bluetxt" style="padding-top:20px;">Add More Album Photos</td>
				  </tr>
				
				   <tr>
				     <td colspan="2" align="left" valign="top" class="top4 green18" style="padding-top:20px; ">Choose Display Option&nbsp;&nbsp;&nbsp;
					 
					 </td>
                    </tr>
                  <tr>
                    
                    <td align="left" valign="top" class="top4" colspan="2"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td  align="left" valign="top"><input type="radio" id="name1" name="name" value="1"></td>
                        <td  align="left" valign="top"> Visible to at Members</td>
                      </tr>
                      <tr>
                        <td align="left" valign="top" class="top4"><input type="radio" id="name2" name="name" value="2"></td>
                        <td align="left" valign="top" class="top4">Visible only to Members | express intreset in and to member | accept</td>
                      </tr>
					  <tr>
                        <td align="left" valign="top" class="top4"><input type="radio" id="name3" name="name" value="3"></td>
                        <td align="left" valign="top" class="top4">Protect my photo with a photo password </td>
                      </tr>
                   
                    </table></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top" class="top4">&nbsp;</td>
                    <td align="left" valign="top" class="top13"></td>
                  </tr>
                </table>
				</form></td>
                <td width="15" align="left" valign="top"><img src="image/specer.gif" width="1" height="1"></td>
                <td align="left" valign="top"><table width="375" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="top" class="green18" id="account_image"><div>Get More Proposals</div> <div>with a photo</div></td>
                  </tr>
                </table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" class="top13"><table width="100%"  cellspacing="0" cellpadding="0">
      <tr>
        <td width="2" align="left" valign="top"><img src="image/bottom_left.jpg" width="2" height="172" alt=""></td>
        <td align="left" valign="top" style="background-image:url(images/bottom_bg.jpg); " ><table width="100%"  cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" valign="top"><table width="50%"  cellspacing="0" cellpadding="0">
              <tr>
                <td width="187" align="left" valign="top"><img src="image/img_nrisection_pink.jpg" width="157" height="35" alt=""></td>
                <td align="left" valign="middle" class="green_14"><a href="#" >Follow us Twitter</a> | <a href="www.facebook.com">Facebook</a> | <a href="#">Orkut</a></td>
              </tr>
            </table></td>
          </tr>
          <tr> 
            <td align="left" valign="top" class="top6 left17"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="left" valign="top"><strong>Explore &ndash;</strong> <a href="index.php">Home</a> | <a href="login.php">login</a> | <a href="registration.php">Register Free</a> | <a href="#">Offers</a> | <a href="#">Features</a> | <a href="#">Photogenic Profiles</a> | <a href="#">Success Stories</a>. </td>
              </tr>
              <tr>
                <td align="left" valign="top" class="top4"><strong>Services &ndash;</strong>  <a href="login.php">Membership Plan</a> | <a href="content.php?id=9">Advertise With Us</a> | <a href="#">Magazine</a> | <a href="#">Franchise</a> | <a href="#">Security Tips</a>.</td>
              </tr>
              <tr>
                <td align="left" valign="top" class="top4"><strong>Need Help &ndash;</strong>  <a href="content.php?id=6">Contact Us</a> | <a href="#">FAQs</a> | <a href="#">Quick Tour</a> | <a href="content.php?id=2">Live Help</a> | <a href="#">Feed Back </a>| <a href="content.php?id=10">Sitemap</a>.</td>
              </tr>
              <tr>
                <td align="left" valign="top" class="top4"><strong>Legal &ndash;</strong>  <a href="#">Report Abuse</a> | <a href="content.php?id=4">Terms & Conditions</a> | <a href="#">Third Party Services</a> | <a href="content.php?id=5">Privacy Policy </a>| <a href="#">Disclaimer</a>.</td>
              </tr>
               </table></td>
          </tr>
          <tr>
            <td align="center" valign="top" class="top8"><strong>Copyright &copy; 2010 marriagemaker Internet Services</strong></td>
          </tr>
        </table></td>
        <td width="2" align="left" valign="top"><img src="image/bottom_right.jpg" width="2" height="172" alt=""></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
