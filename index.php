<?php

//ini_set('set_include_path','.:/usr/lib/php:/usr/local/lib/php:/home/fastest/public_html/library:/home/fastest/public_html/library/Zend::/home/fastest/public_html/library/ZendX');
error_reporting(E_ALL ^ E_NOTICE);
/*error_reporting(E_ALL | E_STRICT);    
ini_set('display_startup_errors', 1);    
ini_set('display_errors', 1);
*/


#error_reporting(0);

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));
//echo APPLICATION_PATH;
// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

//echo get_include_path();
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));


//set_include_path('/home/fastest/public_html/library:/usr/lib/php:/usr/local/lib/php:/home/fastest/php');
//echo '<br>\ndfgfdg-'. get_include_path();
/** Zend_Application */
require_once 'library/Zend/Application.php';

require_once 'library/Zend/Loader/Autoloader.php';
define('WEBSITE','http://www.fastesthiring.com/'); 

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

if(!Zend_Session::isStarted())
{
	Zend_Session::start();
	Zend_Session::setOptions();
}
$application->bootstrap()->run();
?>